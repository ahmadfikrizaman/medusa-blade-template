@extends(WH::theme('layouts.master'), ['title' => trans('applications.ftp accounts'), 'sidemenu' => 'file', 'applicationTitle' => trans('applications.file'), 'breadcrumbArray' => 'app:ftp', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="ftpVM" v-clock>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>{{ trans('core.package usage') }}</h3>
        </div>

        <div class="col-md-12">
            <usage-progress
                title="{{ trans('applications.ftp usage') }}"
                color="{{ $ftpUsage['color'] }}"
                value="{{ $ftpUsage['value'] }}"
                max="{{ $ftpUsage['max'] }}"
                percentage="{{ $ftpUsage['percentage'] }}">
            </usage-progress>
            <usage-progress
                title="{{ trans('applications.ftp quota usage') }}"
                color="{{ $ftpQuotaUsage['color'] }}"
                value="{{ $ftpQuotaUsage['value'] }}"
                max="{{ $ftpQuotaUsage['max'] }}"
                percentage="{{ $ftpQuotaUsage['percentage'] }}">
            </usage-progress>
        </div>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.ftp accounts') }}</h4>
                    <div class="clearfix"></div>
                    <p></p>
                    <table class="table" id="ftpAccountList" data-url="{{ route('app:ftp:api', $user->hashid) }}" >
                        <thead>
                            <tr>
                                <th>{{ trans('form.username') }}</th>
                                <th>{{ trans('form.ftp host') }}</th>
                                <th>{{ trans('form.home folder') }}</th>
                                <th class="text-center">{{ trans('form.ftp quota') }}</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="account in accounts">
                                <td>
                                    <i v-if="account['status'] == 0" class="fa fa-circle text-success" data-toggle="tooltip" data-placement="top" title="{{ trans('form.active') }}"></i>
                                    <i v-if="account['status'] == 1" class="fa fa-circle text-danger" data-toggle="tooltip" data-placement="top" title="{{ trans('form.inactive') }}"></i>
                                    @{{ account['username'] }}
                                </td>
                                <td>@{{ account.host }}</td>
                                <td>@{{ account.homeFolder }}</td>
                                <td class="text-center">@{{ account.quota }}</td>
                                <td class="text-center">
                                    <div class="btn-group">
                                        <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ trans('form.more') }} <i class="fa fa-chevron-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="@{{ account['edit'] }}">{{ trans('form.edit') }}</a></li>
                                            <li>
                                                <a v-if="account['status'] == 0" href="@{{ account['enabledisable'] }}">{{ trans('form.disable') }}</a>
                                                <a v-if="account['status'] == 1" href="@{{ account['enabledisable'] }}">{{ trans('form.enable') }}</a>
                                            </li>
                                            <li><a data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="delete( account.destroy, '{{ trans('messages.confirm delete ftp') }}')">{{ trans('form.delete') }}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])
    </div>

@include(WH::theme('partials.templates'), ['template' => 'usageProgress'])
@stop
