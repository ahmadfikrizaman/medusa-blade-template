@extends(WH::theme('layouts.master'), ['title' => trans('applications.edit ftp account'), 'sidemenu' => 'file', 'applicationTitle' => trans('applications.file'), 'breadcrumbArray' => 'app:ftp:edit', 'breadcrumbParam' => [$user, $ftp]])

@section('main-content')
    <div class="row" id="ftpVM" v-clock>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.edit ftp account') }}</h4>
                    <p></p>
                    <form action="{{ route('app:ftp:update', [$user->hashid, $ftp->hashid]) }}" method="POST" role="form">
                        @include(WH::theme('apps.ftp.form'), ['saveText' => trans('form.update'), 'edit' => true])
                    </form>
                </div>
            </div>
        </div>

    </div>
@stop
