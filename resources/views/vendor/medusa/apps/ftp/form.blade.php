{{ csrf_field() }}

<div class="form-group">
    <label for="">{{ trans('form.username') }}</label>
    <div class="input-group">
        <input type="text" name="username" class="form-control"  placeholder="{{ trans('form.username') }}" value="{{ $ftp->username or  old('username') }}" v-bind:disabled="{{ $edit }}">
        <div class="input-group-addon btn-primary">@</div>
        {!! Form::select('ftpDomain', $ftpDomains, isset($ftp) ? $ftp->record_id : old('ftpDomain'), ['class' => 'form-control', 'v-bind:disabled' => $edit]) !!}
    </div>
</div>


<password-generator
    :password.sync="password"
    :verifypassword.sync="verifyPassword"
    :passwordlength="16"
    modalid="modalGeneratePassword">
</password-generator>

<browse-folder
    label="{{ trans('form.home folder') }}"
    addon="/var/www/{{ $user->username }}"
    placeholder="{{ trans('form.home folder') }}"
    model="homeFolder"
    currentvalue="{{ isset($ftp) ? $ftp->homedir : null }}"
    listmodalid="listFolderModal"
    createmodalid="createFolderModal"
    fetchurl="{{ route('app:ftp:browse', $user->hashid) }}"
    fetchlistener="applications#ListFTPFileDirectory#{{ WH::credentialString() }}"
    createfolderurl="{{ route('app:ftp:createfolder', $user->hashid) }}"
    createfolderlistener="applications#NewFTPFileDirectory#{{ WH::credentialString() }}"
    :is-busy="isBusy"
    :isError="isError"
    :homefolder.sync="homeFolder">
</browse-folder>


<div class="form-group" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.ftp quota info', ['quota' => $maxQuota]) }}">
    <label for="">{{ trans('form.ftp quota') }}</label>
    <input type="number" class="form-control" name="ftpQuota" placeholder="{{ trans('form.ftp quota helper') }}" value="{{ $ftp->quotaSize or old('ftpQuota') }}">
</div>

<button type="submit" class="btn btn-primary btn-submit pull-right">{{ $saveText }}</button>

@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
@include(WH::theme('partials.templates'), ['template' => 'browse-folder'])
