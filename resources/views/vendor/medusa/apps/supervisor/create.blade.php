@extends(WH::theme('layouts.master'), ['title' => trans('applications.add supervisor job'), 'sidemenu' => 'advance setting', 'applicationTitle' => trans('applications.advance settings'), 'breadcrumbArray' => 'app:supervisor:create', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="supervisorCreateVM"
        data-vendor-list="{{ route('app:supervisor:vendor', $user->hashid) }}"
        data-redirect="{{ route('app:supervisor:index', $user->hashid) }}" v-cloak>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('applications.add supervisor worker') }}</h4>
                        <p></p>
                        <form v-show="!isBusy" action="{{ route('app:supervisor:store', $user->hashid) }}" method="POST" role="form" class="margin-top-20" @submit.prevent="submit">

                            <div class="form-group">
                                <label for="">{{ trans('form.name') }}</label>
                                <input type="text" class="form-control" v-model="job.name" placeholder="{{ trans('form.supervisor name helper') }}">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.vendor') }}</label>
                                <select v-model="job.vendor" class="form-control" required="required">
                                    <option selected disabled>{{ trans('form.select vendor') }}</option>
                                    <option v-for="v in vendor" value="@{{ v }}">@{{ v }}</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="">{{ trans('form.command') }}</label>

                                <div class="input-group">
                                    <span class="input-group-addon btn-primary">/var/www/{{ $user->username }}/web</span>
                                    <input type="text" class="form-control" v-model="job.command" placeholder="{{ trans('form.supervisor command helper', ['name' => $user->username]) }}">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.add') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
