@extends(WH::theme('layouts.master'), ['title' => trans('applications.supervisor'), 'sidemenu' => 'advance setting', 'applicationTitle' => trans('applications.advance settings'), 'breadcrumbArray' => 'app:supervisor', 'breadcrumbParam' => [$user]])


@section('main-content')

    <div class="row" id="supervisorVM" v-cloak>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.supervisor worker') }}</h4>
                    <p></p>
                    <table class="table table-hover" id="listJob"
                        data-list="{{ route('app:supervisor:list', $user->hashid) }}"
                        data-reload="{{ route('app:supervisor:reload', $user->hashid) }}"
                        data-delete="{{ route('app:supervisor:destroy', $user->hashid) }}">

                        <thead>
                            <tr>
                                <th>{{ trans('form.name') }}</th>
                                <th>{{ trans('form.command') }}</th>
                                <th class="text-center">{{ trans('form.status') }}</th>
                                <th class="text-center">{{ trans('form.uptime') }}</th>
                                <th class="text-center">{{ trans('form.reload') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="job in jobs">
                                <td>@{{ job.name }}</td>
                                <td>@{{ job.command }}</td>
                                <td class="text-center">@{{ job.status }}</td>
                                <td class="text-center" v-if="job.status == 'RUNNING'">@{{ job.uptime }}</td>
                                <td class="text-center" v-if="job.status != 'RUNNING'">00:00:00</td>
                                <td class="text-center"><a class="text-success" @click="reloadSupervisor(job.baseName)"><span class="fa fa-refresh"></span></a></td>
                                <td class="text-center"><a class="text-danger" @click="deleteSupervisor(job.baseName)"><span class="fa fa-trash"></span></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'reloadsupervisor'])
        @include(WH::theme('partials.modal'), ['modal' => 'deletesupervisor'])
    </div>
@stop
