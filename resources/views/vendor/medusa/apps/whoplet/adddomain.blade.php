@extends(WH::theme('layouts.master'), ['title' => trans('applications.add additional domain'), 'sidemenu' => 'whoplet', 'applicationTitle' => $request->whoplet, 'breadcrumbArray' => 'app:whoplet:adddomain', 'breadcrumbParam' => [$user, $request->whoplet, $request->secure]])

@section('icon')
    <span class="medusa-circle">{{ substr($request->whoplet, 0, 1) }}</span>
@endsection

@section('main-content')

<div class="row" id="addWhopletDomainVM"
    data-fetch="{{ route('app:whoplet:show:fetch', $user->hashid) }}"
    data-redirect="{{ route('app:whoplet:show', [$user->hashid, 'whoplet' => $request->whoplet, 'secure' => $request->secure]) }}" v-cloak>

    @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

    <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ajax-error></ajax-error>
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.add additional domain') }}</h4>
                <p></p>
                <form action="{{ route('app:whoplet:adddomain', [$user->hashid, 'whoplet' => $request->whoplet, 'secure' => $request->secure]) }}" method="POST" role="form" @submit.prevent="addNewDomain">
                    <div class="form-group">
                        <label for="">{{ trans('form.domain name') }}</label>

                        <div class="input-group">
                            <input type="text" class="form-control" name="domain" v-model="addDomain.domain" placeholder="{{ trans('applications.domain name helper') }}" autofocus>
                            <span class="input-group-addon btn-primary">.</span>
                            {!! Form::select('topLevelDomain', $domains, null , ['class' => 'form-control', 'v-model' => 'addDomain.topLevelDomain']) !!}
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.add') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
