@extends(WH::theme('layouts.master'), ['title' => trans('applications.change whoplet certificate'), 'sidemenu' => 'website', 'applicationTitle' => trans('applications.website'), 'breadcrumbArray' => 'app:whoplet:changecertificate', 'breadcrumbParam' => [$user, $request->whoplet, $request->secure]])

@section('main-content')

<div class="row" id="changeWhopletCertificateVM"
    data-fetch="{{ route('app:whoplet:show:fetch', $user->hashid) }}"
    data-listcertificate="{{ route('app:whoplet:listcertificate', $user->hashid) }}"
    data-redirect="{{ route('app:whoplet:show', [$user->hashid, 'whoplet' => $request->whoplet, 'secure' => $request->secure]) }}" v-cloak>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="page-header">
          <h3>{{ trans('applications.change whoplet certificate') }}</h3>
        </div>
    </div>

    @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ajax-error></ajax-error>
    </div>


    <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="{{ route('app:whoplet:changecertificate', [$user->hashid, 'whoplet' => $request->whoplet, 'secure' => $request->secure]) }}" method="POST" role="form" @submit.prevent="changeWhopletCertificate">

            <div class="input-group">
                <span class="input-group-btn">
                    <button type="button" class="btn btn-success" v-on:click="chooseCertificate()">{{ trans('form.select_certificate') }}</button>
                </span>
                <input type="text" class="form-control" v-model="changeCertificate.httpsCertificate" readonly>
            </div>

            <button type="submit" class="btn btn-success btn-submit pull-right">{{ trans('form.save') }}</button>
        </form>
    </div>
    @include(WH::theme('partials.modal'), ['modal' => 'universalcertificate'])
</div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
