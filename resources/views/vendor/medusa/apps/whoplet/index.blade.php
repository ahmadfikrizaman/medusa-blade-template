@extends(WH::theme('layouts.master'), ['title' => trans('applications.whoplet'), 'sidemenu' => 'website', 'applicationTitle' => trans('applications.website'), 'breadcrumbArray' => 'app:whoplet', 'breadcrumbParam' => [$user]])

@section('main-content')
    <div class="row" id="whopletVM" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>{{ trans('core.package usage') }}</h3>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <usage-progress
                title="{{ trans('applications.whoplet usage') }}"
                :color="whopletUsage.color"
                :value="whopletUsage.value"
                :max="whopletUsage.max"
                :percentage="whopletUsage.percentage">
            </usage-progress>

            <usage-progress
                title="{{ trans('applications.website quota usage') }}"
                :color="websiteQuotaUsage.color"
                :value="websiteQuotaUsage.value"
                :max="websiteQuotaUsage.max"
                :percentage="websiteQuotaUsage.percentage">
            </usage-progress>

        </div>

         @if ($user->clientPackage->enableRedis)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-info">
                    {{ trans('messages.dedicated redis client info') }}
                </div>
            </div>
        @endif

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ajax-error></ajax-error>
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.my whoplet') }}</h4>
                    <p></p>
                    <table class="table" id="whopletList"
                        data-list="{{ route('app:whoplet:listwhoplet', $user->hashid) }}"
                        data-removedomain="{{ route('app:whoplet:removedomain', $user->hashid) }}"
                        data-delete="{{ route('app:whoplet:destroy', $user->hashid) }}"
                        data-whopletusage="{{ route('clients:getwhopletusage', $user->hashid) }}"
                        data-websitequotausage="{{ route('clients:getwebsitequotausage', $user->hashid) }}"
                        data-manage="{{ route('app:whoplet:show', $user->hashid) }}">

                        <thead>
                            <tr>
                                <th>{{ trans('form.whoplet name') }}</th>
                                <th>{{ trans('applications.basic whoplet info') }}</th>
                                <th class="text-center">{{ trans('form.domain/s') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="whoplet in whoplets">
                                <td><a :href="manageWhoplet(whoplet.whopletName, whoplet.secure.secure)"><span data-toggle="tooltip" data-placement="right" title="{{ trans('applications.whoplet lock meaning') }}" class="fa fa-lock text-danger" v-if="whoplet.secure.secure"></span> @{{ whoplet.whopletName }}</a></td>
                                <td>
                                    <p class="text-primary" data-toggle="tooltip" data-placement="top" title="{{ trans('form.home folder') }}"><i class="fa fa-folder-o"></i> @{{ whoplet.homeFolder }}</p>
                                    <p class="text-primary" data-toggle="tooltip" data-placement="top" title="{{ trans('form.php open_basedir') }}"><i class="fa fa-circle-thin"></i> @{{ whoplet.openBasedir }}</p>
                                    <p class="text-primary" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.php version') }}"><i class="fa fa-circle-thin"></i> @{{ whoplet.phpVersion }}</p>
                                </td>
                                <td>
                                    <p class="text-primary" v-for="domain in whoplet.domains"><i class="fa fa-circle-thin"></i> @{{ domain }}
                                        <a class="fa fa-times" href="#"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="{{ trans('applications.remove this domain') }}"
                                            @click="deleteDomain('{{ trans('messages.confirm delete whoplet domain') }}', whoplet.whopletName, domain)">
                                        </a>
                                    </p>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ trans('form.more') }} <i class="fa fa-chevron-down"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a :href="manageWhoplet(whoplet.whopletName, whoplet.secure.secure)">{{ trans('applications.manage whoplet') }}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'removewhopletdomain'])
        @include(WH::theme('partials.modal'), ['modal' => 'deletewhoplet'])
        @include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
        @include(WH::theme('partials.templates'), ['template' => 'usageProgress'])
    </div>
@stop
