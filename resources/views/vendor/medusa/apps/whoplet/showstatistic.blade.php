@extends(WH::theme('layouts.master'), ['title' => trans('applications.whoplet statistic'), 'sidemenu' => 'whoplet', 'applicationTitle' => $request->whoplet, 'breadcrumbArray' => 'app:whoplet:showstatistic', 'breadcrumbParam' => [$user, $request->whoplet, $request->secure]])

@section('icon')
    <span class="medusa-circle">{{ substr($request->whoplet, 0, 1) }}</span>
@endsection

@section('main-content')
    <div class="row" id="whopletStatisticVM" data-statistic="{{ route('app:whoplet:getstatistic', [$user->hashid, 'whoplet' => $request->whoplet, 'logtype' => $request->logtype]) }}" v-cloak>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.total request per day') }}</h4>
                    <p></p>
                    <div>
                        <canvas id="requestChart" width="100%" height="300"></canvas>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.total error per day') }}</h4>
                    <p></p>
                    <div>
                        <canvas id="errorChart" width="100%" height="300"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop
