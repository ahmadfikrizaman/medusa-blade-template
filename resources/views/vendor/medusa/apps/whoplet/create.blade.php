@extends(WH::theme('layouts.master'), ['title' => trans('applications.add whoplet'), 'sidemenu' => 'website', 'applicationTitle' => trans('applications.website'), 'breadcrumbArray' => 'app:whoplet:create', 'breadcrumbParam' => [$user]])

@section('main-content')
    <div class="row" id="addWhopletVM" v-cloak>

        <check-job
            :is-busy.sync="isBusy"
            title="{{ trans('applications.whoplet creation progress') }}"
            jobname="CreateWhoplet"
            checkurl="{{ route('app:whoplet:checkjob', $user->hashid) }}"
            redirecturl="{{ route('app:whoplet:index', $user->hashid) }}">
        </check-job>

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ajax-error></ajax-error>
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.add whoplet') }}</h4>
                    <p></p>
                    <form action="{{ route('app:whoplet:store', $user->hashid) }}"
                        @submit.prevent="submit"
                        id="formCreateWhoplet"
                        data-listcertificate-url="{{ route('app:whoplet:listcertificate', $user->hashid) }}"
                        data-listphpversion-url="{{ route('app:whoplet:listphpversion', $user->hashid) }}"
                        data-checkurl="{{ route('app:whoplet:checkjob', $user->hashid) }}"
                        method="POST" role="form">

                        <h4>{{ trans('applications.basic settings') }}</h4>

                        <div class="form-group">
                            <label for="">{{ trans('form.whoplet name') }}</label>

                            <div class="input-group">
                                <span class="input-group-addon btn-primary">{{ $user->username }}-</span>
                                <input type="text" class="form-control" name="whopletName" v-model="whoplet.whopletName" placeholder="{{ trans('applications.whoplet name helper') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">{{ trans('form.domain name') }}</label>

                            <div class="input-group">
                                <input type="text" class="form-control" name="domain" v-model="whoplet.domain" placeholder="{{ trans('applications.domain name helper') }}">
                                <span class="input-group-addon btn-primary">.</span>
                                {!! Form::select('topLevelDomain', $domains, null , ['class' => 'form-control', 'v-model' => 'whoplet.topLevelDomain']) !!}
                            </div>
                        </div>

                        <browse-folder
                            label="{{ trans('form.home folder') }}"
                            addon="/var/www/{{ $user->username }}/web"
                            placeholder="{{ trans('applications.whoplet folder helper') }}"
                            model="homeFolder"
                            listmodalid="listFolderModal"
                            createmodalid="createFolderModal"
                            fetchurl="{{ route('app:whoplet:browse', $user->hashid) }}"
                            fetchlistener="applications#ListWhopletFileDirectory#{{ WH::credentialString() }}"
                            createfolderurl="{{ route('app:whoplet:createfolder', $user->hashid) }}"
                            createfolderlistener="applications#NewWhopletFileDirectory#{{ WH::credentialString() }}"
                            :is-busy="isBusy"
                            :is-error="isError"
                            :homefolder.sync="whoplet.homeFolder">
                        </browse-folder>


                        <div class="form-group">
                            <label for="">{{ trans('form.php version') }}</label>

                            <select class="form-control" v-model="whoplet.phpVersion">
                                <option v-for="php in phpVersions" value="@{{ php }}">@{{ php }}</option>
                            </select>
                        </div>

                        <h4>{{ trans('applications.advance settings') }}</h4>

                        <div class="checkbox">
                            <label>
                                <input type="checkbox" v-model="advanceMode">
                                <p>{!! trans('applications.enable whoplet advance mode') !!}</p>
                            </label>
                        </div>

                        @if ($user->clientPackage->enableSsl === 1)
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" v-model="sslMode">
                                    <p>{!! trans('applications.enable whoplet ssl mode') !!}</p>
                                </label>
                            </div>
                        @endif

                        <!-- Advance Mode -->
                        <div v-show="advanceMode">
                            <h4>{{ trans('applications.whoplet security') }}</h4>

                            <browse-folder
                                label="{{ trans('form.php open_basedir') }}"
                                addon="/var/www/{{ $user->username }}/web"
                                placeholder="{{ trans('applications.open_basedir helper') }}"
                                model="openBasedir"
                                listmodalid="listFolderModal2"
                                createmodalid="createFolderModal2"
                                fetchurl="{{ route('app:whoplet:browse', $user->hashid) }}"
                                fetchlistener="applications#ListWhopletFileDirectory#{{ WH::credentialString() }}"
                                createfolderurl="{{ route('app:whoplet:createfolder', $user->hashid) }}"
                                createfolderlistener="applications#NewWhopletFileDirectory#{{ WH::credentialString() }}"
                                :is-busy="isBusy"
                                :is-error="isError"
                                :homefolder.sync="whoplet.openBasedir">
                            </browse-folder>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="clickjackingProtection" v-model="whoplet.clickjackingProtection">
                                    <p>{!! trans('applications.clickjacking protection') !!} <small>({!! trans('applications.clickjacking protection info') !!})</small></p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="xssProtection" v-model="whoplet.xssProtection">
                                    <p>{!! trans('applications.xss protection') !!} <small>({!! trans('applications.xss protection info') !!})</small></p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="preventMimeSniffing" v-model="whoplet.preventMimeSniffing">
                                    <p>{!! trans('applications.prevent mime sniffing') !!} <small>({!! trans('applications.prevent mime sniffing info') !!})</small></p>
                                </label>
                            </div>
                        </div>
                        <!-- End Advance Mode -->


                        <!-- SSL Mode -->
                        <div v-show="sslMode">
                            @if ($user->clientPackage->enableSsl === 1)

                                <h4>{{ trans('applications.whoplet https') }}</h4>

                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" v-on:click="chooseCertificate()">{{ trans('form.select_certificate') }}</button>
                                    </span>
                                    <input type="text" class="form-control" name="httpsCertificate" v-model="whoplet.httpsCertificate" readonly>
                                </div>

                                <div class="checkbox" v-if="whoplet.httpsCertificate !== null && whoplet.hsts == false">
                                    <label>
                                        <input type="checkbox" name="allowHttp" v-model="whoplet.allowHttp">
                                        <p>{!! trans('applications.allow http') !!} <small>({!! trans('applications.allow http info') !!})</small></p>
                                    </label>
                                </div>

                                <div class="checkbox" v-if="whoplet.httpsCertificate !== null && whoplet.allowHttp == false">
                                    <label>
                                        <input type="checkbox" name="hsts" v-model="whoplet.hsts">
                                        <p>{!! trans('applications.hsts') !!} <small>({!! trans('applications.hsts info') !!})</small></p>
                                    </label>
                                </div>
                            @endif
                        </div>
                        <!-- End SSL Mode -->

                        <div class="input-group pull-right">
                            <br>
                            <button type="submit" class="btn btn-primary btn-submit">{{ trans('form.add') }}</button>
                        </div>
                    </form>
                </div>
            </div>

            @include(WH::theme('partials.modal'), ['modal' => 'universalcertificate'])
        </div>
    </div>
    @include(WH::theme('partials.templates'), ['template' => 'browse-folder'])
    @include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
    @include(WH::theme('partials.templates'), ['template' => 'check-job'])
@stop
