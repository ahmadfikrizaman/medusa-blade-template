@extends(WH::theme('layouts.master'), ['title' => trans('applications.whoplet log viewer'), 'sidemenu' => 'whoplet', 'applicationTitle' => $request->whoplet, 'breadcrumbArray' => 'app:whoplet:logviewer', 'breadcrumbParam' => [$user, $request->whoplet, $request->secure]])

@section('icon')
    <span class="medusa-circle">{{ substr($request->whoplet, 0, 1) }}</span>
@endsection

@section('main-content')
    <div class="row" id="whopletLogVM" data-stream="{{ route('app:whoplet:streamlog', [$user->hashid, 'whoplet' => $request->whoplet, 'logtype' => $request->logtype]) }}" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.streaming whoplet log for', ['whoplet' => $request->whoplet, 'logtype' => $request->logtype]) }}</h4>
                    <p v-if="log.length == 0">{{ trans('applications.open your whoplet to live stream this log', ['whoplet' => $request->whoplet]) }}</p>
                    <p v-for="x in log">
                        <i class="fa fa-angle-double-right"></i> @{{ x }}
                    </p>
                </div>
            </div>
        </div>

    </div>
@stop
