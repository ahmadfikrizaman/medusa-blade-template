@extends(WH::theme('layouts.master'), ['title' => trans('applications.manage whoplet name', ['name' => $whoplet]), 'sidemenu' => 'whoplet', 'applicationTitle' => $whoplet, 'breadcrumbArray' => 'app:whoplet:show', 'breadcrumbParam' => [$user, $whoplet, $secure]])

@section('icon')
    <span class="medusa-circle">{{ substr($whoplet, 0, 1) }}</span>
@endsection

@section('main-content')

    <div class="row" id="showWhopletVM"
        data-fetch="{{ route('app:whoplet:show:fetch', $user->hashid) }}"
        data-redirect="{{ route('app:whoplet:index', $user->hashid) }}"
        data-removedomain="{{ route('app:whoplet:removedomain', $user->hashid) }}"
        data-delete="{{ route('app:whoplet:destroy', $user->hashid) }}" v-cloak>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="pull-left">{{ $whoplet }}</h4>
                        <div class="btn-group pull-right">
                            <a href="" class="btn btn-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-cog"></i> </a>
                            <ul class="dropdown-menu">
                                <li><a @click="deleteWhoplet(whoplet.whopletName)">{{ trans('form.destroy') }}</a></li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>

                        <h5 class="list-group-item-heading">{{ trans('form.home folder') }}</h5>
                        <p>@{{ whoplet.homeFolder }}</p>

                        <h5 class="list-group-item-heading">{{ trans('form.php open_basedir') }}</h5>
                        <p>@{{ whoplet.openBasedir }}</p>

                        <h5 class="list-group-item-heading">{{ trans('form.php version') }}</h5>
                        <p>@{{ whoplet.phpVersion }}</p>

                        <h5 class="list-group-item-heading" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.clickjacking protection info') }}">{{ trans('applications.clickjacking protection') }}</h5>
                        <p v-if="whoplet.clickjackingProtection"><i class="fa fa-check"></i> {{ trans('form.enable') }}</p>
                        <p v-else><i class="fa fa-times"></i> {{ trans('form.disable') }}</p>

                        <h5 class="list-group-item-heading" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.xss protection info') }}">{{ trans('applications.xss protection') }}</h5>
                        <p v-if="whoplet.xssProtection"><i class="fa fa-check"></i> {{ trans('form.enable') }}</p>
                        <p v-else><i class="fa fa-times"></i> {{ trans('form.disable') }}</p>

                        <h5 class="list-group-item-heading" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.prevent mime sniffing info') }}">{{ trans('applications.prevent mime sniffing') }}</h5>
                        <p v-if="whoplet.preventMimeSniffing"><i class="fa fa-check"></i> {{ trans('form.enable') }}</p>
                        <p v-else><i class="fa fa-times"></i> {{ trans('form.disable') }}</p>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('applications.domain/s') }}</h4>
                        <table class="table">
                            <tbody>
                                <tr v-for="domain in whoplet.domains">
                                    <td>@{{ domain }}</td>
                                    <td class="text-right">
                                        <a href="http://@{{ domain }}" target="_blank" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.view domain') }}">
                                            <i class="fa fa-external-link"></i>
                                        </a>
                                        <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.remove this domain') }}" @click="deleteDomain('{{ trans('messages.confirm delete whoplet domain') }}', whoplet.whopletName, domain)">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-if="whoplet.secure.secure">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="pull-left">{{ trans('applications.ssl/tls') }}</h4>
                        <div class="clearfix"></div>

                        <h5 class="list-group-item-heading">{{ trans('applications.whoplet certificate') }}</h5>
                        <p>@{{ whoplet.secure.httpsCertificate }}</p>

                        <h5 class="list-group-item-heading">{{ trans('applications.hsts') }}</h5>
                        <p v-if="whoplet.secure.hsts"><i class="fa fa-check"></i> {{ trans('form.enable') }}</p>
                        <p v-else><i class="fa fa-times"></i> {{ trans('form.disable') }}</p>

                        <h5 class="list-group-item-heading">{{ trans('form.php version') }}</h5>
                        <p v-if="whoplet.secure.allowHttp"><i class="fa fa-check"></i> {{ trans('form.enable') }}</p>
                        <p v-else><i class="fa fa-times"></i> {{ trans('form.disable') }}</p>

                    </div>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'removewhopletdomain'])
        @include(WH::theme('partials.modal'), ['modal' => 'deletewhoplet'])
        @include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
    </div>

@stop
