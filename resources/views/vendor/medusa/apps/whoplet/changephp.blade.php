@extends(WH::theme('layouts.master'), ['title' => trans('applications.change php version'), 'sidemenu' => 'whoplet', 'applicationTitle' => $request->whoplet, 'breadcrumbArray' => 'app:whoplet:changephp', 'breadcrumbParam' => [$user, $request->whoplet, $request->secure]])

@section('icon')
    <span class="medusa-circle">{{ substr($request->whoplet, 0, 1) }}</span>
@endsection

@section('main-content')

<div class="row" id="changeWhopletPHPVM"
    data-fetch="{{ route('app:whoplet:show:fetch', $user->hashid) }}"
    data-listphpversion="{{ route('app:whoplet:listphpversion', $user->hashid) }}"
    data-redirect="{{ route('app:whoplet:show', [$user->hashid, 'whoplet' => $request->whoplet, 'secure' => $request->secure]) }}" v-cloak>

    @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

    <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ajax-error></ajax-error>
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.change php version') }}</h4>
                <p></p>
                <form action="{{ route('app:whoplet:changephp:commit', [$user->hashid, 'whoplet' => $request->whoplet]) }}" method="POST" role="form" @submit.prevent="changePHPVersion">
                    <div class="form-group">
                        <label for="">{{ trans('form.php version') }}</label>
                        <select class="form-control" v-model="changePHP.phpVersion">
                            <option v-for="php in phpVersions" value="@{{ php }}">@{{ php }}</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.save') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
