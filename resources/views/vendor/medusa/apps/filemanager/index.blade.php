@extends(WH::theme('layouts.master'), ['title' => trans('applications.WHoP File Manager'), 'sidemenu' => 'file', 'applicationTitle' => trans('applications.file'), 'breadcrumbArray' => 'app:filemanager', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div id="fileManagerVM" v-clock>

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="errorMessages.length != 0">
                <div class="alert alert-danger">
                    <p v-for="error in errorMessages"><i class="fa fa-circle-o"></i> @{{ error }}</p>
                </div>
            </div>

        </div>

        <div class="row margin-top-20">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form class="dropzone" id="dzUpload">

                </form>
            </div>

            @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <br>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" @click="createFileDirectory('file')"><span class="fa fa-file"></span> {{ trans('applications.new file') }}</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" @click="createFileDirectory('folder')"><span class="fa fa-folder"></span> {{ trans('applications.new folder') }}</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" :disabled="structure.parent == structure.currentLocation" @click="fetchList('/')"><span class="fa fa-level-up"></span> {{ trans('applications.top folder') }}</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" :disabled="disableChangePermission" @click="changePermission()"><span class="fa fa-wrench"></span> {{ trans('applications.change permission') }}</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" :disabled="disableRename" @click="renameFile()"><span class="fa fa-edit"></span> {{ trans('applications.rename') }}</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" :disabled="disableEdit" @click="edit"><span class="fa fa-pencil"></span> {{ trans('applications.edit') }}</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" :disabled="disableDelete" @click="deleteFile()"><span class="fa fa-trash"></span> {{ trans('applications.delete') }}</button>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <br>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="pull-left">{{ trans('applications.WHoP File Manager') }}</h4>
                        <div class="pull-right">
                        </div>
                        <div class="clearfix"></div>
                        <p> /var/www/{{ $user->username }}@{{ structure.currentLocation }}</p>
                        <table v-show="!isBusy" class="table" id="fmTable"
                            data-list="{{ route('app:filemanager:list', $user->hashid) }}"
                            data-newfiledirectory="{{ route('app:filemanager:newfiledirectory', $user->hashid) }}"
                            data-upload="{{ route('app:filemanager:upload', $user->hashid) }}"
                            data-rename="{{ route('app:filemanager:rename', $user->hashid) }}"
                            data-delete="{{ route('app:filemanager:delete', $user->hashid) }}"
                            data-permission="{{ route('app:filemanager:changepermission', $user->hashid) }}"
                            data-edit="{{ route('app:filemanager:edit', $user->hashid) }}">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('applications.file/folder name') }}</th>
                                    <th>{{ trans('applications.size') }}</th>
                                    <th>{{ trans('applications.type') }}</th>
                                    <th>{{ trans('applications.permission') }}</th>
                                    <!--<th></th>-->
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-show="structure.parent != structure.currentLocation" @dblclick="goToParent()">
                                    <td colspan="5">../</td>
                                </tr>
                                <tr v-show="isError">
                                    <td colspan="5">@{{ structure.message }}</td>
                                </tr>
                                <tr v-else="isBusy && isError" v-for="data in structure.content" @dblclick="walk(data)" class="directory">
                                    <td>
                                        <span class="fa fa-folder" v-show="data.type == 'inode/directory'"></span>
                                        <span class="fa fa-file-o" v-else="data.type == 'inode/directory'"></span>
                                    </td>
                                    <td>@{{ data.name }}</td>
                                    <td>@{{ data.size }} KiB</td>
                                    <td>@{{ data.type }}</td>
                                    <td>@{{ data.permission }}</td>
                                    <!--td>
                                        <div class="btn-group">
                                            <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#" :disabled="disableRename" @click="renameFile()"> {{ trans('applications.rename') }}</a>
                                                    <a href="#" :disabled="disableEdit" @click="edit"> {{ trans('applications.edit') }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td-->
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'createFile'])
        @include(WH::theme('partials.modal'), ['modal' => 'createFolder'])
        @include(WH::theme('partials.modal'), ['modal' => 'renameFileFolder'])
        @include(WH::theme('partials.modal'), ['modal' => 'deleteFileFolder'])
        @include(WH::theme('partials.modal'), ['modal' => 'changePermission'])
    </div>

@stop
