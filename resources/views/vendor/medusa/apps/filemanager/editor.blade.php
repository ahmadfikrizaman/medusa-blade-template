@extends(WH::theme('layouts.editor'), ['title' => trans('applications.WHoP File Editor'), 'applicationTitle' => trans('applications.WHoP File Editor'), 'sidemenu' => false, 'breadcrumbArray' => 'app:filemanager:editor', 'breadcrumbParam' => [$user]])

@section('content')

    <br>
    <div class="row" id="fmEditVM"
        data-editor="{{ route('app:filemanager:editor', $user->hashid) }}"
        data-save="{{ route('app:filemanager:save', $user->hashid) }}"
        data-gettheme="{{ route('app:filemanager:geteditortheme', $user->hashid) }}"
        data-savetheme="{{ route('app:filemanager:editortheme', $user->hashid) }}" v-clock>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.editing file') }}: @{{ editingFile }}</h4>
                    <div class="btn-group pull-right">
                        <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog fa-2x"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#" @click="changeMode" :disabled="isBusy">{{ trans('applications.editor mode') }}</a>
                                <a href="#" @click="changeTheme" :disabled="isBusy">{{ trans('applications.editor theme') }}</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="editor"></div>
                <div class="panel-footer">
                    <button type="button" class="btn btn-primary pull-right" @click="save" :disabled="isBusy">{{ trans('form.save') }}</button>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'editorMode'])
        @include(WH::theme('partials.modal'), ['modal' => 'editorTheme'])
    </div>

@stop
