@extends(WH::theme('layouts.webmail'), ['title' => trans('applications.add email forwarding'), 'sidemenu' => 'webmail', 'applicationTitle' => trans('applications.webmail')])

@section('main-content')

    <div class="row" id="webmailForwardingVM" v-clock>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.add email forwarding') }}</h4>
                    <p></p>
                    <form action="{{ route('webmail:forwarding:store') }}" method="POST" role="form">
                        @include(WH::theme('apps.email.forwarding.form'), ['edit' => true, 'saveText' => trans('form.create')])
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
