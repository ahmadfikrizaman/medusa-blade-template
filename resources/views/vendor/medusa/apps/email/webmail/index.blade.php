@extends(WH::theme('layouts.webmail'), ['title' => trans('applications.webmail'), 'sidemenu' => 'webmail', 'applicationTitle' => trans('applications.webmail')])

@section('main-content')

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.email settings information') }}</h4>
                    <a type="button" class="btn btn-primary pull-right" href="{{ route('webmail:password') }}">{{ trans('form.change password') }}</a>
                    <div class="clearfix"></div>

                    <h5 class="list-group-item-heading">{{ trans('form.email') }}</h5>
                    <p>{{ $user->email }}</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.imap server') }}</h5>
                    <p>{{ app('EmailService')->getDefaultEmailServer() }}</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.imap port') }}</h5>
                    <p>993</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.imap connection security') }}</h5>
                    <p>{{ trans('applications.ssl/tls') }}</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.imap authentication method') }}</h5>
                    <p>{{ trans('applications.normal password') }}</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.smtp server') }}</h5>
                    <p>{{ app('EmailService')->getDefaultEmailServer() }}</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.smtp port') }}</h5>
                    <p>587</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.smtp connection security') }}</h5>
                    <p>{{ trans('applications.starttls') }}</p>

                    <h5 class="list-group-item-heading">{{ trans('applications.smtp authentication method') }}</h5>
                    <p>{{ trans('applications.normal password') }}</p>
                </div>
            </div>
        </div>
    </div>

@stop
