@extends(WH::theme('layouts.webmail'), ['title' => trans('applications.webmail'), 'sidemenu' => 'webmail', 'applicationTitle' => trans('applications.webmail')])

@section('main-content')

    <div class="row" id="changeWebmailPasswordVM" v-clock>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('form.change password') }}</h4>
                    <p></p>
                    <form action="{{ route('webmail:password:update') }}" method="POST" role="form">

                        {{ csrf_field() }}

                        <password-generator
                            :password.sync="password"
                            :verifypassword.sync="verifyPassword"
                            :passwordlength="16"
                            modalid="modalGeneratePassword">
                        </password-generator>

                        @include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])

                        <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.update') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
@stop
