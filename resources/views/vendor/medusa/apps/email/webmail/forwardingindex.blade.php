@extends(WH::theme('layouts.webmail'), ['title' => trans('applications.email forwarding'), 'sidemenu' => 'webmail', 'applicationTitle' => trans('applications.webmail')])

@section('main-content')

    <div class="row" id="webmailForwardingVM" v-clock>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include(WH::theme('partials.messages'))
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.email forwarding list') }}</h4>
                    <p></p>
                    <table id="forwardingList" class="table" data-url="{{ route('webmail:forwarding:api') }}">
                        <thead>
                            <tr>
                                <th>{{ trans('form.forward from') }}</th>
                                <th>{{ trans('form.forward to') }}</th>
                                <th class="text-center">{{ trans('form.status') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="forwarding in forwardings">
                                <td>@{{ forwarding['from'] }}</td>
                                <td>@{{ forwarding['to'] }}</td>
                                <td class="text-center">
                                    <a v-if="forwarding['status'] == 0" href="@{{ forwarding['enabledisable'] }}" class="label label-success" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.click to disable') }}">{{ trans('form.active') }}</a>
                                    <a v-if="forwarding['status'] == 1" href="@{{ forwarding['enabledisable'] }}" class="label label-danger" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.click to activate') }}">{{ trans('form.inactive') }}</a>
                                </td>
                                <td class="text-center">
                                    <a class="text-danger" data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="delete( forwarding['delete'], '{{ trans('messages.confirm delete forwarding') }}')"><span class="fa fa-trash"></span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])
    </div>

@stop
