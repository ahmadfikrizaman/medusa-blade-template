@extends(WH::theme('layouts.master'), ['title' => trans('applications.email forwarding'), 'sidemenu' => 'email', 'applicationTitle' => trans('applications.email'), 'breadcrumbArray' => 'app:ef', 'breadcrumbParam' => [$user]])

@section('main-content')

<div class="row" id="emailForwardingVM">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
        <div class="panel panel-default">
            <div class="panel-body">
                <h4 class="pull-left">{{ trans('applications.email forwarding list') }}</h4>
                <div class="clearfix"></div>
                <p></p>
                <table id="forwardingList" class="table" data-url="{{ route('app:ef:api', [$user->hashid]) }}">
                    <thead>
                        <tr>
                            <th>{{ trans('form.forward from') }}</th>
                            <th>{{ trans('form.forward to') }}</th>
                            <th class="text-center"> &nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="forwarding in forwardings">
                            <td>
                                <i v-if="forwarding['status'] == 0" class="fa fa-circle text-success" data-toggle="tooltip" data-placement="top" title="{{ trans('form.active') }}"></i>
                                <i v-if="forwarding['status'] == 1" class="fa fa-circle text-danger" data-toggle="tooltip" data-placement="top" title="{{ trans('form.inactive') }}"></i>
                                @{{ forwarding['from'] }}
                            </td>
                            <td>@{{ forwarding['to'] }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ trans('form.more') }} <i class="fa fa-chevron-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a v-if="forwarding['status'] == 0" href="@{{ forwarding['enabledisable'] }}">{{ trans('form.disable') }}</a>
                                            <a v-if="forwarding['status'] == 1" href="@{{ forwarding['enabledisable'] }}">{{ trans('form.enable') }}</a>
                                        </li>
                                        <li><a data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="delete( forwading['delete'], '{{ trans('messages.confirm delete forwarfing') }}')">{{ trans('form.delete') }}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])
</div>

@stop
