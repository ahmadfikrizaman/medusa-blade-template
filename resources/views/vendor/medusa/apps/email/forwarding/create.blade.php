@extends(WH::theme('layouts.master'), ['title' => trans('applications.add email forwarding'), 'sidemenu' => 'email', 'applicationTitle' => trans('applications.email'), 'breadcrumbArray' => 'app:ef:create', 'breadcrumbParam' => [$user]])

@section('main-content')

<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="alert alert-info">
            {{ trans('applications.create forwarding info') }}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.add email forwarding') }}</h4>
                <p></p>
                <form action="{{ route('app:ef:store', [$user->hashid]) }}" method="POST" role="form">
                    @include(WH::theme('apps.email.forwarding.form'), ['saveText' => trans('form.save'), 'edit' => true])
                </form>
            </div>
        </div>
    </div>
</div>

@stop
