{{ csrf_field() }}

<div class="form-group">
    <label for="">{{ trans('form.forward from') }}</label>
    <div class="input-group">
        <input type="text" class="form-control" name="username" value="{{ $email->username or old('username') }}" placeholder="{{ trans('form.username') }}" v-bind:disabled="{{ $edit }}">
        <span class="input-group-addon btn-primary">@</span>
        {!! Form::select('domainEmail', $domains, old('domainEmail') , ['class' => 'form-control', 'v-bind:disabled' => $edit]) !!}
    </div>
</div>

<div class="form-group">
    <label for="">{{ trans('form.forward to') }}</label>
    <input type="email" class="form-control" name="forwardTo" value="{{ old('forwardTo') }}" placeholder="{{ trans('form.forward to') }}">
</div>

<button type="submit" class="btn btn-primary btn-submit pull-right">{{ $saveText }}</button>
