@extends(WH::theme('layouts.master'), ['title' => trans('applications.add email'), 'sidemenu' => 'email', 'applicationTitle' => trans('applications.email'), 'breadcrumbArray' => 'app:email:create', 'breadcrumbParam' => [$user]])

@section('main-content')


<div class="row" id="emailVM" v-clock>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.add email') }}</h4>
                <p></p>
                <form action="{{ route('app:email:store', [$user->hashid]) }}" method="POST" role="form">
                    @include(WH::theme('apps.email.account.form'), ['saveText' => trans('form.add'), 'edit' => false])
                </form>
            </div>
        </div>
    </div>
</div>

@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
@stop
