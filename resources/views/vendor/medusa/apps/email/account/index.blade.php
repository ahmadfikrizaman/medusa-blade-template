@extends(WH::theme('layouts.master'), ['title' => trans('applications.email account'), 'sidemenu' => 'email', 'applicationTitle' => trans('applications.email'), 'breadcrumbArray' => 'app:email', 'breadcrumbParam' => [$user]])

@section('main-content')

<div class="row" id="emailVM">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3>{{ trans('core.package usage') }}</h3>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <usage-progress
            title="{{ trans('applications.email usage') }}"
            color="{{ $mailUsage['color'] }}"
            value="{{ $mailUsage['value'] }}"
            max="{{ $mailUsage['max'] }}"
            percentage="{{ $mailUsage['percentage'] }}">
        </usage-progress>

        <usage-progress
            title="{{ trans('applications.email quota usage') }}"
            color="{{ $mailQuotaUsage['color'] }}"
            value="{{ $mailQuotaUsage['value'] }}"
            max="{{ $mailQuotaUsage['max'] }}"
            percentage="{{ $mailQuotaUsage['percentage'] }}">
        </usage-progress>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default" v-show="emails.length">
            <div class="panel-body">
                <h4 class="pull-left">{{ trans('applications.email list') }}</h4>
                <div class="clearfix"></div>
                <p></p>
                <table id="emailList" class="table" data-url="{{ route('app:email:api', $user->hashid) }}">
                    <thead>
                        <tr>
                            <th>{{ trans('form.email') }}</th>
                            <th class="text-center">{{ trans('form.email quota') }}</th>
                            <th class="text-center"> &nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="email in emails">
                            <td>
                                <i v-if="email['status'] == 0" class="fa fa-circle text-success" data-toggle="tooltip" data-placement="top" title="{{ trans('form.active') }}"></i>
                                <i v-if="email['status'] == 1" class="fa fa-circle text-danger" data-toggle="tooltip" data-placement="top" title="{{ trans('form.inactive') }}"></i>
                                @{{ email['email'] }}
                            </td>
                            <td class="text-center">@{{ email['quota'] }} MB</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ trans('form.more') }} <i class="fa fa-chevron-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="@{{ email['edit'] }}">{{ trans('form.edit') }}</a></li>
                                        <li><a @click="showSetting(email['settings'])">{{ trans('form.setting') }}</a></li>
                                        <li>
                                            <a v-if="email['status'] == 0" href="@{{ email['enabledisable'] }}">{{ trans('form.disable') }}</a>
                                            <a v-if="email['status'] == 1" href="@{{ email['enabledisable'] }}">{{ trans('form.enable') }}</a>
                                        </li>
                                        <li><a data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="delete( email['delete'], '{{ trans('messages.confirm delete email') }}')">{{ trans('form.delete') }}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default" v-show="!emails.length && !isBusy">
            <div class="panel-body text-center">
                <div class="row">
                    <div class="col-md-offset-4 col-md-4">
                        <i class="fa fa-envelope-o fa-5x"></i>
                    </div>
                </div>
                <h4><strong>Getting started with email account.</strong></h4>
                <p>You can create new email account by clicking button below.</p>
                <button class="btn btn-primary">Add new email</button>
            </div>
        </div>
    </div>

    @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])
    @include(WH::theme('partials.modal'), ['modal' => 'viewEmailSetting'])
</div>
@include(WH::theme('partials.templates'), ['template' => 'usageProgress'])
@stop
