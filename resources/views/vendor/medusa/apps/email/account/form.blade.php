{{ csrf_field() }}

<div class="form-group">
    <label for="">{{ trans('form.email account') }}</label>
    <div class="input-group">
        <input type="text" class="form-control" name="username" value="{{ $email->username or old('username') }}" placeholder="{{ trans('form.username') }}" v-bind:disabled="{{ $edit }}">
        <span class="input-group-addon btn-primary">@</span>
        {!! Form::select('domainEmail', $domains, isset($email) ? $email->record_id : old('domainEmail') , ['class' => 'form-control', 'v-bind:disabled' => $edit]) !!}
    </div>
</div>


<password-generator
    :password.sync="password"
    :verifypassword.sync="verifyPassword"
    :passwordlength="16"
    modalid="modalGeneratePassword">
</password-generator>


<div class="form-group" data-toggle="tooltip" data-placement="top" title="{{ trans('applications.email quota info', ['quota' => $maxQuota]) }}">
    <label for="">{{ trans('form.email quota') }}</label>
    <input type="number" class="form-control" name="quota" value="{{ $email->quota or old('quota') }}" placeholder="{{ trans('form.quota placeholder') }}">
</div>


<button type="submit" class="btn btn-primary pull-right btn-submit">{{ $saveText }}</button>
