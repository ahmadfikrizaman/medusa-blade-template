@extends(WH::theme('layouts.master'), ['title' => trans('applications.domain name'), 'sidemenu' => 'domain', 'applicationTitle' => trans('applications.domain'), 'breadcrumbArray' => 'app:domain', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="domainVM" v-clock>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>{{ trans('core.package usage') }}</h3>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <usage-progress
                title="{{ trans('applications.domain usage') }}"
                color="{{ $domainUsage['color'] }}"
                value="{{ $domainUsage['value'] }}"
                max="{{ $domainUsage['max'] }}"
                percentage="{{ $domainUsage['percentage'] }}">
            </usage-progress>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.domain name') }}</h4>
                    <p></p>
                    <table id="domainList" class="table" data-url="{{ route('app:domain:api', $user->hashid) }}">
                        <thead>
                            <tr>
                                <th>{{ trans('applications.domain name') }}</th>
                                <th class="text-center">{{ trans('applications.dns records') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="domain in domains">
                                <td>@{{ domain['name'] }}</td>
                                <td class="text-center">
                                    <a href="@{{ domain['records'] }}"><span class="fa fa-briefcase"></span></a>
                                </td>
                                <td class="text-center">
                                    <a v-if="domain['deleteEdit'] == 1" class="text-danger" data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="delete( domain['delete'], '{{ trans('messages.confirm delete domain') }}')"><i class="fa fa-trash"></i></a>
                                    <a v-if="domain['deleteEdit'] == 0" class="text-danger" disabled><i class="fa fa-ban"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])

    </div>
@include(WH::theme('partials.templates'), ['template' => 'usageProgress'])
@stop
