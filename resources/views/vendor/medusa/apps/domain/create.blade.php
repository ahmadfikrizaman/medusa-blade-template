@extends(WH::theme('layouts.master'), ['title' => trans('applications.domain name'), 'sidemenu' => 'domain', 'applicationTitle' => trans('applications.domain'), 'breadcrumbArray' => 'app:domain:create', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.add domain') }}</h4>
                    <p></p>
                    <form action="{{ route('app:domain:store', [$user->hashid]) }}" method="POST" role="form">

                        {{ csrf_field() }}


                        <div class="form-group">
                            <label for="">{{ trans('form.domain name') }}</label>
                            <input type="text" class="form-control" name="domainName" placeholder="example.com" value="{{ old('domainName') }}">
                        </div>

                        <button type="submit" class="btn btn-primary pull-right btn-submit">{{ trans('form.add') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
