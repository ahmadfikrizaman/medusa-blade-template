{{ csrf_field() }}

<div class="form-group">
    <label for="">{{ trans('form.username') }}</label>
    <div class="input-group">
        <span class="input-group-addon btn-primary">{{ $user->username }}_</span>
        <input type="text" class="form-control" name="username" value="{{ isset($mariauser) ? $mariauser : old('username') }}" placeholder="{{ trans('form.username') }}" v-bind:disabled="{{ $edit }}">
    </div>
</div>

<password-generator
    :password.sync="password"
    :verifypassword.sync="verifyPassword"
    :passwordlength="16"
    modalid="modalGeneratePassword">
</password-generator>

<button type="submit" class="btn btn-primary btn-submit pull-right">{{ $saveText }}</button>

@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
