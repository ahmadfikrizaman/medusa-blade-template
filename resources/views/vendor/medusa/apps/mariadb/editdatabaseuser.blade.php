@extends(WH::theme('layouts.master'), ['title' => trans('applications.edit database user'), 'sidemenu' => 'database', 'applicationTitle' => trans('applications.database'), 'breadcrumbArray' => 'app:mariadb:edit:user', 'breadcrumbParam' => [$user, $mariauser]])

@section('main-content')

    <div class="row" id="mariaDB" v-cloak>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.edit database user') }}</h4>
                    <div class="clearfix"></div>
                    <p></p>
                    <form action="{{ route('app:mariadb:changepassword:commit', [$user->hashid, $mariauser]) }}" method="POST" role="form">
                        @include(WH::theme('apps.mariadb.createuserform'), ['saveText' => trans('form.update'), 'edit' => true])
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
