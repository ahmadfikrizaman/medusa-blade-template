@extends(WH::theme('layouts.master'), ['title' => trans('applications.add database'), 'sidemenu' => 'database', 'applicationTitle' => trans('applications.database'), 'breadcrumbArray' => 'app:mariadb:create:database', 'breadcrumbParam' => [$user]])

@section('main-content')

<div  class="row" id="mariaDB" v-clock>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.add database') }}</h4>
                <p></p>
                <form action="{{ route('app:mariadb:store:database', [$user->hashid]) }}" method="POST" role="form">

                    {{ csrf_field() }}

                   <div class="form-group">
                        <label for="">{{ trans('applications.database name') }}</label>
                        <div class="input-group">
                            <span class="input-group-addon btn-primary">{{ $user->username }}_</span>
                            <input type="text" class="form-control" name="databaseName" value="{{ old('databaseName') }}" placeholder="{{ trans('applications.database name') }}">
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <a href="{{ route('app:mariadb:index', $user->hashid) }}" class="btn btn-default">{{ trans('form.cancel') }}</a>
                        <button type="submit" class="btn btn-primary btn-submit">{{ trans('form.add') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
