@extends(WH::theme('layouts.master'), ['title' => trans('applications.mariadb database'), 'sidemenu' => 'database', 'applicationTitle' => trans('applications.database'), 'breadcrumbArray' => 'app:mariadb', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="mariaDB" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>{{ trans('core.package usage') }}</h3>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <usage-progress
                title="{{ trans('applications.database usage') }}"
                color="{{ $databaseUsage['color'] }}"
                value="{{ $databaseUsage['value'] }}"
                max="{{ $databaseUsage['max'] }}"
                percentage="{{ $databaseUsage['percentage'] }}">
            </usage-progress>
            <usage-progress
                title="{{ trans('applications.database user usage') }}"
                color="{{ $databaseUserUsage['color'] }}"
                value="{{ $databaseUserUsage['value'] }}"
                max="{{ $databaseUserUsage['max'] }}"
                percentage="{{ $databaseUserUsage['percentage'] }}">
            </usage-progress>
        </div>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.database list') }}</h4>
                    <div class="clearfix"></div>
                    <p></p>
                    <table id="databaseList" class="table" data-url="{{ route('app:mariadb:api', $user->hashid) }}">
                        <thead>
                            <tr>
                                <th>{{ trans('applications.database name') }}</th>
                                <th>{{ trans('applications.database size') }}</th>
                                <th>{{ trans('applications.database user') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="data in databases">
                                <td>@{{ data['database'] }}</td>
                                <td>@{{ data['size'] }}</td>
                                <td>
                                    <span v-for="user in data['user']" data-toggle="tooltip" data-position="top" data-delay="50" title="{{ trans('messages.tooltip_revoke_user_from_database') }}">
                                        @{{ user['realname'] }} <a data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="revoke( user['revoke'], '{{ trans('messages.confirm revoke user from database') }}')"><span class="fa fa-times"></span></a><br>
                                    </span>

                                    <span v-if="data['user'].length == 0">
                                        {{ trans('applications.no user in database') }}
                                    </span>
                                </td>
                                <td class="text-center">
                                    <a class="text-danger" data-toggle="modal" data-backdrop="static" @click="delete(data['delete'], '{{ trans('messages.confirm delete database') }}')" href='#universalPostModal'><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.database user list') }}</h4>
                    <div class="clearfix"></div>
                    <p></p>
                    <table id="databaseUserList" class="table" data-url="{{ route('app:mariadb:userapi', $user->hashid) }}">
                        <thead>
                            <tr>
                                <th>{{ trans('form.username') }}</th>
                                <th class="text-center">{{ trans('form.change password') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="user in databaseUsers">
                                <td>
                                    @{{ user['realname'] }}
                                </td>
                                <td class="text-center">
                                    <a class="text-warning" href="@{{ user['changepassword'] }}"><span class="fa fa-edit"></span></a>
                                </td>
                                <td class="text-center">
                                    <a class="text-danger" data-toggle="modal" data-backdrop="static" @click="delete( user['delete'], '{{ trans('messages.confirm delete database user') }}')" href='#universalPostModal'><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'changeMysqlPassword'])
        @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])

    </div>

@include(WH::theme('partials.templates'), ['template' => 'usageProgress'])
@stop
