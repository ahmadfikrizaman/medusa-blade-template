@extends(WH::theme('layouts.master'), ['title' => trans('applications.assign user to database'), 'sidemenu' => 'database', 'applicationTitle' => trans('applications.database'), 'breadcrumbArray' => 'app:mariadb:create:assign', 'breadcrumbParam' => [$user]])

@section('main-content')

<div class="row" id="mariaDB">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.assign user to database') }}</h4>
                <p></p>
                <form action="{{ route('app:mariadb:assign', [$user->hashid]) }}" method="POST" role="form">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="">{{ trans('applications.assign user') }}</label>
                        <select name="databaseUser" class="form-control" required="required">
                            <option disabled selected>{{ trans('applications.choose database user') }}</option>
                            @foreach ($databaseUserWithoutUsername as $databaseUser)
                                <option value="{{ $databaseUser['strip'] }}">{{ $databaseUser['realname'] }}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="">{{ trans('applications.to database') }}</label>
                        <select name="database" class="form-control" required="required">
                            <option disabled selected>{{ trans('applications.choose database') }}</option>
                            @foreach ($databaseWithoutUsername as $databaseUser)
                                <option value="{{ $databaseUser['strip'] }}">{{ $databaseUser['realname'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">{{ trans('applications.with permission') }}</label>
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2">
                                    <p>
                                        <input type="checkbox" id="selectAll" onClick="toggle(this)"/>
                                        <label for="selectAll">Select All</label>
                                    </p>
                                </th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <input type="checkbox" id="SELECT" name="permission[]" value="SELECT"/>
                                    <label for="SELECT">SELECT</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="INSERT" name="permission[]" value="INSERT"/>
                                    <label for="INSERT">INSERT</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="UPDATE" name="permission[]" value="UPDATE"/>
                                    <label for="UPDATE">UPDATE</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="DELETE" name="permission[]" value="DELETE"/>
                                    <label for="DELETE">DELETE</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="CREATE" name="permission[]" value="CREATE"/>
                                    <label for="CREATE">CREATE</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="ALTER" name="permission[]" value="ALTER"/>
                                    <label for="ALTER">ALTER</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="INDEX" name="permission[]" value="INDEX"/>
                                    <label for="INDEX">INDEX</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="DROP" name="permission[]" value="DROP"/>
                                    <label for="DROP">DROP</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="CREATE-TEMPORARY-TABLE" name="permission[]" value="CREATE-TEMPORARY-TABLE"/>
                                    <label for="CREATE-TEMPORARY-TABLE">CREATE TEMPORARY TABLE</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="SHOW-VIEW" name="permission[]" value="SHOW-VIEW"/>
                                    <label for="SHOW-VIEW">SHOW VIEW</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="CREATE-ROUTINE" name="permission[]" value="CREATE-ROUTINE"/>
                                    <label for="CREATE-ROUTINE">CREATE ROUTINE</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="ALTER-ROUTINE" name="permission[]" value="ALTER-ROUTINE"/>
                                    <label for="ALTER-ROUTINE">ALTER ROUTINE</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="EXECUTE" name="permission[]" value="EXECUTE"/>
                                    <label for="EXECUTE">EXECUTE</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="CREATE-VIEW" name="permission[]" value="CREATE-VIEW"/>
                                    <label for="CREATE-VIEW">CREATE VIEW</label>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <input type="checkbox" id="EVENT" name="permission[]" value="EVENT"/>
                                    <label for="EVENT">EVENT</label>
                                </td>
                                <td>
                                    <input type="checkbox" id="TRIGGER" name="permission[]" value="TRIGGER"/>
                                    <label for="TRIGGER">TRIGGER</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>



                    <button type="submit" class="btn btn-primary pull-right">{{ trans('form.assign') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
@stop
