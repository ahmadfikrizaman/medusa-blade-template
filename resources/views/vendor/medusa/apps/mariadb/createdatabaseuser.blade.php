@extends(WH::theme('layouts.master'), ['title' => trans('applications.add database user'), 'sidemenu' => 'database', 'applicationTitle' => trans('applications.database'), 'breadcrumbArray' => 'app:mariadb:create:user', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="mariaDB" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.add database user') }}</h4>
                    <p></p>
                    <form action="{{ route('app:mariadb:store:user', [$user->hashid]) }}" method="POST" role="form">
                        @include(WH::theme('apps.mariadb.createuserform'), ['saveText' => trans('form.add'), 'edit' => false])
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
