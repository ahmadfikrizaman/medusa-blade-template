@extends(WH::theme('layouts.master'), ['title' => trans('applications.crontab manager'), 'sidemenu' => 'advance setting', 'applicationTitle' => trans('applications.advance settings'), 'breadcrumbArray' => 'app:cron', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="cronVM"
        data-list="{{ route('app:cron:list', $user->hashid) }}"
        data-delete="{{ route('app:cron:destroy', $user->hashid) }}" v-clock>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('applications.cron job') }}</h4>
                        <p></p>
                        <table class="table" id="jobTableList">
                            <thead>
                                <tr>
                                    <th class="text-center">{{ trans('form.minute') }}</th>
                                    <th class="text-center">{{ trans('form.hour') }}</th>
                                    <th class="text-center">{{ trans('form.day') }}</th>
                                    <th class="text-center">{{ trans('form.month') }}</th>
                                    <th class="text-center">{{ trans('form.weekday') }}</th>
                                    <th >{{ trans('form.command') }}</th>
                                    <th class="text-center">{{ trans('form.delete') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="job in jobs">
                                    <td class="text-center">@{{ job.minute }}</td>
                                    <td class="text-center">@{{ job.hour }}</td>
                                    <td class="text-center">@{{ job.day }}</td>
                                    <td class="text-center">@{{ job.month }}</td>
                                    <td class="text-center">@{{ job.weekday }}</td>
                                    <td>@{{ job.command }}</td>
                                    <td class="text-center"><a class="text-danger" @click="deleteJob(job.id)"><span class="fa fa-trash"></span></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'deletecron'])
        @include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
    </div>

@stop
