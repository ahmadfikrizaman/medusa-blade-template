@extends(WH::theme('layouts.master'), ['title' => trans('applications.add cron job'), 'sidemenu' => 'advance setting', 'applicationTitle' => trans('applications.advance settings'), 'breadcrumbArray' => 'app:cron:create', 'breadcrumbParam' => [$user]])


@section('main-content')

    <div class="row" id="addCronVM">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.add cron job') }}</h4>
                    <p></p>
                    <form action="" method="POST" role="form">

                        <div class="form-group">
                            <label for="">{{ trans('form.minute') }}</label>
                            <input type="text" class="form-control" name="minute" v-model="cron.minute">
                        </div>

                        <div class="form-group">
                            <label for="">{{ trans('form.predefined settings') }}</label>
                            <select id="input" class="form-control" v-model="cron.minute">
                                <option value="*" selected>{{ trans_choice('form.every number minute', 1, ['number' => 1]) }} (*)</option>
                                <option value="*/2">{{ trans_choice('form.every number minute', 2, ['number' => 2]) }} (*/2)</option>
                                <option value="*/5">{{ trans_choice('form.every number minute', 5, ['number' => 5]) }} (*/5)</option>
                                <option value="*/10">{{ trans_choice('form.every number minute', 10, ['number' => 10]) }} (*/10)</option>
                                <option value="*/15">{{ trans_choice('form.every number minute', 15, ['number' => 15]) }} (*/15)</option>
                                <option value="0,30">{{ trans_choice('form.every number minute', 30, ['number' => 30]) }} (0,30)</option>
                                <option value="" disabled>{{ trans('form.custom setting') }}</option>
                                @for ($i=0; $i < 60; $i++)
                                    <option value="{{$i}}">:{{ $i }} ({{$i}})</option>
                                @endfor
                            </select>
                        </div>

                        <hr>


                        <div class="form-group">
                            <label for="">{{ trans('form.hour') }}</label>
                            <input type="text" class="form-control" name="hour" v-model="cron.hour">
                        </div>

                        <div class="form-group">
                            <label for="">{{ trans('form.predefined settings') }}</label>
                            <select id="input" class="form-control" v-model="cron.hour">
                                <option value="*" selected>{{ trans_choice('form.every number hour', 1, ['number' => 1]) }} (*)</option>
                                <option value="*/2">{{ trans_choice('form.every number hour', 2, ['number' => 2]) }} (*/2)</option>
                                <option value="*/3">{{ trans_choice('form.every number hour', 3, ['number' => 3]) }} (*/3)</option>
                                <option value="*/5">{{ trans_choice('form.every number hour', 5, ['number' => 5]) }} (*/5)</option>
                                <option value="*/10">{{ trans_choice('form.every number hour', 10, ['number' => 10]) }} (*/10)</option>
                                <option value="0,12">{{ trans_choice('form.every number hour', 12, ['number' => 12]) }} (0,12)</option>
                                <option value="" disabled>{{ trans('form.custom setting') }}</option>
                                @for ($i=0; $i < 24; $i++)
                                    <option value="{{$i}}">{{ trans_choice('form.number hour', $i, ['number' => $i]) }} ({{$i}}.00)</option>
                                @endfor
                            </select>
                        </div>

                        <hr>

                        <div class="form-group">
                            <label for="">{{ trans('form.day') }}</label>
                            <input type="text" class="form-control" name="day" v-model="cron.day">
                        </div>

                        <div class="form-group">
                            <label for="">{{ trans('form.predefined settings') }}</label>
                            <select id="input" class="form-control" v-model="cron.day">
                                <option value="*" selected>{{ trans_choice('form.every number day', 1, ['number' => 1]) }} (*)</option>
                                <option value="*/2">{{ trans_choice('form.every number day', 2, ['number' => 2]) }} (*/2)</option>
                                <option value="*/7">{{ trans_choice('form.every number day', 7, ['number' => 7]) }} (*/7)</option>
                                <option value="*/15">{{ trans_choice('form.every number day', 15, ['number' => 15]) }} (*/15)</option>
                                <option value="1,15,30">{{ trans_choice('form.every number day', 15, ['number' => '1,15,30']) }} (1,15,30)</option>
                                <option value="" disabled>{{ trans('form.custom setting') }}</option>
                                @for ($i=1; $i <= 31; $i++)
                                    <option value="{{$i}}">{{ trans_choice('form.number day', $i, ['number' => $i]) }} ({{$i}})</option>
                                @endfor
                            </select>
                        </div>

                        <hr>


                        <div class="form-group">
                            <label for="">{{ trans('form.month') }}</label>
                            <input type="text" class="form-control" name="month" v-model="cron.month">
                        </div>

                        <div class="form-group">
                            <label for="">{{ trans('form.predefined settings') }}</label>
                            <select id="input" class="form-control" v-model="cron.month">
                                <option value="*" selected>{{ trans_choice('form.every number month', 1, ['number' => 1]) }} (*)</option>
                                <option value="*/2">{{ trans_choice('form.every number month', 2, ['number' => 2]) }} (*/2)</option>
                                <option value="*/4">{{ trans_choice('form.every number month', 4, ['number' => 4]) }} (*/4)</option>
                                <option value="1,7">{{ trans_choice('form.every number month', 7, ['number' => '1,7']) }} (1,7)</option>
                                <option value="" disabled>{{ trans('form.custom setting') }}</option>
                                @for ($i=1; $i <= 12; $i++)
                                    <option value="{{$i}}">{{ trans_choice('form.number month', $i, ['number' => $i]) }} ({{$i}})</option>
                                @endfor
                            </select>
                        </div>

                        <hr>


                        <div class="form-group">
                            <label for="">{{ trans('form.weekday') }}</label>
                            <input type="text" class="form-control" name="weekday" v-model="cron.weekday">
                        </div>

                        <div class="form-group">
                            <label for="">{{ trans('form.predefined settings') }}</label>
                            <select id="input" class="form-control" v-model="cron.weekday">
                                <option value="*">{{ trans('form.every day') }} (*)</option>
                                <option value="1-5">{{ trans('form.every from to to', ['from' => trans_choice('form.number weekday', 1), 'to' => trans_choice('form.number weekday', 5)]) }} (1-5)</option>
                                <option value="6,0">{{ trans('form.every from and to', ['from' => trans_choice('form.number weekday', 6), 'to' => trans_choice('form.number weekday', 0)]) }} (6,0)</option>
                                <option value="" disabled>{{ trans('form.custom setting') }}</option>
                                @for ($i=0; $i <= 6; $i++)
                                    <option value="{{$i}}">{{ trans_choice('form.number weekday', $i) }} ({{$i}})</option>
                                @endfor
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.add') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
