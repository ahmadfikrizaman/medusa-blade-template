@extends(WH::theme('layouts.master'), ['title' => trans('applications.ssl/tls manager'), 'sidemenu' => 'website', 'applicationTitle' => trans('applications.website'), 'breadcrumbArray' => 'app:certificate', 'breadcrumbParam' => [$user]])

@section('main-content')

    <div class="row" id="certificateVM" v-clock>

        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

        <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ajax-error></ajax-error>
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('core.my_certificate') }}</h4>
                    <p></p>
                    <table class="table table-hover" id="listCertificate"
                        data-list="{{ route('app:certificate:list', $user->hashid) }}"
                        data-view="{{ route('app:certificate:show', $user->hashid) }}"
                        data-delete="{{ route('app:certificate:destroy', $user->hashid) }}">
                        <thead>
                            <tr>
                                <th>{{ trans('form.name') }}</th>
                                <th class="text-center">{{ trans('form.view') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="x in certificates">
                                <td>@{{ x.cert }}</td>
                                <td class="text-center"><a v-on:click="view(x.cert)" v-bind:disabled="isBusy"><span class="fa fa-eye"></span></a></td>
                                <td class="text-center"><a class="text-danger" v-on:click="deleteConfirmation(x.cert)"><span class="fa fa-trash"></span></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'viewcertificate'])
        @include(WH::theme('partials.modal'), ['modal' => 'deleteclientcertificate'])
        @include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
    </div>

@stop
