@extends(WH::theme('layouts.master'), ['title' => trans('applications.new certificate'), 'sidemenu' => 'website', 'applicationTitle' => trans('applications.website'),  'breadcrumbArray' => 'app:certificate:create', 'breadcrumbParam' => [$user]])

@section('main-content')
<div class="row" id="addCertificateVM" v-clock>

    @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

    <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <ajax-error></ajax-error>
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('applications.add certificate') }}</h4>
                <p></p>
                <form id="formAddCertificate" role="form" action="{{ route('app:certificate:store', $user->hashid) }}" data-redirect="{{ route('app:certificate:index', $user->hashid) }}" method="POST" @submit.prevent="addCertificate">

                    <div class="form-group">
                        <label for="">{{ trans('form.certificate_name') }}</label>
                        <input type="text" name="certificateName" v-model="certificateName" class="form-control" placeholder="{{ trans('form.alpha_num') }}" required="required">
                    </div>

                    <div class="form-group">
                        <label for="">{{ trans('core.public_certificate') }}</label>
                        <textarea v-model="publicCertificate" name="publicCertificate" class="form-control" rows="6" required="required" placeholder="{{ trans('core.public_certificate_placeholder') }}"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">{{ trans('core.private_key') }}</label>
                        <textarea v-model="privateKey" name="privateKey" class="form-control" rows="6" required="required" placeholder="{{ trans('core.private_key_placeholder') }}"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
