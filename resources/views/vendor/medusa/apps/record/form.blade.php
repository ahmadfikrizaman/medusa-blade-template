{{ csrf_field() }}

<div class="form-group">
    <label for="">{{ trans('form.type') }}</label>
    {!! Form::select('type', $types, isset($record) ? $record->type : old('type'), ['class' => 'form-control', 'v-model' => 'type', 'v-on:change' => 'updatePlaceholder()', 'v-bind:disabled' => $edit]) !!}
</div>


<div class="form-group" v-if="type != 'SPF' && type != 'TXT'">
    <label for="">{{ trans('form.name') }}</label>
    <div class="input-group">
        <input type="text" name="name" class="form-control" v-bind:disabled="{{ $edit }}" value="{{ $record->name or old('name') }}" placeholder="{{ trans('form.name') }}. ({{ trans('applications.record name helper') }})" aria-describedby="basic-addon2">
        <span class="input-group-addon btn-primary">.{{ $domain->name }}</span>
    </div>
</div>

<div class="form-group" v-if="type == 'SPF' || type == 'TXT'">
    <label for="">{{ trans('form.name') }}</label>
    <input type="text" name="name" class="form-control" v-bind:disabled="{{ $edit }}" value="{{ $record->name or old('name') }}">
</div>


{{-- Content selain txt --}}
<div class="form-group" v-if="type != 'TXT' && type != null && type != 'SPF'">
    <label for="">{{ trans('form.content') }}</label>
    <input type="text" name="content" value="{{ $record->content or old('content') }}" class="form-control" placeholder="@{{ contentPlaceholder }}">
</div>

{{-- Content kalau txt --}}
<div class="form-group" v-if="type == 'TXT' || type == 'SPF'">
    <label for="">{{ trans('form.content') }}</label>
    <textarea name="content" class="form-control" rows="3" placeholder="@{{ contentPlaceholder }}">{{ $record->content or old('content') }}</textarea>
</div>


{{-- Content kalau MX --}}
<div class="form-group" v-if="type == 'MX'">
    <label for="">{{ trans('form.prio') }}</label>
    <input type="number" name="prio" class="form-control" value="{{ $record->prio or old('prio') }}" placeholder="{{ trans('applications.prio helper') }}">
</div>


<div class="form-group">
    <label for="">{{ trans('form.ttl') }}</label>
    <input type="number" name="ttl" value="{{ $record->ttl or old('ttl') }}" class="form-control" placeholder="{{ trans('form.ttl') }}">
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary btn-submit pull-right">{{ $saveText }}</button>
</div>
