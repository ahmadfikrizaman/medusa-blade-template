@extends(WH::theme('layouts.master'), ['title' => trans('applications.dns records'), 'sidemenu' => 'record', 'applicationTitle' => trans('applications.dns records'), 'breadcrumbArray' => 'app:record', 'breadcrumbParam' => [$user, $domain]])

@section('main-content')

    <div class="row" id="recordVM" data-dnssec="{{ route('app:record:getdsrecord', [$user->hashid, $domain->hashid]) }}" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('applications.dns records') }}</h4>
                    <div class="clearfix"></div>
                    <p></p>
                    <table class="table dataTable">
                        <thead>
                            <tr>
                                <th>{{ trans('form.name') }}</th>
                                <th>{{ trans('form.type') }}</th>
                                <th>{{ trans('form.content') }}</th>
                                <th class="text-center">{{ trans('form.ttl') }}</th>
                                <th class="text-center">{{ trans('form.prio') }}</th>
                                <th class="text-center">{{ trans('form.edit') }}</th>
                                <th class="text-center">{{ trans('form.delete') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $record)
                                <tr>
                                    <td>{{ $record->name }}</td>
                                    <td>{{ $record->type }}</td>
                                    <td>{{ $record->content }}</td>
                                    <td class="text-center">{{ $record->ttl }}</td>
                                    <td class="text-center">
                                        @if ($record->prio === null)
                                            -
                                        @else
                                            {{ $record->prio }}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($record->deleteEdit == 1)
                                            <a href="{{ route('app:record:edit', [$user->hashid, $domain->hashid, $record->hashid]) }}" class="text-warning"><span class="fa fa-edit"></span></a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if ($record->deleteEdit == 1)
                                            <a class="text-danger" data-toggle="modal" data-backdrop="static" href="#universalPostModal" @click="delete('{{ route('app:record:destroy', [$user->hashid, $domain->hashid, $record->hashid]) }}', '{{ trans('messages.confirm delete record') }}')"><span class="fa fa-trash"></span></a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>{{ trans('applications.dnssec') }}</h3>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="alert alert-info">
                {{ trans('applications.dnssec info') }}
            </div>

            <p>{{ trans('applications.dnssec structure') }}</p>
            <pre>DS = {{ $domain->name }} IN DS &lt;Key Tag&gt; &lt;Algorithm&gt; &lt;Digest type&gt; &lt;Digest&gt; ; (&lt;Digest type name&gt;)</pre>

            <p>{{ trans('applications.dnssec ds data') }}</p>
            <pre><span v-for="sec in dnssecData">@{{ sec }}</br></span></pre>

        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'universalPostModal'])
    </div>

@stop
