@extends(WH::theme('layouts.master'), ['title' => trans('applications.add dns record'), 'sidemenu' => 'record', 'applicationTitle' => trans('applications.dns records'), 'breadcrumbArray' => 'app:record:create', 'breadcrumbParam' => [$user, $domain]])

@section('main-content')

    <div class="row" id="recordVM" data-getip="{{ route('app:record:getip', [$user->hashid, $domain->hashid]) }}" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.server ip address') }}</h4>
                    <p></p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ trans('core.type') }}</th>
                                <th>{{ trans('core.address') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="ip in IPv4">
                                <td>{{ trans('core.ipv4') }}</td>
                                <td>@{{ ip }}</td>
                            </tr>
                            <tr v-for="ip in IPv6">
                                <td>{{ trans('core.ipv6') }}</td>
                                <td>@{{ ip }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('applications.add dns record') }}</h4>
                    <p></p>
                    <form action="{{ route('app:record:store', [$user->hashid, $domain->hashid]) }}" method="POST" role="form">

                        @include(WH::theme('apps.record.form'), ['saveText' => trans('form.add'), 'edit' => false])

                    </form>
                </div>
            </div>
        </div>
    </div>

@stop
