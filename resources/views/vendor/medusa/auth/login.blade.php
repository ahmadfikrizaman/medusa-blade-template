@extends(WH::theme('layouts.plain'), ['title' => trans('navigation.login')])

@section('contentmaster')

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

                <div class="loginPanel">

                    <img src="/build/themes/medusa/images/logo.png" class="img-responsive center-block animated bounceInDown"/>

                    @include(WH::theme('partials.messages'))

                    <div class="panel panel-default">
                          <div class="panel-body">
                            <h4>{{ trans('core.login') }}</h4>
                            <form action="{{ route('guest:login:attempt') }}" method="POST" role="form">

                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" name="username" value="{{ old('username') }}" placeholder="{{ trans('form.username_placeholder') }}" required>
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control input-lg" name="password" value="{{ old('password') }}" placeholder="{{ trans('form.password_placeholder') }}" required>
                                </div>

                                {{ csrf_field() }}

                                <button type="submit" class="btn btn-success btn-lg btn-block btn-submit">{{ trans('form.login') }}</button>
                            </form>
                          </div>
                    </div>

                    <p class="text-center">WHoP by Cool Code Sdn. Bhd.</p>
                </div>

            </div>
        </div>
    </div>

@stop
