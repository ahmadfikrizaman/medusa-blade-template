@extends(WH::theme('layouts.master'), ['title' => trans('navigation.panel log'), 'sidemenu' => false, 'applicationTitle' => trans('navigation.panel log'), 'breadcrumb' => 'log:panellog'])

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>{{ trans('navigation.panel log') }}</h1>
                <div class="container-fluid">
                    <div class="row">
                        {!! Breadcrumbs::render('log:panellog') !!}
                    </div>
                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <form action="{{ route('log:panellog:index') }}" method="GET" role="form">

                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="searchText" class="form-control input-lg" value="{{ $searchText }}" placeholder="{{ trans('form.log to search') }}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-lg" type="submit">{{ trans('form.search') }}</button>
                            </span>
                        </div>
                    </div>
                </form>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.log') }}</h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('core.category') }}</th>
                                    <th>{{ trans('core.log') }}</th>
                                    <th>{{ trans('core.date') }}</th>
                                    <th>{{ trans('core.time') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($logs as $log)
                                    <tr>
                                        <td>{{ $log->category }}</td>
                                        <td>{{ $log->log }}</td>
                                        <td>{{ WH::date($log->created_at, false) }}</td>
                                        <td>{{ date('H:i:s', strtotime($log->created_at)) }}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="text-center">{!! $logs->appends(['searchText' => $searchText])->links() !!}</div>
            </div>
        </div>
    </div>

@stop
