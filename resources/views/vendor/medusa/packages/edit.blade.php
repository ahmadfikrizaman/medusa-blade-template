@extends(WH::theme('layouts.master'), ['title' => trans('navigation.edit_package'), 'sidemenu' => 'package', 'applicationTitle' => trans('navigation.packages')])

@section('main-content')

    <div id="editPackage">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include(WH::theme('partials.messages'))
                <div class="alert alert-warning">
                    {{ trans('messages.update_package_warning') }}
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('navigation.edit_package') }}</h4>
                        <p></p>
                        <form action="{{ route('packages:update', $package) }}" method="POST" role="form">

                            {{ csrf_field() }}

                            @include(WH::theme('packages.forms.essential'), ['buttonName' => trans('form.update')])

                         </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop
