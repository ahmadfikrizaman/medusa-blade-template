<div class="form-group">
    <label for="">{{ trans('form.package name') }}</label>
    <input type="text" class="form-control" placeholder="{{ trans('form.package name') }}" name="name" value="{{ $package->name or old('name') }}">
</div>


<div class="form-group">
    <label for="">{{ trans('form.validity number') }}</label>
    {!! Form::select('validityNumber', $day, isset($valid[0]) ? $valid[0] : old('validityNumber'), ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    <label for="">{{ trans('form.validity time') }}</label>
    {!! Form::select('validityTime', array('DAY' => 'Day/s', 'MONTH' => 'Month/s', 'YEAR' => 'Year/s'), isset($valid[1]) ? $valid[1] : old('validityTime'), ['class' => 'form-control']) !!}
</div>


@include(WH::theme('packages.forms.main'))

<div class="form-group">
    <button type="submit" class="btn btn-primary btn-submit pull-right">{{ $buttonName }}</button>
</div>
