<div class="form-group">

    <label>{{ trans('form.ssh') }}</label>

    {!! Form::select('ssh', array(0 => 'Disable', 1 => 'Enable'), isset($package->ssh) ? $package->ssh : old('ssh'), ['class' => 'form-control']) !!}

    @if ( isset($packageOriginal) )

        @if ($packageOriginal->ssh == 0)

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.disable')]) }}</p><br><br>

        @else

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.enable')]) }}</p><br><br>

        @endif

    @endif

</div>


<div class="form-group">

    <label>{{ trans('form.ssl/tls') }} ({{ trans('messages.ssl/tls') }})</label>

    {!! Form::select('enableSsl', array(0 => 'Disable', 1 => 'Enable'), isset($package->enableSsl) ? $package->enableSsl : old('enableSsl'), ['class' => 'form-control']) !!}

    @if ( isset($packageOriginal) )

        @if ($packageOriginal->enableSsl == 0)

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.disable')]) }}</p><br><br>

        @else

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.enable')]) }}</p><br><br>

        @endif

    @endif

</div>



<div class="form-group">

    <label>{{ trans('form.dedicated redis') }} ({{ trans('messages.dedicated redis') }})</label>

    {!! Form::select('enableRedis', array(0 => 'Disable', 1 => 'Enable'), isset($package->enableRedis) ? $package->enableRedis : old('enableRedis'), ['class' => 'form-control']) !!}


    @if ( isset($packageOriginal) )

        @if ($packageOriginal->enableRedis == 0)

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.disable')]) }}</p><br><br>

        @else

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.enable')]) }}</p><br><br>

        @endif

    @endif

</div>




<div class="form-group">

    <label>{{ trans('form.git') }} ({{ trans('messages.git') }})</label>

    {!! Form::select('enableGit', array(0 => 'Disable', 1 => 'Enable'), isset($package->enableGit) ? $package->enableGit : old('enableGit'), ['class' => 'form-control']) !!}


    @if ( isset($packageOriginal) )

        @if ($packageOriginal->enableGit == 0)

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.disable')]) }}</p><br><br>

        @else

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.enable')]) }}</p><br><br>

        @endif

    @endif

</div>



<div class="form-group">

    <label>{{ trans('form.composer') }}</label>

    {!! Form::select('enableComposer', array(0 => 'Disable', 1 => 'Enable'), isset($package->enableComposer) ? $package->enableComposer : old('enableComposer'), ['class' => 'form-control']) !!}


    @if ( isset($packageOriginal) )

        @if ($packageOriginal->enableComposer == 0)

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.disable')]) }}</p><br><br>

        @else

            <p class="help-block">{{ trans('messages.original value is', ['value' => trans('form.enable')]) }}</p><br><br>

        @endif

    @endif

</div>




<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.disk quota') }}</label>

    <input placeholder="" name="diskQuota" value="{{ $package->diskQuota or old('diskQuota') }}" type="number" class="form-control">


    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->diskQuota]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.email quota') }}</label>

    <input placeholder="" name="emailQuota" value="{{ $package->emailQuota or old('emailQuota') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->emailQuota]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without. FTP quota usually unlimited">

    <label>{{ trans('form.ftp quota') }}</label>

    <input placeholder="" name="ftpQuota" value="{{ $package->ftpQuota or old('ftpQuota') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->ftpQuota]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.website quota') }}</label>

    <input placeholder="" name="websiteQuota" value="{{ $package->websiteQuota or old('websiteQuota') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->websiteQuota]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.number of database') }}</label>

    <input placeholder="" name="numberOfDatabase" value="{{ $package->numberOfDatabase or  old('numberOfDatabase') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->numberOfDatabase]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.number of database user') }}</label>

    <input placeholder="" name="numberOfDatabaseUser" value="{{ $package->numberOfDatabaseUser or old('numberOfDatabaseUser') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->numberOfDatabaseUser]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.maximum database queries per hour') }}</label>

    <input placeholder="" name="maximumQueriesPerHour" value="{{ $package->maximumQueriesPerHour or old('maximumQueriesPerHour') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->maximumQueriesPerHour]) }} {{ trans('messages.database user no change') }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.maximum database connection per hour') }}</label>

    <input placeholder="" name="maximumConnectionsPerHour" value="{{ $package->maximumConnectionsPerHour or old('maximumConnectionsPerHour') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->maximumConnectionsPerHour]) }} {{ trans('messages.database user no change') }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.maximum database updates per hour') }}</label>

    <input placeholder="" name="maximumUpdatesPerHour" value="{{ $package->maximumUpdatesPerHour or old('maximumUpdatesPerHour') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->maximumUpdatesPerHour]) }} {{ trans('messages.database user no change') }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.maximum database user connections') }}</label>

    <input placeholder="" name="maximumUserConnections" value="{{ $package->maximumUserConnections or old('maximumUserConnections') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->maximumUserConnections]) }} {{ trans('messages.database user no change') }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.number of email account') }}</label>

    <input placeholder="" name="numberOfEmailAccount" value="{{ $package->numberOfEmailAccount or old('numberOfEmailAccount') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->numberOfEmailAccount]) }}</p><br><br>

    @endif

</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.number of ftp account') }}</label>

    <input placeholder="" name="numberOfFtpAccount" value="{{ $package->numberOfFtpAccount or old('numberOfFtpAccount') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->numberOfFtpAccount]) }}</p><br><br>

    @endif

</div>


<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.number of domain') }}</label>

    <input placeholder="" name="numberOfDomain" value="{{ $package->numberOfDomain or old('numberOfDomain') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->numberOfDomain]) }}</p><br><br>

    @endif
</div>



<div class="form-group" data-toggle="tooltip" data-placement="top" title="Insert '0' for unlimited without ' sign">

    <label>{{ trans('form.number of whoplet') }}</label>

    <input placeholder="" name="numberOfWhoplet" value="{{ $package->numberOfWhoplet or old('numberOfWhoplet') }}" type="number" class="form-control">

    @if ( isset($packageOriginal) )

        <p class="help-block">{{ trans('messages.original value is', ['value' => $packageOriginal->numberOfWhoplet]) }}</p><br><br>

    @endif
</div>
