@extends(WH::theme('layouts.master'), ['title' => trans('navigation.packages'), 'sidemenu' => 'package', 'applicationTitle' => trans('navigation.packages'), 'breadcrumb' => 'packages'])

@section('main-content')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @include(WH::theme('partials.messages'))
        </div>
    </div>

    <div class="row" id="listPackages">
        <div class="package">
            @foreach($packages as $package)
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="package-table">
                        <div class="package-header">
                            <p class="package-title">{{ $package->name }} Plan</p>
                            {{-- <p class="package-rate"><sup>$</sup> 0 <span>/Mo.</span></p>
                            <a href="#" class="btn btn-custom">And Get Free Month</a> --}}
                        </div>

                        <div class="package-list">
                            <ul>
                                <li><i class="fa fa-circle-o"></i>{{ trans_choice('core.client using this package', count($package->clientPackage)) }} <span class="pull-right">{{ trans_choice('core.client', count($package->clientpackage), ['number' => count($package->clientpackage)]) }}</li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.validity') }} <span class="text-lowercase pull-right">{{ $package->validFor }}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.ssh') }} <span class="pull-right">{{ ($package->ssh == 0 ? 'Disable' : 'Enable') }}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.ssl/tls') }} <span class="pull-right">{{ ($package->enableSsl == 0 ? 'Disable' : 'Enable') }}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.dedicated redis') }} <span class="pull-right">{{ ($package->enableRedis == 0 ? 'Disable' : 'Enable') }}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.git') }} <span class="pull-right">{{ ($package->enableGit == 0 ? 'Disable' : 'Enable') }}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.composer') }} <span class="pull-right">{{ ($package->enableComposer == 0 ? 'Disable' : 'Enable') }}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.disk quota') }}  <span class="pull-right">{!! ($package->diskQuota == 0 ? trans('core.unlimited') : $package->diskQuota . ' MB') !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.email quota') }} <span class="pull-right">{!! ($package->emailQuota == 0 ? trans('core.unlimited') : $package->emailQuota . ' MB') !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.ftp quota') }} <span class="pull-right">{!! ($package->ftpQuota == 0 ? trans('core.unlimited') : $package->ftpQuota . ' MB') !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.website quota') }} <span class="pull-right">{!! ($package->websiteQuota == 0 ? trans('core.unlimited') : $package->websiteQuota . ' GB') !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.number of database') }} <span class="pull-right">{!! ($package->numberOfDatabase == 0 ? trans('core.unlimited') : $package->numberOfDatabase) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.number of database user') }} <span class="pull-right">{!! ($package->numberOfDatabaseUser == 0 ? trans('core.unlimited') : $package->numberOfDatabaseUser) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.maximum database queries per hour') }} <span class="pull-right">{!! ($package->maximumQueriesPerHour == 0 ? trans('core.unlimited') : $package->maximumQueriesPerHour) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.maximum database connection per hour') }} <span class="pull-right">{!! ($package->maximumConnectionsPerHour == 0 ? trans('core.unlimited') : $package->maximumConnectionsPerHour) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.maximum database updates per hour') }} <span class="pull-right">{!! ($package->maximumUpdatesPerHour == 0 ? trans('core.unlimited') : $package->maximumUpdatesPerHour) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.maximum database user connections') }} <span class="pull-right">{!! ($package->maximumUserConnections == 0 ? trans('core.unlimited') : $package->maximumUserConnections) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.number of email account') }} <span class="pull-right">{!! ($package->numberOfEmailAccount == 0 ? trans('core.unlimited') : $package->numberOfEmailAccount) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.number of ftp account') }} <span class="pull-right">{!! ($package->numberOfFtpAccount == 0 ? trans('core.unlimited') : $package->numberOfFtpAccount) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.number of domain') }} <span class="pull-right">{!! ($package->numberOfDomain == 0 ? trans('core.unlimited') : $package->numberOfDomain) !!}</span></li>
                                <li><i class="fa fa-circle-o"></i>{{ trans('form.number of whoplet') }} <span class="pull-right">{!! ($package->numberOfWhoplet == 0 ? trans('core.unlimited') : $package->numberOfWhoplet) !!}</span></li>
                                <div class="row">
                                    <br>
                                    <div class="col-md-12 text-right">
                                        @if ($package->id === 1 && Bouncer::allows('edit-package'))
                                            <button class="btn btn-primary" disabled="disabled">{{ trans('form.edit') }}</button>
                                        @elseif (Bouncer::allows('edit-package'))
                                            <a href="{{ route('packages:edit', $package) }}" class="btn btn-primary">{{ trans('form.edit') }}</a>
                                        @endif

                                        @if ( ($package->id ===1 || count($package->clientpackage) !== 0) && Auth::user()->can('destroy-package') )
                                            <button class="btn btn-danger" disabled="disabled">{{ trans('form.delete') }}</button>
                                        @elseif (Auth::user()->can('destroy-package'))
                                            <a class="btn btn-danger" data-toggle="modal" data-backdrop="static" v-on:click="initDelete('{{ route('packages:destroy', $package) }}', {{ $package }})" href='#deletePackage'>{{ trans('form.delete') }}</a>
                                        @endif
                                    </div>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach

            @include(WH::theme('partials.modal'), ['modal' => 'deletepackage'])
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! $packages->render() !!}
        </div>
    </div>

@stop
