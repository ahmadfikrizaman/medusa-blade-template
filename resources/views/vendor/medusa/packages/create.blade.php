@extends(WH::theme('layouts.master'), ['title' => trans('navigation.add_package'), 'sidemenu' => 'package', 'applicationTitle' => trans('navigation.packages'), 'breadcrumb' => 'packages-add'])

@section('main-content')

    <div id="createPackage">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include(WH::theme('partials.messages'))
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('navigation.add_package') }}</h4>
                        <p></p>
                        <form action="{{ route('packages:store') }}" method="POST" role="form">

                            {{ csrf_field() }}

                            @include(WH::theme('packages.forms.essential'), ['buttonName' => trans('form.create')])

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop
