@extends(WH::theme('layouts.master'), ['title' => trans('navigation.dashboard'), 'applicationTitle' => trans('navigation.dashboard'), 'sidemenu' => false, 'breadcrumb' => 'admindashboard'])

@section('content')
    <div id="admindashboard" data-url="{{ route('dashboard:admindashboard:data') }}" data-log="{{ route('dashboard:admindashboard:getlatestlog') }}" v-cloak>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-if="licenseAlert != null">
                    <div class="alert alert-danger">
                        @{{ licenseAlert }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-if="expired">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">{{ trans('core.error') }}</h3>
                        </div>
                        <div class="panel-body">
                            {!! trans('core.license expired or error') !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="whop-information" v-if="licenseAlert == null && !expired">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <h1>{{ $data['totalClient'] }}</h1>
                        <h5>{{ trans('core.total_client') }}</h5>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <h1>{{ $data['totalDomain'] }}</h1>
                        <h5>{{ trans('core.total_domain_name') }}</h5>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <h1>{{ $data['totalDatabase'] }}</h1>
                        <h5>{{ trans('core.total_database') }}</h5>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <h1>@{{ nginxCount }}</h1>
                        <h5>{{ trans('core.total_whoplet') }}</h5>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <h1>{{ $data['totalFTPAccounts'] }}</h1>
                        <h5>{{ trans('core.total_ftp_account') }} </h5>
                    </div>
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <h1>{{ $data['totalEmailAccounts'] }}</h1>
                        <h5>{{ trans('core.total_email_account') }}</h5>
                    </div>
                </div>
            </div>

            <div class="distribution-info">
                <div class="row">
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 chart">
                        <canvas id="packageChart" width="100%" height="200px"></canvas>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 chart">
                        <canvas id="phpChart" width="100%" height="200px"></canvas>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 special-info">
                        <div class="row with-border-bottom">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ licenseOwner }}</h1>
                                <h5>{{ trans('core.license_owner') }}</h5>
                            </div>
                        </div>
                        <div class="row with-border-bottom">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ licenseType }}</h1>
                                <h5>{{ trans('core.license_type') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ licenseExpiry }}</h1>
                                <h5>{{ trans('core.license_expiry') }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="server-info no-border">
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 server">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ kernelVersion }}</h1>
                                <h5>{{ trans('core.kernel_version') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ processorName }}</h1>
                                <h5>{{ trans('core.processor_name') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ freeMemory }}/@{{ totalMemory }}</h1>
                                <h5>{{ trans('core.free_memory') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ freeDiskSpace }}/@{{ totalDiskSpace }}</h1>
                                <h5>{{ trans('core.free_diskspace') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ uptime }}</h1>
                                <h5>{{ trans('core.server_uptime') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>{{ env('WHOP_PANEL_VERSION') }}</h1>
                                <h5>{{ trans('core.whop_version') }}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h1>@{{ nodeVersion }}</h1>
                                <h5>{{ trans('core.whop_node_version') }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 no-border" v-if="licenseAlert == null && !expired">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 live-log">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('core.category') }}</th>
                                            <th>{{ trans('core.log') }}</th>
                                            <th>{{ trans('core.time') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="log in logs" transition="staggered" stagger="100">
                                            <td>@{{ log.category }}</td>
                                            <td>@{{ log.log }}</td>
                                            <td>@{{ log.time }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
