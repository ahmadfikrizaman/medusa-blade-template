@extends(WH::theme('layouts.blueprint'))

@section('contentmaster')

    <div class="container-fluid">
        @yield('content')
    </div>

@stop

@section('socket')
    <!-- Plase change this to CDN later -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.6/socket.io.min.js"></script>
@stop
