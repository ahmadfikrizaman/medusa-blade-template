@extends(WH::theme('layouts.blueprint'))

@section('navigation')
    @include(WH::theme('layouts.partials.webmailnavbar'))
@stop

@section('breadcrumb')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            @if (isset($breadcrumb))
                {!! Breadcrumbs::render($breadcrumb) !!}
            @elseif (isset($breadcrumbArray))
                {!! Breadcrumbs::renderArray($breadcrumbArray, $breadcrumbParam) !!}
            @endif
            </div>
        </div>
    </div>
@stop

@section('contentmaster')

    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>
                        @yield('icon')
                        {{ trans($applicationTitle) }}
                    </h1>
                    <div class="container-fluid">
                        <div class="row">
                            @if (isset($breadcrumb))
                                {!! Breadcrumbs::render($breadcrumb) !!}
                            @elseif (isset($breadcrumbArray))
                                {!! Breadcrumbs::renderArray($breadcrumbArray, $breadcrumbParam) !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 medusa-sidemenu">
                <ul class="nav">

                    @include(WH::theme('partials.sidemenu'))

                </ul>
            </div>
            <div class="col-lg-10">

                @include(WH::theme('partials.messages'))

                @yield('main-content')

            </div>
        </div>
    </div>

@stop

@section('socket')
    <!-- Plase change this to CDN later -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.6/socket.io.min.js"></script>
@stop
