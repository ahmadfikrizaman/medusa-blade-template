@extends(WH::theme('layouts.blueprint'))

@section('navigation')
    @if (Auth::check())
        @if (Auth::user()->is('admin'))
            @include(WH::theme('layouts.partials.adminnavbar'))
        @elseif (Auth::user()->is('client'))
            @include(WH::theme('layouts.partials.clientnavbar'))
        @endif
    @endif
@stop

@section('contentmaster')

    <div class="container-fluid">

        @if(!$sidemenu)
            @yield('content')
        @else
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>
                            @yield('icon')
                            {{ trans($applicationTitle) }}
                        </h1>
                        <div class="container-fluid">
                            <div class="row">
                                @if (isset($breadcrumb))
                                    {!! Breadcrumbs::render($breadcrumb) !!}
                                @elseif (isset($breadcrumbArray))
                                    {!! Breadcrumbs::renderArray($breadcrumbArray, $breadcrumbParam) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2  medusa-sidemenu">
                    <ul class="nav">

                        @include(WH::theme('partials.sidemenu'))

                    </ul>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">

                    @include(WH::theme('partials.messages'))

                    @yield('main-content')

                </div>
            </div>
        @endif

    </div>

@stop

@section('socket')
    <!-- Plase change this to CDN later -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.6/socket.io.min.js"></script>
@stop
