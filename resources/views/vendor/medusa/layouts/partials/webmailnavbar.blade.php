<nav class="navbar navbar-default navbar-static-top" role="navigation" id="nav">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand animated fadeIn" href="{{ route('webmail') }}">WHoP {{ trans('applications.webmail') }}</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('webmail') }}"> {{ trans('applications.webmail') }}</a></li>
                    <li><a href="{{ route('webmail:forwarding') }}"> {{ trans('applications.email forwarding') }}</a></li>
                    <li><a href="/roundcube" target="_blank"> {{ trans('applications.roundcube') }}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::guard('mail')->user()->email }} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('webmail:logout') }}"> {{ trans('navigation.logout') }}</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </div>
</nav>
