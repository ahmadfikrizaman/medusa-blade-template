<nav class="navbar navbar-default navbar-static-top" role="navigation" id="nav">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('dashboard:admindashboard') }}">
                    <img src="/whopwhite.png">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('dashboard:admindashboard') }}"> {{ trans('navigation.dashboard') }}</a></li>
                    <li><a href="{{ route('clients:index', ['filter' => 'all']) }}">{{ trans('navigation.users') }}</a></li>
                    <li><a href="{{ route('packages:index') }}">{{ trans('navigation.packages') }}</a></li>
                    <li><a href="{{ route('serversetting:dovecot') }}">{{ trans('navigation.server_settings') }}</a></li>
                    <li><a href="{{ route('whopsetting:certificate') }}">{{ trans('navigation.whop_settings') }}</a></li>
                    <li><a href="{{ route('log:panellog:index') }}">{{ trans('navigation.panel log') }}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}</a>
                        <ul class="dropdown-menu">
                            <li><a href="#"> {{ trans('navigation.profile') }}</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ route('logout') }}"> {{ trans('navigation.logout') }}</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        @if($applicationTitle == trans('navigation.dashboard'))
            <div class="medusa-dashboard">
                <div class="row">
                    <div class="col-md-12">
                        <span class="badge"> {{ trans('core.live_server_load') }}</span>
                        <canvas id="liveLoad" height="200px"></canvas>
                    </div>
                </div>
            </div>
        @endif
    </div>
</nav>
