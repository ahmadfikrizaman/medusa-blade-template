<nav class="navbar navbar-default navbar-static-top" role="navigation" id="nav">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('clients:show', $user->hashid) }}">
                    <img src="/build/themes/medusa/images/whop-white.png">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('clients:show', $user->hashid) }}">{{ trans('navigation.dashboard') }}</a></li>
                    <li><a href="{{ route('app:whoplet:index', $user->hashid) }}">{{ trans('applications.website') }}</a></li>
                    <li><a href="{{ route('app:mariadb:index', $user->hashid) }}">{{ trans('applications.database') }}</a></li>
                    <li><a href="{{ route('app:domain:index', $user->hashid) }}">{{ trans('applications.domain') }}</a></li>
                    <li><a href="{{ route('app:email:index', [$user->hashid]) }}">{{ trans('applications.email') }}</a></li>
                    <li><a href="{{ route('app:filemanager', $user->hashid) }}">{{ trans('applications.file') }}</a></li>
                    <li><a href="{{ route('app:cron:index', $user->hashid) }}">{{ trans('applications.advance settings') }}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}</a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('clients:edit', $user->hashid) }}"> {{ trans('navigation.profile') }}</a></li>
                            <li><a href="{{ route('logout') }}"> {{ trans('navigation.logout') }}</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </div>
</nav>
