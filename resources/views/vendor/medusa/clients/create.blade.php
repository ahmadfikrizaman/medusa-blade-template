@extends(WH::theme('layouts.master'), ['title' => trans('navigation.add_client'), 'sidemenu' => 'user', 'applicationTitle' => trans('navigation.clients'), 'breadcrumb' => 'clients-add'])

@section('main-content')

    <div class="row" id="addClient" v-cloak>
        <check-job
            :is-busy.sync="isBusy"
            title="{{ trans('core.creating user progress') }}"
            jobname="CreateClient"
            checkurl="{{ route('clients:checkjob') }}">
        </check-job>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="isAjaxError">
            <ajax-error></ajax-error>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-if="errorMessages.length > 0">
            <div class="alert alert-danger">
                <p v-for="error in errorMessages">
                    <i class="fa fa-circle-o"></i> @{{ error }}
                </p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="!isBusy">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('navigation.add_client') }}</h4>
                    <p></p>
                    <form id="formCreateClient" action="{{ route('clients:checklicense') }}" data-store-action="{{ route('clients:store') }}" method="POST" role="form" @submit.prevent="checkLicense">
                        <div class="form-group">
                            <label>{{ trans('form.name') }}</label>
                            <input placeholder="" id="name" v-model="name" class="form-control" type="text" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('form.username') }}</label>
                            <input placeholder="" id="username" v-model="username" class="form-control" type="text" value="{{ old('username') }}">
                        </div>
                        <password-generator
                            :password.sync="password"
                            :verifypassword.sync="verifyPassword"
                            :passwordlength="16"
                            modalid="modalGeneratePassword">
                        </password-generator>
                        <div class="form-group">
                            <label>{{ trans('form.email') }}</label>
                            <input placeholder="" id="email" v-model="email" class="form-control" type="email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('form.package') }}</label>
                            {!! Form::select('package', $packages, old('package'), ['class' => 'form-control', 'v-model' => 'package']) !!}
                        </div>
                        <div class="form-group">
                            <label>Domain Name</label>
                            <input placeholder="" id="domainName" v-model="domainName" class="form-control" type="text" value="{{ old('domainName') }}">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.submit') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@include(WH::theme('partials.templates'), ['template' => 'check-job'])
@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
@stop
