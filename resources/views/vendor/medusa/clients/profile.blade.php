@extends(WH::theme('layouts.master'), ['title' => trans('core.profile'), 'breadcrumbArray' => 'clientprofile', 'breadcrumbParam' => [$user], 'sidemenu' => 'client', 'applicationTitle' => trans('navigation.profile')])

@section('main-content')

<div class="row" id="editProfileVM" v-cloak>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="!isBusy">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('core.edit profile') }}</h4>
                <form action="{{ route('clients:update', $user->hashid) }}" method="POST" role="form">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label>{{ trans('form.name') }}</label>
                        <input class="form-control" type="text" name="name" value="{{ $user->name or old('name') }}">
                    </div>

                    <password-generator
                        :password.sync="password"
                        :verifypassword.sync="verifyPassword"
                        :passwordlength="16"
                        modalid="modalGeneratePassword">
                    </password-generator>

                    <div class="form-group">
                        <label>{{ trans('form.email') }}</label>
                        <input  class="form-control" type="email" name="email" value="{{ $user->email or old('email') }}">
                    </div>


                    <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.submit') }}</button>

                </form>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="!isBusy">
        <div class="panel panel-default">
            <div class="panel-body">
                <h4>{{ trans('core.theme') }}</h4>
                <p>Set default theme</p>
                <form action="{{ route('clients:updatetheme', $user->hashid) }}" method="POST" role="form">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="">{{ trans('core.WHoP Theme') }}</label>
                       {!! Form::select('theme', $themes, $theme, ['class' => 'form-control']) !!}
                    </div>

                    <button type="submit" class="btn btn-primary btn-submit pull-right">{{ trans('form.submit') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include(WH::theme('partials.templates'), ['template' => 'passwordGenerator'])
@stop
