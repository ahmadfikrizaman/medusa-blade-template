@extends(WH::theme('layouts.master'), ['title' => trans('navigation.clients'), 'sidemenu' => 'user', 'applicationTitle' => trans('navigation.users'), 'breadcrumb' => 'clients'])

@section('main-content')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4 class="pull-left">{{ trans('navigation.clients') }}</h4>
                    <div class="clearfix"></div>
                    <p></p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>{{ trans('form.name') }}</th>
                                <th>{{ trans('form.username') }}</th>
                                <th>{{ trans('form.email') }}</th>
                                <th>{{ trans('form.created_at') }}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td>
                                        @if ($client->disable == false)
                                            <i class="fa fa-circle text-success"></i>
                                        @else
                                            <i class="fa fa-circle text-danger"></i>
                                        @endif
                                        <a href="{{ route('clients:show', $client->hashid) }}" class="text-primary">{{ $client->name }}</a>
                                    </td>
                                    <td>{{ $client->username }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->created_at->diffForHumans() }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ trans('form.more') }} <i class="fa fa-chevron-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{ route('clients:show', $client->hashid) }}">{{ trans('form.view') }}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@stop
