@extends(WH::theme('layouts.master'), ['title' => $user->name, 'sidemenu' => 'client', 'applicationTitle' =>  $user->name, 'breadcrumbArray' => 'view-client', 'breadcrumbParam' => [$user]])

@section('meta')
    <meta name="diskSpaceUsage" content="{{ route('clients:getdiskusage', $user->hashid) }}">
    <meta name="whopletUsage" content="{{ route('clients:getwhopletusage', $user->hashid) }}">
    <meta name="websiteQuotaUsage" content="{{ route('clients:getwebsitequotausage', $user->hashid) }}">
@stop

@section('icon')
    <span class="medusa-circle">{{ substr($user->name, 0, 1) }}</span>
@endsection

@section('main-content')

    <div id="clientShowPage" v-cloak>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="pull-left">{{ $user->name }} ({{ $user->username }})</h4>
                        <div class="btn-group pull-right">
                            <a href="" class="btn btn-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-cog"></i> </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('clients:edit', $user->hashid) }}">{{ trans('form.edit profile') }}</a></li>
                                <li><a href="#" @click="resetQuota('{{ route('clients:resetquota', $user->hashid) }}')">{{ trans('core.reset website quota') }}</a></li>
                                <li><a href="#" @click="resetAccount('{{ route('clients:reset', $user->hashid) }}')">{{ trans('core.reset account expiry') }}</a></li>
                                @if ($user->id !== 2)
                                    @if ($user->disable == false)
                                        <li><a href="#" @click="enableDisable('{{ route('clients:enabledisable', $user->hashid) }}')">{{ trans('core.disable account') }}</a></li>
                                    @else
                                        <li><a href="#" @click="enableDisable('{{ route('clients:enabledisable', $user->hashid) }}')">{{ trans('core.enable account') }}</a></li>
                                    @endif
                                    <li><a href="#" @click="delete('{{ route('clients:destroy', $user->hashid) }}', '{{ route('clients:index', ['filter' => 'all']) }}')">{{ trans('core.delete client') }}</a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="clearfix"></div>

                        <h5 class="list-group-item-heading">{{ trans('form.email') }}</h5>
                        <p>{{ $user->email }}</p>

                        <h5 class="list-group-item-heading">{{ trans('form.package') }}</h5>
                        <p>{{ $user->clientPackage->package->name }}</p>

                        <h5 class="list-group-item-heading">{{ trans('core.member since') }}</h5>
                        <p>{{ $user->created_at->toFormattedDateString() }}</p>

                        <h5 class="list-group-item-heading">{{ trans('clients.activation date') }}</h5>
                        <p>{{ WH::date($user->clientPackage->activeDate, false) }}</p>

                        <h5 class="list-group-item-heading">{{ trans('clients.expiration date') }}</h5>
                        <p>{{ WH::date($user->clientPackage->endDate, false) }}</p>

                        <h5 class="list-group-item-heading">{{ trans('clients.register time') }}</h5>
                        <p>{{ WH::date($user->created_at) }}</p>

                        <h5 class="list-group-item-heading">{{ trans('core.last login') }}</h5>
                        <p>{{ $lastLogin }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary panel-category">
                    <div class="panel-heading">
                        <h4 class="panel-title pull-left">{{ trans('applications.website') }}</h4>
                        <h4 class="panel-title pull-right" tabindex="0" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="{{ trans('applications.whop info') }} {{ trans('applications.ssl/tls info') }}"><i class="fa fa-info-circle">&nbsp;</i></h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <br>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <usage-progress
                                    title="{{ trans('applications.whoplet usage') }}"
                                    :color="whopletUsage.color"
                                    :value="whopletUsage.value"
                                    :max="whopletUsage.max"
                                    :percentage="whopletUsage.percentage">
                                </usage-progress>
                                <usage-progress
                                    title="{{ trans('applications.website quota usage') }}"
                                    :color="websiteQuotaUsage.color"
                                    :value="websiteQuotaUsage.value"
                                    :max="websiteQuotaUsage.max"
                                    :percentage="websiteQuotaUsage.percentage">
                                </usage-progress>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:whoplet:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/whoplet.png">
                                    <h5 class="text-center text-link">{{ trans('applications.whoplet') }}</h5>
                                </a>
                            </div>
                            @if ($user->clientPackage->enableSsl === 1)
                                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                    <a href="{{ route('app:certificate:index', $user->hashid) }}">
                                        <img class="img-thumbnail" src="/build/themes/medusa/images/certificate.png">
                                        <h5 class="text-center text-link">{{ trans('applications.ssl/tls manager') }}</h5>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary panel-category">
                    <div class="panel-heading">
                        <h4 class="panel-title pull-left">{{ trans('applications.domain') }}</h4>
                        <h4 class="panel-title pull-right" tabindex="0" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="{{ trans('applications.domain info') }}"><i class="fa fa-info-circle">&nbsp;</i></h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <br>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <usage-progress
                                    title="{{ trans('applications.domain usage') }}"
                                    color="{{ $progress['domainUsage']['color'] }}"
                                    value="{{ $progress['domainUsage']['value'] }}"
                                    max="{{ $progress['domainUsage']['max'] }}"
                                    percentage="{{ $progress['domainUsage']['percentage'] }}">
                                </usage-progress>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:whoplet:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/w3.png">
                                    <h5 class="text-center text-link">{{ trans('applications.domain name & records') }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary panel-category">
                    <div class="panel-heading">
                        <h4 class="panel-title pull-left">{{ trans('applications.database') }}</h4>
                        <h4 class="panel-title pull-right" tabindex="0" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="{{ trans('applications.database info') }}"><i class="fa fa-info-circle">&nbsp;</i></h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <usage-progress
                                    title="{{ trans('applications.database usage') }}"
                                    color="{{ $progress['databaseUsage']['color'] }}"
                                    value="{{ $progress['databaseUsage']['value'] }}"
                                    max="{{ $progress['databaseUsage']['max'] }}"
                                    percentage="{{ $progress['databaseUsage']['percentage'] }}">
                                </usage-progress>

                                <usage-progress
                                    title="{{ trans('applications.database user usage') }}"
                                    color="{{ $progress['databaseUserUsage']['color'] }}"
                                    value="{{ $progress['databaseUserUsage']['value'] }}"
                                    max="{{ $progress['databaseUserUsage']['max'] }}"
                                    percentage="{{ $progress['databaseUserUsage']['percentage'] }}">
                                </usage-progress>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:mariadb:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/database.png"/>
                                    <h5 class="text-center text-link">{{ trans('applications.mariadb database') }}</h5>
                                </a>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="/phpMyAdmin" target="_blank">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/pma.png"/>
                                    <h5 class="text-center text-link">{{ trans('applications.phpMyAdmin') }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary panel-category">
                    <div class="panel-heading">
                        <h4 class="panel-title pull-left">{{ trans('applications.email') }}</h4>
                        <h4 class="panel-title pull-right" tabindex="0" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="{{ trans('applications.email info') }}"><i class="fa fa-info-circle">&nbsp;</i></h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <usage-progress
                                    title="{{ trans('applications.email usage') }}"
                                    color="{{ $progress['emailUsage']['color'] }}"
                                    value="{{ $progress['emailUsage']['value'] }}"
                                    max="{{ $progress['emailUsage']['max'] }}"
                                    percentage="{{ $progress['emailUsage']['percentage'] }}">
                                </usage-progress>

                                <usage-progress
                                    title="{{ trans('applications.email quota usage') }}"
                                    color="{{ $progress['emailQuotaUsage']['color'] }}"
                                    value="{{ $progress['emailQuotaUsage']['value'] }}"
                                    max="{{ $progress['emailQuotaUsage']['max'] }}"
                                    percentage="{{ $progress['emailQuotaUsage']['percentage'] }}">
                                </usage-progress>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:email:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/emailaccount.png">
                                    <h5 class="text-center text-link">{{ trans('applications.email account') }}</h5>
                                </a>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:ef:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/emailforwarding.png">
                                    <h5 class="text-center text-link">{{ trans('applications.email forwarding') }}</h5>
                                </a>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('webmail:login') }}" target="_blank">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/webmaillogin.png">
                                    <h5 class="text-center text-link">{{ trans('applications.webmail login') }}</h5>
                                </a>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="/roundcube" target="_blank">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/roundcube.png">
                                    <h5 class="text-center text-link">{{ trans('applications.roundcube') }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary panel-category">
                    <div class="panel-heading">
                        <h4 class="panel-title pull-left">{{ trans('applications.file') }}</h4>
                        <h4 class="panel-title pull-right" tabindex="0" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="{{ trans('applications.file management info') }}"><i class="fa fa-info-circle">&nbsp;</i></h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <usage-progress
                                    title="{{ trans('applications.disk space usage') }}"
                                    :color="diskSpaceUsage.color"
                                    :value="diskSpaceUsage.value"
                                    :max="diskSpaceUsage.max"
                                    :percentage="diskSpaceUsage.percentage">
                                </usage-progress>

                                <usage-progress
                                    title="{{ trans('applications.ftp usage') }}"
                                    color="{{ $progress['ftpUsage']['color'] }}"
                                    value="{{ $progress['ftpUsage']['value'] }}"
                                    max="{{ $progress['ftpUsage']['max'] }}"
                                    percentage="{{ $progress['ftpUsage']['percentage'] }}">
                                </usage-progress>

                                <usage-progress
                                    title="{{ trans('applications.ftp quota usage') }}"
                                    color="{{ $progress['ftpQuotaUsage']['color'] }}"
                                    value="{{ $progress['ftpQuotaUsage']['value'] }}"
                                    max="{{ $progress['ftpQuotaUsage']['max'] }}"
                                    percentage="{{ $progress['ftpQuotaUsage']['percentage'] }}">
                                </usage-progress>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:filemanager', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/fm.png"/>
                                    <h5 class="text-center text-link">{{ trans('applications.WHoP File Manager') }}</h5>
                                </a>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:ftp:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/ftpaccount.png"/>
                                    <h5 class="text-center text-link">{{ trans('applications.ftp accounts') }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-primary panel-category">
                    <div class="panel-heading">
                        <h4 class="panel-title pull-left">{{ trans('applications.advance settings') }}</h4>
                        <h4 class="panel-title pull-right" tabindex="0" role="button" data-toggle="popover" data-placement="top" data-trigger="focus" data-content="{{ trans('applications.crontab manager info') }} {{ trans('applications.supervisor info') }}"><i class="fa fa-info-circle">&nbsp;</i></h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:cron:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/crontab.png"/>
                                    <h5 class="text-center text-link">{{ trans('applications.crontab manager') }}</h5>
                                </a>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                                <a href="{{ route('app:supervisor:index', $user->hashid) }}">
                                    <img class="img-thumbnail" src="/build/themes/medusa/images/supervisor.png"/>
                                    <h5 class="text-center text-link">{{ trans('applications.supervisor') }}</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'deleteclient'])
        @include(WH::theme('partials.modal'), ['modal' => 'resetwebsitequota'])
        @include(WH::theme('partials.modal'), ['modal' => 'enabledisableaccount'])
        @include(WH::theme('partials.modal'), ['modal' => 'resetexpiredaccount'])
    </div>

@include(WH::theme('partials.templates'), ['template' => 'usageProgress'])
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
