@if($sidemenu == 'admin')

    <li><a href="{{ route('dashboard:admindashboard') }}"> {{ trans('navigation.dashboard') }}</a></li>
    <li><a href="{{ route('clients:index', ['filter' => 'all']) }}">{{ trans('navigation.users') }}</a></li>
    <li><a href="{{ route('packages:index') }}">{{ trans('navigation.packages') }}</a></li>
    <li><a href="{{ route('serversetting:dovecot') }}">{{ trans('navigation.server_settings') }}</a></li>
    <li><a href="{{ route('whopsetting:certificate') }}">{{ trans('navigation.whop_settings') }}</a></li>
    <li><a href="{{ route('log:panellog:index') }}">{{ trans('navigation.panel log') }}</a></li>

@elseif($sidemenu == 'client')

    <li><a href="{{ route('clients:show', $user->hashid) }}">{{ trans('navigation.dashboard') }}</a></li>
    <li><a href="{{ route('app:whoplet:index', $user->hashid) }}">{{ trans('applications.website') }}</a></li>
    <li><a href="{{ route('app:mariadb:index', $user->hashid) }}">{{ trans('applications.database') }}</a></li>
    <li><a href="{{ route('app:domain:index', $user->hashid) }}">{{ trans('applications.domain') }}</a></li>
    <li><a href="{{ route('app:email:index', [$user->hashid]) }}">{{ trans('applications.email') }}</a></li>
    <li><a href="{{ route('app:filemanager', $user->hashid) }}">{{ trans('applications.file') }}</a></li>
    <li><a href="{{ route('app:cron:index', $user->hashid) }}">{{ trans('applications.advance settings') }}</a></li>

@elseif($sidemenu == 'website')

    <li><a href="{{ route('app:whoplet:index', $user->hashid) }}">{{ trans('applications.whoplet') }}</a></li>
    <li><a href="{{ route('app:whoplet:create', $user->hashid) }}">{{ trans('applications.add whoplet') }}</a></li>
    @if ($user->clientPackage->enableSsl === 1)
        <li><a href="{{ route('app:certificate:index', $user->hashid) }}">{{ trans('applications.ssl/tls manager') }}</a></li>
        <li><a href="{{ route('app:certificate:create', $user->hashid) }}">{{ trans('applications.add certificate') }}</a></li>
    @endif

@elseif($sidemenu == 'whoplet')

    <li><a href="{{ route('app:whoplet:show', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure]) }}">{{ trans('navigation.dashboard') }}</a></li>
    <li><a href="{{ route('app:whoplet:changephp', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure]) }}">{{ trans('applications.change php version') }}</a></li>
    <li><a href="{{ route('app:whoplet:adddomain', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure]) }}">{{ trans('applications.add additional domain') }}</a></li>
    @if ( filter_var(isset($whoplet) ? $secure : $request->secure, FILTER_VALIDATE_BOOLEAN) === true)
        <li><a href="{{ route('app:whoplet:changecertificate', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure]) }}">{{ trans('applications.change whoplet certificate') }}</a></li>
    @endif
    <li><a href="{{ route('app:whoplet:showstatistic', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure]) }}">{{ trans('applications.statistics') }}</a></li>
    <li><a href="{{ route('app:whoplet:viewlog', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure, 'logtype' => 'access']) }}">{{ trans('applications.access log') }}</a></li>
    <li><a href="{{ route('app:whoplet:viewlog', [$user->hashid, 'whoplet' => (isset($whoplet)) ? $whoplet : $request->whoplet, 'secure' => (isset($whoplet)) ? $secure : $request->secure, 'logtype' => 'error']) }}">{{ trans('applications.error log') }}</a></li>

@elseif($sidemenu == 'database')

    <li><a href="{{ route('app:mariadb:index', $user->hashid) }}">{{ trans('applications.mariadb database') }}</a></li>
    <li><a href="{{ route('app:mariadb:create:database', [$user->hashid]) }}">{{ trans('applications.add database') }}</a></li>
    <li><a href="{{ route('app:mariadb:create:user', [$user->hashid]) }}">{{ trans('applications.add database user') }}</a></li>
    <li><a href="{{ route('app:mariadb:create:assign', [$user->hashid]) }}">{{ trans('applications.assign user to database') }}</a></li>
    <li><a href="/phpMyAdmin" target="_blank">{{ trans('applications.phpMyAdmin') }}</a></li>

@elseif($sidemenu == 'domain')

    <li><a href="{{ route('app:domain:index', $user->hashid) }}">{{ trans('applications.domain name & records') }}</a></li>
    <li><a href="{{ route('app:domain:create', [$user->hashid]) }}">{{ trans('applications.add domain') }}</a></li>

@elseif($sidemenu == 'record')

    <li><a href="{{ route('app:record:index', [$user->hashid, $domain->hashid]) }}">{{ trans('applications.records') }} </a></li>
    <li><a href="{{ route('app:record:create', [$user->hashid, $domain->hashid]) }}">{{ trans('applications.add dns record') }} </a></li>

@elseif($sidemenu == 'email')

    <li><a href="{{ route('app:email:index', [$user->hashid]) }}">{{ trans('applications.email account') }}</a></li>
    <li><a href="{{ route('app:email:create', [$user->hashid]) }}">{{ trans('applications.add email') }}</a></li>
    <li><a href="{{ route('app:ef:index', [$user->hashid]) }}">{{ trans('applications.email forwarding') }}</a></li>
    <li><a href="{{ route('app:ef:create', [$user->hashid]) }}">{{ trans('applications.add email forwarding') }}</a></li>
    <li><a href="{{ route('webmail:login') }}" target="_blank">{{ trans('applications.webmail login') }} </a></li>
    <li><a href="/roundcube" target="_blank">{{ trans('applications.roundcube') }} </a></li>

@elseif($sidemenu == 'webmail')

    <li><a href="{{ route('webmail') }}"> {{ trans('applications.email settings information') }}</a></li>
    <li><a href="{{ route('webmail:forwarding') }}">{{ trans('applications.email forwarding list') }}</a></li>
    <li><a href="{{ route('webmail:forwarding:create') }}">{{ trans('applications.add email forwarding') }}</a></li>

@elseif($sidemenu == 'file')

    <li><a href="{{ route('app:filemanager', $user->hashid) }}">{{ trans('applications.WHoP File Manager') }}</a></li>
    <li><a href="{{ route('app:ftp:index', $user->hashid) }}">{{ trans('applications.ftp accounts') }}</a></li>
    <li><a href="{{ route('app:ftp:create', [$user->hashid]) }}">{{ trans('applications.add ftp account') }}</a></li>

@elseif($sidemenu == 'advance setting')

    <li><a href="{{ route('app:cron:index', $user->hashid) }}">{{ trans('applications.crontab manager') }}</a></li>
    <li><a href="{{ route('app:cron:create', [$user->hashid]) }}">{{ trans('applications.add cron job') }}</a></li>
    <li><a href="{{ route('app:supervisor:index', $user->hashid) }}">{{ trans('applications.supervisor') }}</a></li>
    <li><a href="{{ route('app:supervisor:create', $user->hashid) }}">{{ trans('applications.add supervisor worker') }}</a></li>

@elseif($sidemenu == 'package')

    <li><a href="{{ route('packages:index') }}">{{ trans('navigation.packages') }}</a></li>
    <li><a href="{{ route('packages:create') }}">{{ trans('navigation.add_package') }}</a></li>

@elseif($sidemenu == 'server')

    <li><a href="{{ route('serversetting:dovecot') }}">{{ trans('navigation.dovecot') }}</a></li>
    <li><a href="{{ route('serversetting:mariadb') }}">{{ trans('navigation.mariadb') }}</a></li>
    <li><a href="{{ route('serversetting:php') }}">{{ trans('navigation.php') }}</a></li>
    <li><a href="{{ route('serversetting:postfix') }}">{{ trans('navigation.postfix') }}</a></li>
    <li><a href="{{ route('serversetting:pdns') }}">{{ trans('navigation.pdns') }}</a></li>
    <li><a href="{{ route('serversetting:pure-ftpd') }}">{{ trans('navigation.pure-ftpd') }}</a></li>
    <li><a href="{{ route('serversetting:spamassassin') }}">{{ trans('navigation.spamassassin') }}</a></li>
    <li><a href="{{ route('serversetting:ssh') }}">{{ trans('navigation.ssh') }}</a></li>
    <li><a href="{{ route('serversetting:nginx') }}">{{ trans('navigation.nginx') }}</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="{{ route('serversetting:csf') }}">{{ trans('navigation.csf') }}</a></li>

@elseif($sidemenu == 'user')

    <li><a href="{{ route('clients:index', ['filter' => 'all']) }}">{{ trans('navigation.clients') }}</a></li>
    <li><a href="{{ route('clients:create') }}">{{ trans('navigation.add_client') }}</a></li>

@elseif($sidemenu == 'whop')

    <li><a href="{{ route('whopsetting:certificate') }}">{{ trans('navigation.panel_certificate') }}</a></li>
    <li><a href="{{ route('whopsetting:email') }}">{{ trans('navigation.panel_email') }}</a></li>
    <li><a href="{{ route('whopsetting:license') }}">{{ trans('navigation.license') }}</a></li>
    <li><a href="{{ route('whopsetting:panel') }}">{{ trans('navigation.panel_setting') }}</a></li>

@endif
