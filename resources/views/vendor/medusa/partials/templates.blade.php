@if ($template == "passwordGenerator")

<template id="passwordGeneratorComponent">

    <div class="form-group">
        <label for="">{{ trans('form.password') }}</label>

        <div class="input-group">
            <input type="password" class="form-control" name="password" v-model="password" placeholder="{{ trans('form.password') }}">
            <span class="input-group-btn">
                <button class="btn btn-primary" type="button" v-on:click="openGeneratorModal()">{{ trans('core.generate password') }}</button>
            </span>
        </div>
    </div>

    <div class="form-group">
        <label for="">{{ trans('form.verify password') }}</label>
        <input type="password" class="form-control" name="verifyPassword" v-model="verifypassword" placeholder="{{ trans('form.verify password') }}">
    </div>


    <div class="modal modal-primary fade" id="@{{ modalid }}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="alert alert-warning">
                        {{ trans('messages.password generator warning') }}
                    </div>

                    <div class="form-group">
                        <label for="">{{ trans('core.password length') }}</label>

                        <select v-model="passwordlength" class="form-control" required="required" @change="generateRandomPassword">
                            <option v-for="x in range" value="@{{ x }}">@{{ x }}</option>
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="">{{ trans('core.generated password') }}</label>

                        <div class="input-group">
                            <input type="text" class="form-control" id="copyText" v-model="generatedPassword" readonly aria-label>
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-copy" type="button" data-clipboard-target="#copyText" v-on:click="copy()">
                                    <span class="fa fa-copy"></span>
                                </button>
                            </span>
                        </div>
                        <p v-show="message != null" class="text-danger"><br>@{{ message }}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success pull-left" v-on:click="generateRandomPassword()">{{ trans('core.generate password') }}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" @click="close()">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-primary" v-on:click="usePassword()">{{ trans('core.use password') }}</button>
                </div>
            </div>
        </div>
    </div>
</template>


@elseif ($template == "usageProgress")

    <template id="usageProgress">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2">@{{ title }}</label>
                <div class="col-sm-7">
                    <div class="progress">
                        <div class="progress-bar active progress-bar-@{{ color }}" role="progressbar" aria-valuenow="@{{ value }}" aria-valuemin="0" aria-valuemax="@{{ max }}" style="width: @{{ percentage }}%"></div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <span class="progressText">@{{ value }} / @{{ max }}</span>
                </div>
            </div>
        </form>
    </template>


@elseif ($template == "ajaxerror")

    <template id="ajaxError">
        <div class="alert alert-danger" v-if="errors.length != 0">
            <p v-for="error in errors"><i class="fa fa-circle-o"></i> @{{ error }}</p>
        </div>
    </template>


@elseif ($template == "servicestatus")

    <template id="servicestatus">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>@{{ title }}</h4>
                        <p></p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">{{ trans('serversetting.average_cpu_usage') }} <span class="pull-right">@{{ cpu }}</span></li>
                        <li class="list-group-item">{{ trans('serversetting.average_memory_usage') }} <span class="pull-right">@{{ memory }}</span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row" v-if="startstopbtn || restartbtn">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.service_control') }}</h4>
                        <p></p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item clearfix" v-if="startstopbtn">
                            {{ trans('serversetting.startstop') }}
                            <div class="pull-right">
                                <button type="button" class="btn btn-success" v-bind:disabled="switch || isBusy" v-on:click="toggle('start')">{{ trans('form.start') }}</button>
                                <button type="button" class="btn btn-danger" v-bind:disabled="!switch || isBusy" v-on:click="toggle('stop')">{{ trans('form.stop') }}</button>
                            </div>
                        </li>
                        <li class="list-group-item clearfix" v-if="restartbtn">{{ trans('serversetting.restart') }}<div class="pull-right"><button class="btn btn-primary" v-bind:disabled="isBusy" v-on:click="restart()">{{ trans('form.restart') }}</button></div></li>
                    </ul>
                </div>
            </div>
        </div>

    </template>

@elseif ($template == 'check-job')

    <template id="checkJob">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="jobMessages.length > 0">
            <h3>@{{ title }}</h3>
            <p class="text-center">@{{ jobMessages[jobMessages.length - 1] }}</p>
            <div class="progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: @{{ percentage }}%;">
                    @{{ percentage }}%
                </div>
            </div>
        </div>
    </template>

@elseif ($template == "browse-folder")

    <template id="browseFolder">

        <div class="form-group">
            <label for="">@{{ label }}</label>

            <div class="input-group">
                <div class="input-group-addon btn-primary">@{{ addon }}</div>
                <input type="text" class="form-control" name="@{{ model }}" v-model="homefolder" value="@{{ currentvalue }}" placeholder="@{{ placeholder }}" readonly>
                <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" @click="chooseHomeFolder">{{ trans('form.browse') }}</button>
                </span>
            </div>
        </div>

        <div class="modal modal-primary fade" id="@{{ listmodalid }}">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{ trans('form.browse') }}</h4>
                    </div>
                    <div class="modal-body">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                        <table v-show="!isBusy" class="table table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{{ trans('applications.file/folder name') }}</th>
                                    <th>{{ trans('applications.type') }}</th>
                                    <th>{{ trans('applications.permission') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-show="structure.parent != structure.currentLocation" @dblclick="goToParent()">
                                    <td colspan="5">../</td>
                                </tr>
                                <tr v-show="isError">
                                    <td colspan="5">@{{ structure.message }}</td>
                                </tr>
                                <tr v-else="isBusy && isError" v-for="data in structure.content" @dblclick="walk(data)" class="directory">
                                    <td>
                                        <span class="fa fa-folder" v-show="data.type == 'inode/directory'"></span>
                                        <span class="fa fa-file-o" v-else="data.type == 'inode/directory'"></span>
                                    </td>
                                    <td>@{{ data.name }}</td>
                                    <td>@{{ data.type }}</td>
                                    <td>@{{ data.permission }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success pull-left" @click="createDirectory">{{ trans('applications.new folder') }}</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                        <button type="button" class="btn btn-primary" @click="choose">{{ trans('form.choose') }}</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal modal-primary fade" id="@{{ createmodalid }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{ trans('applications.new folder') }}</h4>
                    </div>
                    <div class="modal-body">
                        <p v-show="errorMessages.length > 0" v-for="error in errorMessages">@{{ error }}</p>
                        <form @submit.prevent="createDirectoryCommit" method="POST" role="form">
                            <div class="form-group">
                                <label for="">{{ trans('form.folder name') }}</label>
                                <input type="text" class="form-control" v-model="create.name" placeholder="{{ trans('form.folder name') }}" autofocus>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.close') }}</button>
                        <button type="button" class="btn btn-primary" @click="createDirectoryCommit">{{ trans('form.create') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </template>

@endif
