@if ($modal == "universalcertificate")

    <div class="modal modal-primary fade" id="modalSelectCertificate">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('form.select_certificate') }}</h4>
                </div>
                <div class="modal-body">
                    <ul class="list-group" v-show="!isBusy && !isEmpty">
                        <li class="list-group-item" v-for="x in certificates" v-on:click="setCertificate(x.cert)"><span class="fa fa-certificate"></span> @{{ x.cert }}</li>
                    </ul>

                    <p v-show="isBusy">{{ trans('core.loading') }}</p>

                    <p v-show="isEmpty">{{ trans('core.no_certificate') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="cancel()">{{ trans('form.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == "deletepackage")

    <div class="modal modal-primary fade" id="deletePackage">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('form.delete_package') }} @{{ package.name }}</h4>
                </div>
                <form action="@{{ url }}" method="POST" role="form">
                    <div class="modal-body">
                        <p>{{ trans('messages.confirm_delete') }} @{{ package.name }}?</p>
                        {{ csrf_field() }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="clearInit()" :disabled="isBusy">{{ trans('form.cancel') }}</button>
                        <button type="submit" class="btn btn-danger" :disabled="isBusy"><span class="fa fa-refresh fa-spin" v-show="isBusy"></span> {{ trans('form.delete') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@elseif ($modal == "viewcertificate")

    <div class="modal modal-primary modal-primary fade" id="viewCertificateModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('form.view') }} @{{ certificate }}</h4>
                </div>
                <div class="modal-body">
                    @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    <pre v-show="!isBusy">@{{ certificateData }}</pre>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="close()" v-bind="{ 'disabled': isBusy }">{{ trans('form.close') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == "deletecertificate")

    <div class="modal modal-primary fade" id="deleteCertificateModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('form.delete') }} @{{ certificate }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>
                    <p v-show="!isBusy">{{ trans('core.rsync_certificate') }} {{ trans('messages.confirm_delete') }} @{{ certificate }}?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="close()" :disabled="isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" v-on:click="deleteCertificate()" :disabled="isBusy">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == "deleteclientcertificate")

    <div class="modal modal-primary fade" id="deleteCertificateModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('form.delete') }} @{{ certificate }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>
                    <p v-show="!isBusy">{{ trans('messages.confirm_delete') }} @{{ certificate }}?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on:click="close()" :disabled="isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" v-on:click="deleteCertificate()" :disabled="isBusy">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == "universalPostModal")

    <div class="modal modal-primary fade" id="universalPostModal">
        <div class="modal-dialog">
            <form action="@{{ url }}" method="POST" role="form">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" v-show="postMessage != null">{{ trans('messages.confirm action') }}</h4>
                        <h4 class="modal-title" v-show="deleteMessage != null">{{ trans('form.delete') }}</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}

                        @{{ postMessage }}

                        @{{ deleteMessage }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                        <button type="submit" class="btn btn-danger" v-show="deleteMessage != null">{{ trans('form.delete') }}</button>
                        <button type="submit" class="btn btn-primary" v-show="postMessage != null">{{ trans('form.submit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@elseif ($modal == "viewEmailSetting")

    <div class="modal modal-primary fade" id="viewEmailSettingModal">
        <div class="modal-dialog">
            <form action="@{{ url }}" method="POST" role="form">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">{{ trans('form.settings') }}</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>{{ trans('form.setting') }}</th>
                                    <th>{{ trans('form.value') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(key, setting) in settings">
                                    <td>@{{ key }}</td>
                                    <td>@{{ setting }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.close') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@elseif ($modal == 'createFile')

    <div class="modal modal-primary fade" id="createFileModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.new file') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <form v-show="!isBusy" @submit.prevent="createCommit" method="POST" role="form">
                        <div class="form-group">
                            <label for="">{{ trans('form.file name') }}</label>
                            <input type="text" class="form-control" v-model="create.name" placeholder="{{ trans('form.file name') }}" autofocus>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-primary" @click="createCommit" v-show="!isBusy">{{ trans('form.create') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == 'createFolder')

    <div class="modal modal-primary fade" id="createFolderModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.new folder') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <form v-show="!isBusy" @submit.prevent="createCommit" method="POST" role="form">
                        <div class="form-group">
                            <label for="">{{ trans('form.folder name') }}</label>
                            <input type="text" class="form-control" v-model="create.name" placeholder="{{ trans('form.folder name') }}" autofocus>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-primary" @click="createCommit" v-show="!isBusy">{{ trans('form.create') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == 'renameFileFolder')

    <div class="modal modal-primary fade" id="renameFileFolderModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.rename') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <form v-show="!isBusy" @submit.prevent="renameCommit" method="POST" role="form">
                        <div class="form-group">
                            <label for="">{{ trans('form.new name') }}</label>
                            <input type="text" class="form-control" v-model="rename.newName" placeholder="{{ trans('form.new name') }}" autofocus>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-primary" @click="renameCommit" v-show="!isBusy">{{ trans('form.save') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif ($modal == 'deleteFileFolder')

    <div class="modal modal-primary fade" id="deleteFileFolderModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.delete') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <p v-show="!isBusy" >{{ trans('messages.confirm delete file/folder') }}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-danger" @click="deleteCommit" v-show="!isBusy">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>


@elseif ($modal == 'changePermission')

    <div class="modal modal-primary fade" id="changePermissionModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.change permission') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <form v-show="!isBusy" @submit.prevent="permissionCommit" method="POST" role="form">
                        <h4>{{ trans('applications.permission mode') }}: @{{ permissionSum }}</h4>

                        <div class="form-group">
                            <label for="">{{ trans('form.user') }}</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.user.read"> {{ trans('form.read') }}
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.user.write"> {{ trans('form.write') }}
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.user.execute"> {{ trans('form.execute') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                       <div class="form-group">
                            <label for="">{{ trans('form.group') }}</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.group.read"> {{ trans('form.read') }}
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.group.write"> {{ trans('form.write') }}
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.group.execute"> {{ trans('form.execute') }}
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="">{{ trans('form.world') }}</label>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.world.read"> {{ trans('form.read') }}
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.world.write"> {{ trans('form.write') }}
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="checkbox-inline">
                                        <input type="checkbox"  v-model="permission.world.execute"> {{ trans('form.execute') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" @click="resetPermission()"  v-show="!isBusy">{{ trans('form.close') }}</button>
                    <button type="button" class="btn btn-primary" @click="permissionCommit" v-show="!isBusy">{{ trans('form.submit') }}</button>
                </div>
            </div>
        </div>
    </div>


@elseif ($modal == 'editorMode')

    <div class="modal modal-primary fade" id="editorModeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.editor mode') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="row">
                            @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                        </div>

                        <form v-show="!isBusy" @submit.prevent="changeModeCommit" method="POST" role="form">
                            <div class="form-group">
                                <label for="">{{ trans('applications.editor mode') }}</label>
                                <select v-model="mode" class="form-control" required="required" v-bind:change="applyMode()">
                                    <option value="ace/mode/abap">ABAP</option>
                                    <option value="ace/mode/actionscript">ActionScript</option>
                                    <option value="ace/mode/ada">ADA</option>
                                    <option value="ace/mode/apache_conf">Apache Conf</option>
                                    <option value="ace/mode/asciidoc">AsciiDoc</option>
                                    <option value="ace/mode/assembly_x86">Assembly x86</option>
                                    <option value="ace/mode/autohotkey">AutoHotKey</option>
                                    <option value="ace/mode/batchfile">BatchFile</option>
                                    <option value="ace/mode/c9search">C9Search</option>
                                    <option value="ace/mode/c_cpp">C and C++</option>
                                    <option value="ace/mode/cirru">Cirru</option>
                                    <option value="ace/mode/clojure">Clojure</option>
                                    <option value="ace/mode/cobol">Cobol</option>
                                    <option value="ace/mode/coffee">CoffeeScript</option>
                                    <option value="ace/mode/coldfusion">ColdFusion</option>
                                    <option value="ace/mode/csharp">C#</option>
                                    <option value="ace/mode/css">CSS</option>
                                    <option value="ace/mode/curly">Curly</option>
                                    <option value="ace/mode/d">D</option>
                                    <option value="ace/mode/dart">Dart</option>
                                    <option value="ace/mode/diff">Diff</option>
                                    <option value="ace/mode/dockerfile">Dockerfile</option>
                                    <option value="ace/mode/dot">Dot</option>
                                    <option value="ace/mode/dummy">Dummy</option>
                                    <option value="ace/mode/dummysyntax">DummySyntax</option>
                                    <option value="ace/mode/eiffel">Eiffel</option>
                                    <option value="ace/mode/ejs">EJS</option>
                                    <option value="ace/mode/elixir">Elixir</option>
                                    <option value="ace/mode/elm">Elm</option>
                                    <option value="ace/mode/erlang">Erlang</option>
                                    <option value="ace/mode/forth">Forth</option>
                                    <option value="ace/mode/ftl">FreeMarker</option>
                                    <option value="ace/mode/gcode">Gcode</option>
                                    <option value="ace/mode/gherkin">Gherkin</option>
                                    <option value="ace/mode/gitignore">Gitignore</option>
                                    <option value="ace/mode/glsl">Glsl</option>
                                    <option value="ace/mode/golang">Go</option>
                                    <option value="ace/mode/groovy">Groovy</option>
                                    <option value="ace/mode/haml">HAML</option>
                                    <option value="ace/mode/handlebars">Handlebars</option>
                                    <option value="ace/mode/haskell">Haskell</option>
                                    <option value="ace/mode/haxe">haXe</option>
                                    <option value="ace/mode/html">HTML</option>
                                    <option value="ace/mode/html_ruby">HTML (Ruby)</option>
                                    <option value="ace/mode/ini">INI</option>
                                    <option value="ace/mode/io">Io</option>
                                    <option value="ace/mode/jack">Jack</option>
                                    <option value="ace/mode/jade">Jade</option>
                                    <option value="ace/mode/java">Java</option>
                                    <option value="ace/mode/javascript">JavaScript</option>
                                    <option value="ace/mode/json">JSON</option>
                                    <option value="ace/mode/jsoniq">JSONiq</option>
                                    <option value="ace/mode/jsp">JSP</option>
                                    <option value="ace/mode/jsx">JSX</option>
                                    <option value="ace/mode/julia">Julia</option>
                                    <option value="ace/mode/latex">LaTeX</option>
                                    <option value="ace/mode/less">LESS</option>
                                    <option value="ace/mode/liquid">Liquid</option>
                                    <option value="ace/mode/lisp">Lisp</option>
                                    <option value="ace/mode/livescript">LiveScript</option>
                                    <option value="ace/mode/logiql">LogiQL</option>
                                    <option value="ace/mode/lsl">LSL</option>
                                    <option value="ace/mode/lua">Lua</option>
                                    <option value="ace/mode/luapage">LuaPage</option>
                                    <option value="ace/mode/lucene">Lucene</option>
                                    <option value="ace/mode/makefile">Makefile</option>
                                    <option value="ace/mode/markdown">Markdown</option>
                                    <option value="ace/mode/mask">Mask</option>
                                    <option value="ace/mode/matlab">MATLAB</option>
                                    <option value="ace/mode/mel">MEL</option>
                                    <option value="ace/mode/mushcode">MUSHCode</option>
                                    <option value="ace/mode/mysql">MySQL</option>
                                    <option value="ace/mode/nix">Nix</option>
                                    <option value="ace/mode/objectivec">Objective-C</option>
                                    <option value="ace/mode/ocaml">OCaml</option>
                                    <option value="ace/mode/pascal">Pascal</option>
                                    <option value="ace/mode/perl">Perl</option>
                                    <option value="ace/mode/pgsql">pgSQL</option>
                                    <option value="ace/mode/php" selected>PHP</option>
                                    <option value="ace/mode/powershell">Powershell</option>
                                    <option value="ace/mode/praat">Praat</option>
                                    <option value="ace/mode/prolog">Prolog</option>
                                    <option value="ace/mode/properties">Properties</option>
                                    <option value="ace/mode/protobuf">Protobuf</option>
                                    <option value="ace/mode/python">Python</option>
                                    <option value="ace/mode/r">R</option>
                                    <option value="ace/mode/rdoc">RDoc</option>
                                    <option value="ace/mode/rhtml">RHTML</option>
                                    <option value="ace/mode/ruby">Ruby</option>
                                    <option value="ace/mode/rust">Rust</option>
                                    <option value="ace/mode/sass">SASS</option>
                                    <option value="ace/mode/scad">SCAD</option>
                                    <option value="ace/mode/scala">Scala</option>
                                    <option value="ace/mode/scheme">Scheme</option>
                                    <option value="ace/mode/scss">SCSS</option>
                                    <option value="ace/mode/sh">SH</option>
                                    <option value="ace/mode/sjs">SJS</option>
                                    <option value="ace/mode/smarty">Smarty</option>
                                    <option value="ace/mode/snippets">snippets</option>
                                    <option value="ace/mode/soy_template">Soy Template</option>
                                    <option value="ace/mode/space">Space</option>
                                    <option value="ace/mode/sql">SQL</option>
                                    <option value="ace/mode/stylus">Stylus</option>
                                    <option value="ace/mode/svg">SVG</option>
                                    <option value="ace/mode/tcl">Tcl</option>
                                    <option value="ace/mode/tex">Tex</option>
                                    <option value="ace/mode/text">Text</option>
                                    <option value="ace/mode/textile">Textile</option>
                                    <option value="ace/mode/toml">Toml</option>
                                    <option value="ace/mode/twig">Twig</option>
                                    <option value="ace/mode/typescript">Typescript</option>
                                    <option value="ace/mode/vala">Vala</option>
                                    <option value="ace/mode/vbscript">VBScript</option>
                                    <option value="ace/mode/velocity">Velocity</option>
                                    <option value="ace/mode/verilog">Verilog</option>
                                    <option value="ace/mode/vhdl">VHDL</option>
                                    <option value="ace/mode/xml">XML</option>
                                    <option value="ace/mode/xquery">XQuery</option>
                                    <option value="ace/mode/yaml">YAML</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-primary" @click="changeModeCommit" v-show="!isBusy">{{ trans('form.submit') }}</button>
                </div>
            </div>
        </div>
    </div>


@elseif ($modal == 'editorTheme')

    <div class="modal modal-primary fade" id="editorThemeModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.editor theme') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <form v-show="!isBusy" @submit.prevent="changeThemeCommit" method="POST" role="form">
                        <div class="form-group">
                            <label for="">{{ trans('applications.editor theme') }}</label>
                            <select v-model="theme" class="form-control" required="required" v-bind:change="applyTheme()">
                                <optgroup label="Bright">
                                    <option value="ace/theme/chrome">Chrome</option>
                                    <option value="ace/theme/clouds">Clouds</option>
                                    <option value="ace/theme/crimson_editor">Crimson Editor</option>
                                    <option value="ace/theme/dawn">Dawn</option>
                                    <option value="ace/theme/dreamweaver">Dreamweaver</option>
                                    <option value="ace/theme/eclipse">Eclipse</option>
                                    <option value="ace/theme/github">GitHub</option>
                                    <option value="ace/theme/iplastic">IPlastic</option>
                                    <option value="ace/theme/solarized_light">Solarized Light</option>
                                    <option value="ace/theme/textmate">TextMate</option>
                                    <option value="ace/theme/tomorrow">Tomorrow</option>
                                    <option value="ace/theme/xcode">XCode</option>
                                    <option value="ace/theme/kuroir">Kuroir</option>
                                    <option value="ace/theme/katzenmilch">KatzenMilch</option>
                                    <option value="ace/theme/sqlserver">SQL Server</option>
                                </optgroup>
                                <optgroup label="Dark">
                                    <option value="ace/theme/ambiance">Ambiance</option>
                                    <option value="ace/theme/chaos">Chaos</option>
                                    <option value="ace/theme/clouds_midnight">Clouds Midnight</option>
                                    <option value="ace/theme/cobalt">Cobalt</option>
                                    <option value="ace/theme/idle_fingers">idle Fingers</option>
                                    <option value="ace/theme/kr_theme">krTheme</option>
                                    <option value="ace/theme/merbivore">Merbivore</option>
                                    <option value="ace/theme/merbivore_soft">Merbivore Soft</option>
                                    <option value="ace/theme/mono_industrial">Mono Industrial</option>
                                    <option value="ace/theme/monokai">Monokai</option>
                                    <option value="ace/theme/pastel_on_dark">Pastel on dark</option>
                                    <option value="ace/theme/solarized_dark">Solarized Dark</option>
                                    <option value="ace/theme/terminal">Terminal</option>
                                    <option value="ace/theme/tomorrow_night">Tomorrow Night</option>
                                    <option value="ace/theme/tomorrow_night_blue">Tomorrow Night Blue</option>
                                    <option value="ace/theme/tomorrow_night_bright">Tomorrow Night Bright</option>
                                    <option value="ace/theme/tomorrow_night_eighties">Tomorrow Night 80s</option>
                                    <option value="ace/theme/twilight">Twilight</option>
                                    <option value="ace/theme/vibrant_ink">Vibrant Ink</option>
                                </optgroup>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-primary" @click="changeThemeCommit" v-show="!isBusy">{{ trans('form.submit') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif($modal == 'removewhopletdomain')

    <div class="modal modal-primary fade" id="removeWhopletDomainModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.remove whoplet domain') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>
                    <div v-show="!isBusy">
                        @{{ deleteMessage }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-danger" @click="deleteDomainCommit" v-show="!isBusy">{{ trans('form.remove') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif($modal == 'deletewhoplet')

    <div class="modal modal-primary fade" id="deleteWhopletModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.delete whoplet') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <div v-show="!isBusy">
                        {{ trans('messages.confirm delete whoplet') }}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-danger" @click="deleteWhopletCommit">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif($modal == 'deletecron')

    <div class="modal modal-primary fade" id="deleteCronModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">{{ trans('applications.delete cronjob') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <div v-show="!isBusy">{{ trans('messages.verify delete cronjob') }}</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-danger" @click="deleteJobCommit" v-show="!isBusy">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif($modal == 'reloadsupervisor')

    <div class="modal modal-primary fade" id="reloadSupervisorModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.reload supervisor') }}</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <div v-show="!isBusy">{{ trans('messages.reload supervisor') }}</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-success" @click="reloadSupervisorCommit" v-show="!isBusy">{{ trans('form.reload') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif($modal == 'deletesupervisor')

    <div class="modal modal-primary fade" id="deleteSupervisorModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('applications.delete supervisor') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])
                    </div>

                    <div v-show="!isBusy">{{ trans('messages.delete supervisor') }}</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" v-show="!isBusy">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-danger" v-show="!isBusy" @click="deleteSupervisorCommit">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>
@elseif($modal == 'deleteclient')

    <div class="modal modal-primary fade" id="deleteClientModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('core.delete client') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">

                        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ajax-error></ajax-error>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="!isBusy">
                            {!! trans('messages.delete client message', ['username' => $user->username]) !!}

                            <div class="form-group margin-top-20">
                                <label for="">{{ trans('form.username') }}</label>
                                <input type="text" class="form-control" v-model="deleteClient.username">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" v-show="!isBusy">
                    <button type="button" class="btn btn-default" data-dismiss="modal" @click="cancelDelete">{{ trans('form.cancel') }}</button>
                    <button type="button" class="btn btn-danger" @click="deleteCommit">{{ trans('form.delete') }}</button>
                </div>
            </div>
        </div>
    </div>

@elseif($modal == 'resetexpiredaccount')

    <div class="modal modal-primary fade" id="resetExpiredAccountModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('core.reset account expiry') }}</h4>
                </div>
                <form action="@{{ url }}" method="POST" role="form">

                    {{ csrf_field() }}

                    <div class="modal-body">
                        {{ trans('messages.reset account message', ['validity' => $user->clientPackage->package->validFor]) }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ trans('form.reset') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@elseif($modal == 'resetwebsitequota')

    <div class="modal modal-primary fade" id="resetQuotaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ trans('core.reset website quota') }}</h4>
                </div>
                <form action="@{{ url }}" method="POST" role="form">

                    {{ csrf_field() }}

                    <div class="modal-body">
                        {!! trans('messages.reset quota message', ['username' => $user->username]) !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                        <button type="submit" class="btn btn-primary">{{ trans('form.reset') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@elseif($modal == 'enabledisableaccount')

    <div class="modal modal-primary fade" id="enableDisableAccountModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    @if ($user->disable == false)
                        <h4 class="modal-title">{{ trans('core.disable account') }}</h4>
                    @else
                        <h4 class="modal-title">{{ trans('core.enable account') }}</h4>
                    @endif
                </div>
                <form action="@{{ url }}" method="POST" role="form">

                    {{ csrf_field() }}
                    <div class="modal-body">
                        @if ($user->disable == false)
                            {{ trans('messages.disable account message') }}
                        @else
                            {{ trans('messages.enable account message') }}
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.cancel') }}</button>
                        @if ($user->disable == false)
                            <button type="submit" class="btn btn-primary">{{ trans('form.disable') }}</button>
                        @else
                            <button type="submit" class="btn btn-primary">{{ trans('form.enable') }}</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

@endif
