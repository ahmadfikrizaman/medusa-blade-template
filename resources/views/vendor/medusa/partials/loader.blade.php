@if ($type == 'ball-pulse')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" v-show="isBusy">
        <div class="loader-inner ball-pulse">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
@endif
