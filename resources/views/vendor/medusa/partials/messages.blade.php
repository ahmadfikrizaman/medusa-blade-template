@if ( count($errors) > 0 )
    <div class="alert alert-danger animated bounceIn">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        @foreach ($errors->all() as $error)
            <p><i class="fa fa-circle-o"></i> {{ $error }}</p>
        @endforeach
    </div>
@endif


@if (Session::has('errorMessage'))
    <div class="alert alert-danger animated bounceIn">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="fa fa-circle-o"></i> {{ Session::get('errorMessage') }}
    </div>
@endif


@if (Session::has('successMessage'))
<div class="alert alert-success animated bounceIn">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <i class="fa fa-circle-o"></i> {{ Session::get('successMessage') }}
</div>
@endif
