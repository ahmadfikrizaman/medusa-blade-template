@extends(WH::theme('layouts.master'), ['title' => trans('navigation.panel_email'), 'sidemenu' => 'whop', 'applicationTitle' => trans('navigation.whop_settings'), 'breadcrumb' => 'panel-email'])

@section('main-content')

    <div id="panelEmail">

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                @include(WH::theme('partials.messages'))
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('navigation.panel_email') }}</h4>
                        <p>This is the email setting for panel. This email setting will notify when user succesfully login to the panel.</p>
                        <form action="{{ route('whopsetting:email:update') }}" method="POST" role="form">
                            <div class="form-group">
                                <label for="">Host</label>
                                <input type="text" class="form-control" name="host" placeholder="Email host" value="{{ env('MAIL_HOST') }}">
                            </div>


                            <div class="form-group">
                                <label for="">Port</label>
                                <input type="integer" class="form-control" name="port" placeholder="Email port" value="{{ env('MAIL_PORT') }}">
                            </div>


                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Name of the user who send the email" value="{{ env('MAIL_NAME') }}">
                            </div>

                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="email" class="form-control" name="username" placeholder="The email address of the sender" value="{{ env('MAIL_USERNAME') }}">
                            </div>


                            <div class="form-group">
                                <label for="">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="The password of the email address" value="{{ env('MAIL_PASSWORD') }}">
                            </div>

                            <div class="form-group">
                                <label for="">Encryption (null, ssl, tls)</label>
                                <input type="text" class="form-control" name="encryption" placeholder="Email encryption" value="{{ env('MAIL_ENCRYPTION') }}">
                            </div>

                            {{ csrf_field() }}

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-submit pull-right">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop
