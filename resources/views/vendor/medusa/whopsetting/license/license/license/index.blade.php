@extends(WH::theme('layouts.master'), ['title' => trans('navigation.license'), 'breadcrumb' => 'license-setting'])


@section('content')

    <div class="row" id="licenseUploaderVM" v-cloak>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="page-header">
              <h3>{{ trans('core.update license') }}</h3>
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="validating">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ trans('messages.validating license') }}
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="licenseAlert != null">
            <div class="alert alert-danger">
                @{{ licenseAlert }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="licenseSuccess != null">
            <div class="alert alert-success">
                @{{ licenseSuccess }}
            </div>
        </div>


        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="!isBusy">
            <form action="{{ route('whopsetting:license:update') }}" enctype="multipart/form-data" method="POST" role="form" @submit.prevent="submit">

                <div class="form-group">
                    <label for="">{{ trans('form.upload license') }}</label>
                    <input id="licenseInput" type="file" name="license" accept="application/gzip" data-show-preview="false">
                </div>

            </form>
        </div>
    </div>
@stop
