@extends(WH::theme('layouts.master'), ['title' => trans('navigation.license'), 'breadcrumb' => 'license-setting', 'sidemenu' => 'whop', 'applicationTitle' => trans('navigation.license')])


@section('main-content')

    <div class="row" id="licenseUploaderVM" v-cloak>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="validating">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ trans('messages.validating license') }}
            </div>
        </div>


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="licenseAlert != null">
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @{{ licenseAlert }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="licenseSuccess != null">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @{{ licenseSuccess }}
            </div>
        </div>


        @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])


        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" v-show="!isBusy">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>{{ trans('core.update license') }}</h4>
                    <p>{{ trans('form.upload license') }}</p>
                    <form action="{{ route('whopsetting:license:update') }}" enctype="multipart/form-data" method="POST" role="form" @submit.prevent="submit">

                        <div class="form-group">
                            <label for="">{{ trans('navigation.license') }}</label>
                            <input id="licenseInput" type="file" name="license" accept="application/gzip" data-show-preview="false">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
