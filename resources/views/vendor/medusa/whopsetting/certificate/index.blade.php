@extends(WH::theme('layouts.master'), ['title' => trans('navigation.panel_certificate'), 'sidemenu' => 'whop', 'applicationTitle' => trans('navigation.whop_settings'), 'breadcrumb' => 'certificate-setting'])

@section('main-content')

    <div id="panelCertificate"
        data-list-url="{{ route('whopsetting:certificate:list') }}"
        data-view-url="{{ route('whopsetting:certificate:view') }}"
        data-update-url="{{ route('whopsetting:certificate:update') }}"
        data-destroy-url="{{ route('whopsetting:certificate:destroy') }}" v-clock>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.my_certificate') }}</h4>
                        <p></p>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>{{ trans('form.name') }}</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="x in certificates">
                                    <td>@{{ x.cert }}</td>
                                    <td class="text-center">
                                        <button class="btn btn-primary btn-sm" v-on:click="view(x.cert)" v-bind:disabled="isBusy"> {{ trans('form.view') }}</button>
                                        <button class="btn btn-info btn-sm" v-bind:disabled="x.cert == usingCert || isBusy" v-on:click="changePanelCertificate(x.cert)"> {{ trans('form.select') }}</button>
                                        <button class="btn btn-danger btn-sm" v-bind:disabled="x.cert == usingCert || isBusy" v-on:click="deleteConfirmation(x.cert)"> {{ trans('form.delete') }}</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.add_certificate') }}</h4>
                        <p></p>

                        <form role="form" action="{{ route('whopsetting:certificate:store') }}" method="POST" @submit.prevent="addCertificate">

                            <div class="form-group">
                                <label for="">{{ trans('form.certificate_name') }}</label>
                                <input type="text" name="certificateName" v-model="certificateName" class="form-control" placeholder="{{ trans('form.alpha_num') }}" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('core.public_certificate') }}</label>
                                <textarea v-model="publicCertificate" name="publicCertificate" class="form-control" rows="6" required="required" placeholder="{{ trans('core.public_certificate_placeholder') }}"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('core.private_key') }}</label>
                                <textarea v-model="privateKey" name="privateKey" class="form-control" rows="6" required="required" placeholder="{{ trans('core.private_key_placeholder') }}"></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include(WH::theme('partials.modal'), ['modal' => 'viewcertificate'])
        @include(WH::theme('partials.modal'), ['modal' => 'deletecertificate'])

    </div>

@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
