@extends(WH::theme('layouts.master'), ['title' => trans('navigation.panel_setting'), 'sidemenu' => 'whop', 'applicationTitle' => trans('navigation.whop_settings'), 'breadcrumb' => 'panel-setting'])

@section('main-content')

    <div id="panelSetting" data-list-url="{{ route('whopsetting:panel:phpversion') }}" v-clock>

        <div class="row">
            @include(WH::theme('partials.loader'), ['type' => 'ball-pulse'])

            <div v-show="!isBusy" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.theme') }}</h4>
                        <p></p>
                        <form action="{{ route('whopsetting:panel:update-theme') }}" method="POST" role="form">

                            {{ csrf_field() }}

                            <div class="form-group" data-toggle="tooltip" data-placement="top" title="Theme for WHoP. This will be fallback theme for clients and resellers">
                                <label for="">{{ trans('core.WHoP Theme') }}</label>
                                {!! Form::select('theme', $themes, env('WHOP_TEMPLATE'), ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.localization')  }}</h4>
                        <p></p>
                        <form action="{{ route('whopsetting:panel:update-date') }}" method="POST" role="form">

                            {{ csrf_field() }}

                            <div class="form-group" data-toggle="tooltip" data-placement="top">
                                <label for="">{{ trans('core.date format') }}</label>
                                {!! Form::select('dateFormat', $dateFormat, env('DATE'), ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.bruteforce') }}</h4>
                        <p></p>
                        <form action="{{ route('whopsetting:panel:update') }}" method="POST" role="form">

                            {{ csrf_field() }}

                            @foreach ( $settings as $setting )

                                @if ($setting->settingName == 'BruteforceTimes')

                                    <div class="form-group" data-toggle="tooltip" data-placement="top" title="How many times user can try login to panel before being block by WHoP">
                                        <label for="">{{ trans('core.Bruteforce Times') }}</label>
                                        <input type="number" class="form-control" name="BruteforceTimes" placeholder="" value="{{ $setting->value }}">
                                    </div>

                                @elseif ($setting->settingName == 'BruteforceMinutes')

                                    <div class="form-group" data-toggle="tooltip" data-placement="top" title="How many minutes user will be lock by WHoP before he/she can log in back">
                                        <label for="">{{ trans('core.Bruteforce Minutes') }}</label>
                                        <input type="number" class="form-control" name="BruteforceMinutes" placeholder="" value="{{ $setting->value }}">
                                    </div>

                                @endif

                            @endforeach
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('core.php_version') }}</h4>
                        <p></p>
                        <form role="form" action="{{ route('whopsetting:panel:updatePHP') }}" method="post" @submit.prevent="changePHPVersion">
                            <div class="form-group" data-toggle="tooltip" data-placement="top" title="EXPERIMENTAL!! PHP version to use for this panel and CLI version for root. Not adviseable to change to lower version. (No effect on any WHoPlet)">
                                <label for="">{{ trans('core.php_version') }}</label>
                                <select name="phpVersion" v-model="phpVersion" class="form-control" required="required">
                                    <option v-for="x in phpVersions" value="@{{ x }}">@{{ x }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy" >{{ trans('form.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
