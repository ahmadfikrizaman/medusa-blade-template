@extends(WH::theme('layouts.master'), ['title' => trans('navigation.csf'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'csf'])

@section('main-content')

    <div id="csfSetting"
        data-status-url="{{ route('serversetting:csf:status') }}"
        data-service-url="{{ route('serversetting:csf:service') }}"
        data-saveeditor-url="{{ route('serversetting:csf:saveeditor') }}"
        data-tools-url="{{ route('serversetting:csf:tools') }}" v-clock>

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.csf_service_status') }}</h4>
                        <p></p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item clearfix">
                            {{ trans('serversetting.startstop') }} ({{ trans('serversetting.csf-start-stop') }})
                            <div class="pull-right">
                                <button type="button" class="btn btn-success" v-bind:disabled="switch || isBusy" v-on:click="toggle('start')">{{ trans('form.start') }}</button>
                                <button type="button" class="btn btn-danger" v-bind:disabled="!switch || isBusy" v-on:click="toggle('stop')">{{ trans('form.stop') }}</button>
                            </div>
                        </li>
                        <li class="list-group-item clearfix">{{ trans('serversetting.restart') }} ({{ trans('serversetting.csf-restart') }})<div class="pull-right"><button class="btn btn-primary" v-bind:disabled="isBusy" v-on:click="restart()">Restart</button></div></li>
                    </ul>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div role="tabpanel" class="medusa-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#tools" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('serversetting.csf_tools') }}</a>
                        </li>
                        <li role="presentation">
                            <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('form.csf_setting') }}</a>
                        </li>
                        <li role="presentation">
                            <a href="#csfAllow" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('form.csf_allow') }}</a>
                        </li>
                        <li role="presentation">
                            <a href="#csfDeny" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('form.csf_deny') }}</a>
                        </li>
                        <li role="presentation">
                            <a href="#csfIgnore" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('form.csf_ignore') }}</a>
                        </li>
                        <li role="presentation">
                            <a href="#csfPignore" aria-controls="settings" role="tab" data-toggle="tab">{{ trans('form.csf_pignore') }}</a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        {{-- CSF Tools --}}
                        <div role="tabpanel" class="tab-pane active" id="tools">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>{{ trans('serversetting.csf_service_status') }}</h4>
                                    <p></p>
                                </div>
                                <ul class="list-group">
                                    <li class="list-group-item clearfix">
                                        {{ trans('serversetting.view_blocked_ip') }}
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-primary" v-bind:disabled="isBusy" v-on:click="tools('t')">{{ trans('form.view') }}</button>
                                        </div>
                                    </li>
                                    <li class="list-group-item clearfix">
                                        {{ trans('serversetting.unblocked_remove') }}
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-primary" v-bind:disabled="isBusy" v-on:click="tools('df')">{{ trans('form.unblock_remove') }}</button>
                                        </div>
                                    </li>
                                    <li class="list-group-item clearfix">
                                        {{ trans('serversetting.flush_temporary') }}
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-primary" v-bind:disabled="isBusy" v-on:click="tools('tf')">{{ trans('form.flush') }}</button>
                                        </div>
                                    </li>
                                    <li class="list-group-item clearfix">
                                        {{ trans('serversetting.add_csf_allow') }}
                                        <div class="input-group margin-top-bottom-20">
                                            <input type="text" class="form-control" v-model="ip" placeholder="{{ trans('form.ip_address') }}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button" v-bind:disabled="isBusy" v-on:click="tools('ca')">{{ trans('form.add') }}</button>
                                            </span>
                                        </div>
                                    </li>
                                    <li class="list-group-item clearfix">
                                        {{ trans('serversetting.add_csf_deny') }}
                                        <div class="input-group margin-top-bottom-20">
                                            <input type="text" class="form-control" v-model="ip" placeholder="{{ trans('form.ip_address') }}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button" v-bind:disabled="isBusy" v-on:click="tools('cd')">{{ trans('form.add') }}</button>
                                            </span>
                                        </div>
                                    </li>
                                    <li class="list-group-item clearfix">
                                        {{ trans('serversetting.ping_cluster') }}
                                        <div class="pull-right">
                                            <button type="button" class="btn btn-primary" v-bind:disabled="isBusy" v-on:click="tools('cp')">{{ trans('form.ping') }}</button>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>{{ trans('core.results') }}</h4>
                                    <p></p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            @{{ results }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- CSF Settings --}}
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 margin-top-20">
                                    <div class="alert alert-danger">
                                        {{ trans('serversetting.csf-danger') }}
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h4>{{ trans('form.csf_setting') }}</h4>
                                            <p></p>
                                            <form @submit.prevent="save" action="{{ route('serversetting:csf:saveconfig') }}" method="POST" role="form">

                                                <h4>{{ trans('form.email') }}</h4>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_ALERT_TO') }}</label>
                                                    <input type="email" class="form-control" v-model="LF_ALERT_TO">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_ALERT_FROM') }}</label>
                                                    <input type="email" class="form-control" v-model="LF_ALERT_FROM">
                                                </div>

                                                <br>
                                                <h4>{{ trans('form.csf_basic') }}</h4>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.TCP_IN') }}</label>
                                                    <input type="text" class="form-control" v-model="TCP_IN">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.TCP_OUT') }}</label>
                                                    <input type="text" class="form-control" v-model="TCP_OUT">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.UDP_IN') }}</label>
                                                    <input type="text" class="form-control" v-model="UDP_IN">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.UDP_OUT') }}</label>
                                                    <input type="text" class="form-control" v-model="UDP_OUT">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.SYNFLOOD') }}</label>
                                                    <select v-model="SYNFLOOD" class="form-control" required="required">
                                                        <option value="0">{{ trans('form.disable') }}</option>
                                                        <option value="1">{{ trans('form.enable') }}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.UDPFLOOD') }}</label>
                                                    <select v-model="UDPFLOOD" class="form-control" required="required">
                                                        <option value="0">{{ trans('form.disable') }}</option>
                                                        <option value="1">{{ trans('form.enable') }}</option>
                                                    </select>
                                                </div>

                                                <br>
                                                <h4>{{ trans('form.csf_temp_to_perm_block') }}</h4>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_PERMBLOCK_COUNT') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_PERMBLOCK_COUNT">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_PERMBLOCK_ALERT') }}</label>
                                                    <select v-model="LF_PERMBLOCK_ALERT" class="form-control" required="required">
                                                        <option value="0">{{ trans('form.disable') }}</option>
                                                        <option value="1">{{ trans('form.enable') }}</option>
                                                    </select>
                                                </div>

                                                <br>
                                                <h4>{{ trans('form.csf_lfd') }}</h4>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_EMAIL_ALERT') }}</label>
                                                    <select v-model="LF_EMAIL_ALERT" class="form-control" required="required">
                                                        <option value="0">{{ trans('form.disable') }}</option>
                                                        <option value="1">{{ trans('form.enable') }}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SSHD') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_SSHD">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SSHD_PERM') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_SSHD_PERM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_FTPD') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_FTPD">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_FTPD_PERM') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_FTPD_PERM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SMTPAUTH') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_SMTPAUTH">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SMTPAUTH_PERM') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_SMTPAUTH_PERM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_POP3D') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_POP3D">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_POP3D_PERM') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_POP3D_PERM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_IMAPD') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_IMAPD">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_IMAPD_PERM') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_IMAPD_PERM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SUHOSIN') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_SUHOSIN">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SUHOSIN_PERM') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_SUHOSIN_PERM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_SSH_EMAIL_ALERT') }}</label>
                                                    <select v-model="LF_SSH_EMAIL_ALERT" class="form-control" required="required">
                                                        <option value="0">{{ trans('form.disable') }}</option>
                                                        <option value="1">{{ trans('form.enable') }}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_CONSOLE_EMAIL_ALERT') }}</label>
                                                    <select v-model="LF_CONSOLE_EMAIL_ALERT" class="form-control" required="required">
                                                        <option value="0">{{ trans('form.disable') }}</option>
                                                        <option value="1">{{ trans('form.enable') }}</option>
                                                    </select>
                                                </div>

                                                <br>
                                                <h4>{{ trans('form.csf_dirwatch') }}</h4>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.LF_DIRWATCH') }}</label>
                                                    <input type="number" class="form-control" v-model="LF_DIRWATCH">
                                                </div>

                                                <br>
                                                <h4>{{ trans('form.csf_processtracking') }}</h4>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.PT_LIMIT') }}</label>
                                                    <input type="number" class="form-control" v-model="PT_LIMIT">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.PT_USERPROC') }}</label>
                                                    <input type="number" class="form-control" v-model="PT_USERPROC">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.PT_USERMEM') }}</label>
                                                    <input type="number" class="form-control" v-model="PT_USERMEM">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.PT_USERTIME') }}</label>
                                                    <input type="number" class="form-control" v-model="PT_USERTIME">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">{{ trans('form.PT_FORKBOMB') }}</label>
                                                    <input type="number" class="form-control" v-model="PT_FORKBOMB">
                                                </div>


                                                <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy">{{ trans('form.save') }}</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- CSF Allow --}}
                        <div role="tabpanel" class="tab-pane" id="csfAllow">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>{{ trans('form.csf_setting') }}</h4>
                                    <p></p>
                                </div>
                                <div id="csfAllowEditor"></div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary pull-right btn-submit" v-bind:disabled="isBusy" v-on:click="saveConfig('allow')">{{ trans('form.save') }}</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        {{-- CSF Deny --}}
                        <div role="tabpanel" class="tab-pane" id="csfDeny">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>{{ trans('form.csf_deny') }}</h4>
                                    <p></p>
                                </div>
                                <div id="csfDenyEditor"></div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary pull-right btn-submit" v-bind:disabled="isBusy" v-on:click="saveConfig('deny')">{{ trans('form.save') }}</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        {{-- CSF Ignore --}}
                        <div role="tabpanel" class="tab-pane" id="csfIgnore">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>{{ trans('form.csf_deny') }}</h4>
                                    <p></p>
                                </div>
                                <div id="csfIgnoreEditor"></div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary pull-right btn-submit" v-bind:disabled="isBusy" v-on:click="saveConfig('ignore')">{{ trans('form.save') }}</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        {{-- CSF Pignore --}}
                        <div role="tabpanel" class="tab-pane" id="csfPignore">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h4>{{ trans('form.csf_deny') }}</h4>
                                    <p></p>
                                </div>
                                <div id="csfPignoreEditor"></div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary pull-right btn-submit" v-bind:disabled="isBusy" v-on:click="saveConfig('pignore')">{{ trans('form.save') }}</button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

@stop
