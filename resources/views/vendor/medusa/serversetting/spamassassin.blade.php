@extends(WH::theme('layouts.master'), ['title' => trans('navigation.spamassassin'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'spamassassin'])

@section('main-content')

    <div id="spamassassinSetting" v-clock>

        <service-status
            title="{{ trans('serversetting.spamassassin_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#Spamassassin#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:spamassassin:status') }}"
            serviceurl="{{ route('serversetting:spamassassin:service') }}"
            servicelistener="serversetting#SpamassassinController#{{ WH::credentialString() }}"
            :startstopbtn="true"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.spamassassin_setting') }}</h4>
                        <p></p>
                        <form role="form" action="{{ route('serversetting:spamassassin:saveconfig') }}" method="POST" @submit.prevent="submit">
                            <div class="form-group">
                                <label for="">{{ trans('form.report_safe') }}</label>
                                <select v-model="report_safe" class="form-control" required="required">
                                    <option value="0">{{ trans('form.report_safe_0') }}</option>
                                    <option value="1">{{ trans('form.report_safe_1') }}</option>
                                    <option value="2">{{ trans('form.report_safe_2') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.required_score') }}</label>
                                <input type="number" step="any" class="form-control" v-model="required_score">
                            </div>

                            <button type="submit" class="btn btn-primary pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@stop
