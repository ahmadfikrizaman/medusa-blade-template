@extends(WH::theme('layouts.master'), ['title' => trans('navigation.dovecot'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'dovecot'])

@section('main-content')

    <div id="dovecotSetting" data-listcertificate-url="{{ route('serversetting:dovecot:listuniversalcertificate') }}" v-clock>

        <service-status
            title="{{ trans('serversetting.dovecot_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#Dovecot#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:dovecot:status') }}"
            serviceurl="{{ route('serversetting:dovecot:service') }}"
            servicelistener="serversetting#DovecotController#{{ WH::credentialString() }}"
            :startstopbtn="true"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.dovecot_setting') }}</h4>
                        <p></p>

                        <form role="form" action="{{ route('serversetting:dovecot:setcertificate') }}" method="POST" @submit.prevent="submit">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-primary" v-on:click="chooseCertificate()">{{ trans('form.select_certificate') }}</button>
                                    </span>
                                    <input type="text" class="form-control" v-model="certificate" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy || !certificate">{{ trans('form.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include(WH::theme('partials.modal'), ['modal' => 'universalcertificate'])

    </div>

@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@endsection
