@extends(WH::theme('layouts.master'), ['title' => trans('navigation.php'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'php'])

@section('main-content')

    <div id="phpSetting" data-config-url="{{ route('serversetting:php:getconfig') }}" v-clock>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.php_setting') }}</h4>
                        <p></p>
                        <form role="form" action="{{ route('serversetting:php:saveconfig') }}" method="post" @submit.prevent="save">
                            <div class="form-group">
                                <label for="">{{ trans('form.expose_php') }}</label>
                                <select v-model="expose_php" class="form-control" required="required">
                                    <option value="On">{{ trans('form.on') }}</option>
                                    <option value="Off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.max_execution_time') }}</label>
                                <input type="number" v-model="max_execution_time" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.max_input_time') }}</label>
                                <input type="number" v-model="max_input_time" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.memory_limit') }}</label>
                                <input type="number" v-model="memory_limit" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.display_errors') }}</label>
                                <select v-model="display_errors" class="form-control" required="required">
                                    <option value="On">{{ trans('form.on') }}</option>
                                    <option value="Off">{{ trans('form.off') }}</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="">{{ trans('form.post_max_size') }}</label>
                                <input type="number" v-model="post_max_size" class="form-control" required="required">
                            </div>


                            <div class="form-group">
                                <label for="">{{ trans('form.file_uploads') }}</label>
                                <select v-model="file_uploads" class="form-control" required="required">
                                    <option value="On">{{ trans('form.on') }}</option>
                                    <option value="Off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.upload_max_filesize') }}</label>
                                <input type="number" v-model="upload_max_filesize" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.allow_url_fopen') }}</label>
                                <select v-model="allow_url_fopen" class="form-control" required="required">
                                    <option value="On">{{ trans('form.on') }}</option>
                                    <option value="Off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.allow_url_include') }}</label>
                                <select v-model="allow_url_include" class="form-control" required="required">
                                    <option value="On">{{ trans('form.on') }}</option>
                                    <option value="Off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.session_name') }}</label>
                                <input type="text" v-model="session_name" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.session_gc_maxlifetime') }}</label>
                                <input type="number" v-model="session_gc_maxlifetime" class="form-control" required="required">
                            </div>

                            <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
