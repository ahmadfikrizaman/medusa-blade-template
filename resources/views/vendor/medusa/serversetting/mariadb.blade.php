@extends(WH::theme('layouts.master'), ['title' => trans('navigation.mariadb'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'mariadb'])

@section('main-content')

    <div id="mariaDBSetting" v-clock>

        <service-status
            title="{{ trans('serversetting.mariadb_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#MariaDB#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:mariadb:status') }}"
            serviceurl="{{ route('serversetting:mariadb:service') }}"
            servicelistener="serversetting#MariaDBController#{{ WH::credentialString() }}"
            :startstopbtn="false"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.mariadb_setting') }}</h4>
                        <p></p>

                        <form role="form" action="{{ route('serversetting:mariadb:saveconfig') }}" method="post" @submit.prevent="save">
                            <div class="form-group">
                                <label for="">{{ trans('form.innodb_buffer_pool_size') }}</label>
                                <input type="number" v-model="innodb_buffer_pool_size" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.max_connections') }}</label>
                                <input type="number" v-model="max_connections" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.query_cache_size') }}</label>
                                <input type="number" v-model="query_cache_size" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.query_cache_limit') }}</label>
                                <input type="number" v-model="query_cache_limit" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.query_cache_min_res_unit') }}</label>
                                <input type="number" v-model="query_cache_min_res_unit" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.thread_cache_size') }}</label>
                                <input type="number" v-model="thread_cache_size" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.innodb_buffer_pool_instances') }}</label>
                                <input type="number" v-model="innodb_buffer_pool_instances" class="form-control" required="required">
                            </div>

                            <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@stop
