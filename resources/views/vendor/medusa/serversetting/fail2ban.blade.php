@extends(WH::theme('layouts.master'), ['title' => trans('navigation.fail2ban'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'fail2ban'])

@section('main-content')

    <div id="fail2banSetting" v-clock>

        <service-status
            title="{{ trans('serversetting.fail2ban_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#Fail2Ban#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:fail2ban:status') }}"
            serviceurl="{{ route('serversetting:fail2ban:service') }}"
            servicelistener="serversetting#Fail2BanController#{{ WH::credentialString() }}"
            :startstopbtn="false"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="alert alert-danger">
                    {{ trans('serversetting.fail2ban-danger') }}
                </div>
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.fail2ban_setting') }}</h4>
                        <p></p>
                        <form role="form" action="{{ route('serversetting:fail2ban:saveconfig') }}" method="POST" @submit.prevent="save">
                            <div class="form-group">
                                <label for="">{{ trans('form.ignoreip') }}</label>
                                <input type="text" v-model="ignoreip" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.bantime') }}</label>
                                <input type="number" v-model="bantime" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.findtime') }}</label>
                                <input type="number" v-model="findtime" class="form-control" required="required">
                            </div>

                            <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy" v-on:click="save()">{{ trans('form.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@stop
