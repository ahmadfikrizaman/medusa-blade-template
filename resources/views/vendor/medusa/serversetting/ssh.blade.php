@extends(WH::theme('layouts.master'), ['title' => trans('navigation.ssh'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'ssh'])

@section('main-content')

    <div id="sshSetting" v-clock>

        <service-status
            title="{{ trans('serversetting.ssh_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#SSH#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:ssh:status') }}"
            serviceurl="{{ route('serversetting:ssh:service') }}"
            servicelistener="serversetting#SSHController#{{ WH::credentialString() }}"
            :startstopbtn="false"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="alert alert-warning">
                    {{ trans('serversetting.ssh-warning') }}
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.ssh_setting') }}</h4>
                        <p></p>

                        <form role="form" action="{{ route('serversetting:ssh:saveconfig') }}" method="post" @submit.prevent="submit">
                            <div class="form-group">
                                <label for="">{{ trans('form.UseDNS') }}</label>
                                <select v-model="UseDNS" class="form-control" required="required">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@stop
