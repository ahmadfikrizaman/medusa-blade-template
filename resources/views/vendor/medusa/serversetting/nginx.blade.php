@extends(WH::theme('layouts.master'), ['title' => trans('navigation.nginx'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'nginx'])

@section('main-content')

    <div id="nginxSetting" v-clock>

        <service-status
            title="{{ trans('serversetting.nginx_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#Nginx#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:nginx:status') }}"
            serviceurl="{{ route('serversetting:nginx:service') }}"
            servicelistener="serversetting#NginxController#{{ WH::credentialString() }}"
            :startstopbtn="false"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.nginx_setting') }}</h4>
                        <p></p>
                        <form role="form" action="{{ route('serversetting:nginx:saveconfig') }}" method="POST" @submit.prevent="save">
                            <div class="form-group">
                                <label for="">{{ trans('form.worker_processes') }}</label>
                                <input type="text" v-model="worker_processes" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.worker_rlimit_nofile') }}</label>
                                <input type="number" v-model="worker_rlimit_nofile" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.worker_connections') }}</label>
                                <input type="number" v-model="worker_connections" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.multi_accept') }}</label>
                                <select v-model="multi_accept" class="form-control" required="required">
                                    <option value="on">{{ trans('form.on') }}</option>
                                    <option value="off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.sendfile') }}</label>
                                <select v-model="sendfile" class="form-control" required="required">
                                    <option value="on">{{ trans('form.on') }}</option>
                                    <option value="off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.tcp_nopush') }}</label>
                                <select v-model="tcp_nopush" class="form-control" required="required">
                                    <option value="on">{{ trans('form.on') }}</option>
                                    <option value="off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.tcp_nodelay') }}</label>
                                <select v-model="tcp_nodelay" class="form-control" required="required">
                                    <option value="on">{{ trans('form.on') }}</option>
                                    <option value="off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.client_body_timeout') }}</label>
                                <input type="number" v-model="client_body_timeout" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.client_header_timeout') }}</label>
                                <input type="number" v-model="client_header_timeout" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.keepalive_timeout') }}</label>
                                <input type="number" v-model="keepalive_timeout" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.client_header_buffer_size') }}</label>
                                <input type="number" v-model="client_header_buffer_size" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.client_max_body_size') }}</label>
                                <input type="text" v-model="client_max_body_size" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.open_file_cache_valid') }}</label>
                                <input type="text" v-model="open_file_cache_valid" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.open_file_cache_min_uses') }}</label>
                                <input type="number" v-model="open_file_cache_min_uses" class="form-control" required="required">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.open_file_cache_errors') }}</label>
                                <select v-model="open_file_cache_errors" class="form-control" required="required">
                                    <option value="on">{{ trans('form.on') }}</option>
                                    <option value="off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.keepalive_requests') }}</label>
                                <input type="number" v-model="keepalive_requests" class="form-control" required="required">
                            </div>


                            <div class="form-group">
                                <label for="">{{ trans('form.reset_timedout_connection') }}</label>
                                <select v-model="reset_timedout_connection" class="form-control" required="required">
                                    <option value="on">{{ trans('form.on') }}</option>
                                    <option value="off">{{ trans('form.off') }}</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary btn-submit pull-right" v-bind:disabled="isBusy" v-on:click="save()">{{ trans('form.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@stop
