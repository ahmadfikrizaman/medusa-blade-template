@extends(WH::theme('layouts.master'), ['title' => trans('navigation.pdns'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'pdns'])

@section('main-content')

    <div id="pdnsSetting" v-clock>
        <service-status
            title="{{ trans('serversetting.pdns_service_status') }}"
            :isBusy="isBusy"
            statuslistener="serversetting#PDNS#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:pdns:status') }}"
            serviceurl="{{ route('serversetting:pdns:service') }}"
            servicelistener="serversetting#PDNSController#{{ WH::credentialString() }}"
            :startstopbtn="true"
            :restartbtn="true">
        </service-status>
    </div>

@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@stop
