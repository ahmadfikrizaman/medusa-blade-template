@extends(WH::theme('layouts.master'), ['title' => trans('navigation.pure-ftpd'), 'sidemenu' => 'server', 'applicationTitle' => trans('navigation.server_settings'), 'breadcrumb' => 'pure-ftpd'])

@section('main-content')

    <div id="pureFTPDSetting" v-clock>

        <service-status
            title="{{ trans('serversetting.pure-ftpd_service_status') }}"
            :is-busy="isBusy"
            statuslistener="serversetting#PureFTPD#{{ WH::credentialString() }}"
            statusurl="{{ route('serversetting:pure-ftpd:status') }}"
            serviceurl="{{ route('serversetting:pure-ftpd:service') }}"
            servicelistener="serversetting#PureFTPDController#{{ WH::credentialString() }}"
            :startstopbtn="true"
            :restartbtn="true">
        </service-status>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <ajax-error></ajax-error>
                <div class="alert alert-danger">
                    {{ trans('serversetting.pure-ftpd-warning') }}
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>{{ trans('serversetting.pure-ftpd_setting') }}</h4>
                        <p></p>
                        <form role="form" action="{{ route('serversetting:pure-ftpd:saveconfig') }}" method="post" @submit.prevent="submit">
                            <div class="form-group">
                                <label for="">{{ trans('form.MaxClientsNumber') }}</label>
                                <input type="number" class="form-control" v-model="MaxClientsNumber">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.MaxClientsPerIP') }}</label>
                                <input type="number" class="form-control" v-model="MaxClientsPerIP">
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="">{{ trans('form.PassivePortRangeFrom') }}</label>
                                        <input type="number" class="form-control" v-model="PassivePortRangeFrom">
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <div class="form-group">
                                        <label for="">{{ trans('form.PassivePortRangeTo') }}</label>
                                        <input type="number" class="form-control" v-model="PassivePortRangeTo">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.NoAnonymous') }}</label>
                                <select v-model="NoAnonymous" class="form-control" required="required">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.DisplayDotFiles') }}</label>
                                <select v-model="DisplayDotFiles" class="form-control" required="required">
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.MaxIdleTime') }}</label>
                                <input type="number" class="form-control" v-model="MaxIdleTime">
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('form.TLS') }}</label>
                                <select v-model="TLS" class="form-control" required="required">
                                    <option value="0">{{ trans('form.disabled') }}</option>
                                    <option value="1">{{ trans('form.enable_tls_and_non_tls') }}</option>
                                    <option value="2">{{ trans('form.disable_non_tls') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('core.public_certificate') }}</label>
                                <textarea v-model="publicCertificate" class="form-control" rows="6" required="required" placeholder="{{ trans('core.public_certificate_placeholder') }}"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="">{{ trans('core.private_key') }}</label>
                                <textarea v-model="privateKey" class="form-control" rows="6" required="required" placeholder="{{ trans('core.private_key_placeholder') }}"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary pull-right" v-bind:disabled="isBusy">{{ trans('form.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include(WH::theme('partials.templates'), ['template' => 'ajaxerror'])
@include(WH::theme('partials.templates'), ['template' => 'servicestatus'])
@stop
