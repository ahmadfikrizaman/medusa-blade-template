'use strict';

var MyUsername = $("meta[name='username']").attr('content');
var MyToken = $("meta[name='token']").attr('content');

var credential = {
    MyUsername: MyUsername,
    MyToken: MyToken,
};

var credentialString = credential.MyUsername + ':' + credential.MyToken;
var modelist = ace.require("ace/ext/modelist");

$(document).ready(function() {
    var url = window.location;
    $('ul.nav a[href="' + url + '"]').parent().addClass('active');
    // $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).parent().addClass('active');
    
    $('table.dataTable').DataTable({
        "lengthMenu": [[25, 50, 100, 200], [25, 50, 100, 200]],
    });
});

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

$(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover({
        container: 'body',
        trigger: 'hover'
    })
});

$('.modal').on('shown.bs.modal', function() {
  $(this).find('[autofocus]').focus();
});

function initDataTable(selector) {
    return $('table' + selector).DataTable({
        "lengthMenu": [[25, 50, 100, 200], [25, 50, 100, 200]],
    });
}

// instantiate clipboard
var clipboard = new Clipboard('.btn-copy');


// Fallback message for clipboard. thanks to clipboard.js
function clipBoardFallbackMessage(action) {
    var actionMsg = '';
    var actionKey = (action === 'cut' ? 'X' : 'C');

    if(/iPhone|iPad/i.test(navigator.userAgent)) {
        actionMsg = 'No support :(';
    }
    else if (/Mac/i.test(navigator.userAgent)) {
        actionMsg = 'Press ⌘-' + actionKey + ' to ' + action;
    }
    else {
        actionMsg = 'Press Ctrl+' + actionKey + ' to ' + action;
    }

    return actionMsg;
}


function toggle(source) {
    checkboxes = document.getElementsByName("permission[]");
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}


// Credit to http://stackoverflow.com/a/979997
function gup( name, url ) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}


/* Functions */
bytesToSize = function(bytes) {
if (bytes == 0) return '0 Byte';
var k = 1024;
var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
var i = Math.floor(Math.log(bytes) / Math.log(k));
return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

bytesToMB = function(bytes) {
var num = bytes / 1024 / 1024;
return num.toFixed(2) / 1;
}

KBToMB = function(kb) {
var num = kb / 1024;
return num.toFixed(2) / 1;
}


function initEditor(content, id, mode, theme, readonly) {
    var id = id || 'editor';
    var mode = mode || 'ace/mode/php';
    var theme = theme || 'ace/theme/github';
    var readonly = readonly || 0;

    $('#' + id).addClass('editor');
    var editor = ace.edit(id);
    editor.setTheme(theme);
    editor.getSession().setMode(mode);
    editor.setShowPrintMargin(false);
    readonly == 1 ? editor.setReadOnly(true) : editor.setReadOnly(false);
    editor.setValue(content);
    editor.gotoLine(1);

    return editor;
}


/**
 * Socket
 */
var socket = io();



/**
 * Setting
 */
ace.config.set("basePath", "/build/themes/bootstrap/js/ace");

Dropzone.autoDiscover = false;

Dropzone.options.dzUpload = {
    dictDefaultMessage: "<h3><i class='fa fa-cloud-upload fa-2x'></i> <strong> Click or drag & drop file here to upload</strong></h3><p>or click to pick manually</p>",
}


Chart.defaults.global.responsive = true;
Chart.defaults.global.maintainAspectRatio = false;


Vue.http.headers.common['X-CSRF-TOKEN'] = $("meta[name='X-CSRF-TOKEN']").attr('content');



Vue.component('password-generator', {

    template: '#passwordGeneratorComponent',

    props: {
        password: null,
        verifypassword: null,
        passwordlength: 10,
        modalid: null,
    },

    data: function() {
        return {
            generatedPassword: null,
            passwordLength: null,
            message: null,
            range: [],
        }
    },


    ready: function() {
        for (var i = 6; i <= 64; i++) {
            this.range.push(i);
        };
    },


    // events: {
    //     'generate-password': function(passwordLength) {

    //         $('#modalGeneratePassword').modal({
    //             keyboard: false,
    //             backdrop: 'static',
    //         });

    //         this.passwordLength = passwordLength
    //         this.generateRandomPassword(passwordLength);
    //     }
    // },


    methods: {

        openGeneratorModal: function() {
            $('#modalGeneratePassword').modal({
                keyboard: false,
                backdrop: 'static',
            });

            this.generateRandomPassword(this.passwordlength);
        },

        generateRandomPassword: function() {
            // Credit to hajiklist http://stackoverflow.com/a/1497512

            var length = this.passwordlength;
            var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+~`|}{[]\:;?><,./-=";
            var retVal = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
            this.generatedPassword = retVal;

            this.message = null;
        },

        usePassword: function() {
            $('#modalGeneratePassword').modal('toggle');

            this.password = this.generatedPassword;
            this.verifypassword = this.generatedPassword;

            this.message = null;
        },

        copy: function() {

            var _self = this;

            clipboard.on('success', function(e) {
                _self.message = 'Copied!';
            });

            clipboard.on('error', function(e) {
                _self.message = clipBoardFallbackMessage(e.action);
            });
        },

        close: function() {
            this.message = null;
            this.password = null;
            this.verifypassword = null;
        }
    }
});


Vue.component('usage-progress', {

    template: '#usageProgress',

    props: {
        title: null,
        value: null,
        max: null,
        percentage: null,
        color: null,
        completion: null,
    },
});


Vue.component('ajax-error', {

    template: '#ajaxError',

    data: function() {
        return {
            errors: [],
        }
    },

    events: {
        'generate-error': function(ajaxError) {

            var _self = this;
            this.errors = [];

            if (ajaxError.status == 422) {
                
                $.each(ajaxError.data, function(index, value) {
                    _self.errors.push(value[0]);
                });
                
            } else {
                toastr.error('Unknown error occurred!');
            }
        },

        'clear-error': function() {
            this.errors = [];
        },
    },
});


Vue.component('service-status', {
    template: '#servicestatus',

    props: {
        title: null,
        statusurl: null,
        statuslistener: null,
        isBusy: true,
        serviceurl: null,
        servicelistener: null,
        startstopbtn: true,
        restartbtn: true,
    },

    data: function() {
        return {
            cpu: null,
            memory: null,
            switch: null,
        };
    },

    events: {
        'restart': function() {

            this.restart();
        }
    },

    ready: function() {

        var _self = this;


        this.fetchStatus();

        socket.on(this.statuslistener, function(content) {
            _self.isBusy = false;


            if (content.sucess == false) {
                toastr.error(content.message);
            } else {
                if (typeof content.Memory != 'undefined') {
                    _self.memory = content.Memory;
                }
                if (typeof content.CPU != 'undefined') {
                    _self.cpu = content.CPU;

                    if (content.CPU == "Service not running") {
                        _self.switch = false;
                    } else {
                        _self.switch = true;
                    }
                }
            }
        });

        socket.on(this.servicelistener, function(content) {

            _self.isBusy = false;


            if (content.success == false) {
                toastr.error(content.message);
            } else {
                if (content.Command == 'start' && content.success == true) {
                    toastr.success('Succesfully start your service!');
                    _self.switch = true;
                } else if (content.Command == 'stop' && content.success == true) {
                    toastr.error('Succesfully stop your service!');
                    _self.switch = false;
                } else if (content.Command == 'restart' && content.success == true) {
                    toastr.info('Succesfully restart your service!');
                    _self.switch = true;
                }

                _self.fetchStatus();
            }

        });

    },

    methods: {
        toggle: function(state) {
            this.isBusy = true;
            
            var data = {
                state: state
            };

            this.$http({url: this.serviceurl, method: 'GET', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },

        restart: function() {
            this.isBusy = true;
            
            var data = {
                state: 'restart'
            };

            this.$http({url: this.serviceurl, method: 'GET', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },

        fetchStatus: function() {
            this.$http({url: this.statusurl, method: 'GET'});
        }
    }
});


Vue.component('browse-folder', {
    template: '#browseFolder',

    props: {
        fetchurl: null,
        fetchlistener: null,
        createfolderurl: null,
        createfolderlistener: null,
        homefolder: null,
        isBusy: false,
        isError: false,
        label: null,
        addon: null,
        placeholder: null,
        model: null,
        currentvalue: null,
        listmodalid: null,
        createmodalid: null,
    },

    data: function() {
        return {
            structure: {
                parent: '/',
                currentLocation: '/',
                message: null,
                content: null,  
            },

            create: {
                type: 'folder',
                name: null,
            },
            errorMessages: [],
        }
    },

    events: {
        'chooseHomeFolder': function() {

            this.chooseHomeFolder();
        }
    },


    ready: function() {
        var _self = this; 

        socket.on(this.fetchlistener, function(message) {

            _self.isBusy = false;

            if (message.success == false) {
                _self.isError = true;
            } else {
                _self.isError = false;
            }

            _self.structure = {
                parent: message.parent,
                currentLocation: message.currentLocation,
                message: message.message,
                content: message.content,
            };
        });

        socket.on(this.createfolderlistener, function(message) {

            _self.isBusy = false;

            if (message.success == true) {
                $('#' + _self.createmodalid).modal('hide');
                _self.create.name = null;
                _self.fetchList(_self.structure.currentLocation);
            } else {
                _self.errorMessages = [];
                _self.errorMessages.push(message.message);
            }
        });
    },

    methods: {
        chooseHomeFolder: function() {
            $('#' + this.listmodalid).modal({
                backdrop: 'static',
            });

            this.fetchList('/');
        },

        walk: function(item) {
            if (item.type == 'inode/directory') {
                this.fetchList(item.location);
            }
        },

        goToParent: function() {
            this.fetchList(this.structure.parent);
        },

        fetchList: function(folder) {
            var _self = this;

            this.isBusy = true;

            var data = {
                folder: folder,
            };

            this.$http({url: this.fetchurl, method: 'GET', data: data}).then(function(success) {
            }, function(error) {
                this.isBusy = false;
            });
        },

        choose: function() {
            $('#' + this.listmodalid).modal('hide');

            this.homefolder = this.structure.currentLocation;
            this.$dispatch('choosed-homefolder', this.homefolder);
        },

        createDirectory: function() {
            $('#' + this.createmodalid).modal({
                backdrop: 'static',
            });
        },

        createDirectoryCommit: function() {

            this.isBusy = true;
            this.errorMessages = [];

            var _self = this;

            var data = this.create;
            data.folder = this.structure.currentLocation;

            this.$http({url: this.createfolderurl, method: 'POST', data: data}).then(function(success) {
                
            }, function(error) {
                this.isBusy = false;

                if (error.status == 422) {

                    $('#' + _self.createmodalid).modal('hide');

                    $.each(error.data, function(key, value) {
                        $.each(value, function(key, error) {
                            _self.errorMessages.push(error);
                        });
                    });
                }
            });

        },
    },
});


Vue.component('check-job', {
    template: '#checkJob',

    props: {
        isBusy: true,
        jobname: null,
        checkurl: null,
        redirecturl: null,
        title: null,
    },

    data: function() {
        return {
            jobMessages: [],
            percentage: 0,
        };
    },

    events: {
        'clear-job': function() {

            this.jobMessages = [];
            this.percentage = 0;
        }
    },

    ready: function() {

        var _self = this;


        socket.on('jobs#' + this.jobname + '#' + credentialString, function(job) {

            console.log(job);

            _self.jobMessages = job.progress.messages;
            _self.percentage = job.progress.percentage;
            
            if (job.finish == false) {
                _self.isBusy = true;
            } else if (job.finish == true && _self.percentage == 0) {
                _self.jobMessages = [];
                _self.isBusy = false;
            } else if (job.finish == true && _self.percentage == 100) {
                setTimeout(function() {
                    if (_self.redirecturl != null) {
                        window.location.href = _self.redirecturl;
                    } else {
                        _self.jobMessages = [];
                        _self.isBusy = false;
                    }
                }, 1500);
            }
        });

        this.$http({url: this.checkurl, method: 'GET'});
    },
});