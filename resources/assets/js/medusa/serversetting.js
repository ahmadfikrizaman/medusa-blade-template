'use strict';

/**
 * MariaDB Setting
 * @param  {[type]} )        {                                               return IO.ServerSetting();        }    } [description]
 * @param  {[type]} ready: function()    {        const _self [description]
 * @return {[type]}          [description]
 */
new Vue({
    el: '#mariaDBSetting',
    data: {
        isBusy: true,
        innodb_buffer_pool_size: null,
        max_connections: null,
        query_cache_size: null,
        query_cache_limit: null,
        query_cache_min_res_unit: null,
        thread_cache_size: null,
        innodb_buffer_pool_instances: null,
    },
    ready: function() {
        var _self = this;


        socket.on('serversetting#MariaDBSetting#' + credentialString, function(settings) {
            _self.isBusy = false;

            if (settings.sucess == true) {
                for (var key in settings) {
                    _self[key] = settings[key];
                }
            } else {
                toastr.error(settings.message);
            }
        });



        socket.on('serversetting#MariaDBSettingSave#' + credentialString, function(json) {
            _self.isBusy = false;

            if (json.success == true) {
                _self.$broadcast('restart');
                toastr.succes(json.message);
            } else {
                toastr.error(json.message);
            }
        });
    },
    methods: {
        save: function(e) {
            this.isBusy = true;

            var data = {
                innodb_buffer_pool_size: this.innodb_buffer_pool_size,
                max_connections: this.max_connections,
                query_cache_size: this.query_cache_size,
                query_cache_limit: this.query_cache_limit,
                query_cache_min_res_unit: this.query_cache_min_res_unit,
                thread_cache_size: this.thread_cache_size,
                innodb_buffer_pool_instances: this.innodb_buffer_pool_instances,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    }
});


/**
 * Dovecot Setting
 * @param  {[type]} )      {                                              return IO.ServerSetting();        } [description]
 * @param  {[type]} }     [description]
 * @param  {[type]} ready: function()    {        var _self [description]
 * @return {[type]}        [description]
 */
new Vue({
    el: '#dovecotSetting',
    data: {
        isBusy: true,
        setting: 'Loading...',
        certificate: null,
        certificates: null,
        isEmpty: false,
    },
    ready: function() {
        var _self = this;



        socket.on('DovecotCert', function (json) {

            _self.isBusy = false;

            if (json.success == true) {
                _self.certificate = json.cert;
            } else {
                toastr.error(json.message);
            }
        });



        socket.on('serversetting#DovecotSettingSave#' + credentialString, function(json) {

            _self.isBusy = false;

            if (json.success == true) {
                toastr.succes(json.message);
                _self.$broadcast('restart');
            } else {
                toastr.error(json.message);
            }
        });


        socket.on('universal#ListCertificate#' + credentialString, function(jsonArray){
            _self.isBusy = false;
            _self.certificates = jsonArray;

            if (jsonArray.length == 0) {
                _self.isEmpty = true;
            }
        });
    },
    methods: {

        chooseCertificate: function() {
            var _self = this;
            this.isBusy = true;
            this.isEmpty = false;

            $('#modalSelectCertificate').modal({
                backdrop: 'static',
            }).on('shown.bs.modal', function (e) {
                _self.$http({url: $('#dovecotSetting').data('listcertificate-url'), method: 'GET'}).then(function() {

                }, function (error) {
                    this.isBusy = false;
                    toastr.error('Unknown error!');
                    $('#modalSelectCertificate').modal('hide');
                });

                $(this).off('shown.bs.modal');
            });
        },

        setCertificate: function(cert) {
            this.certificate = cert;
            $('#modalSelectCertificate').modal('hide');
        },

        cancel: function() {

        },

        submit: function(e) {
            this.isBusy = true;

            var data = {
                Cert: this.certificate,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                console.log(error);
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },
    }
});


/**
 * PHP Setting
 * @type {String}
 */
new Vue({
    el: '#phpSetting',
    data: {
        isBusy: true,

        expose_php: null,
        max_execution_time: null,
        max_input_time: null,
        memory_limit: null,
        display_errors: null,
        post_max_size: null,
        file_uploads: null,
        upload_max_filesize: null,
        allow_url_fopen: null,
        allow_url_include: null,
        session_name: null,
        session_gc_maxlifetime: null,
    },
    ready: function() {
        
        var _self = this;

        this.$http({url: $('#phpSetting').data('config-url'), method: 'GET'});


        socket.on('serversetting#PHP#' + credentialString, function(data) {
            _self.isBusy = false;
            
            for (var key in data) {
                _self[key] = data[key];
            }
        });


        socket.on('serversetting#PHPSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
            } else {
                toastr.succes(message.message);
            }
        });
    },
    methods: {
        save: function(e) {
            this.isBusy = true;

            var data = {
                expose_php: this.expose_php,
                max_execution_time: this.max_execution_time,
                max_input_time: this.max_input_time,
                memory_limit: this.memory_limit,
                display_errors: this.display_errors,
                post_max_size: this.post_max_size,
                file_uploads: this.file_uploads,
                upload_max_filesize: this.upload_max_filesize,
                allow_url_fopen: this.allow_url_fopen,
                allow_url_include: this.allow_url_include,
                session_name: this.session_name,
                session_gc_maxlifetime: this.session_gc_maxlifetime,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
});


/**
 * Postfix setting
 */
new Vue({
    el: '#postfixSetting',
    data: {
        isBusy: true,
        certificate: null,
        certificates: null,
    },
    ready: function() {
        var _self = this;



        socket.on('serversetting#PostfixCert#' + credentialString, function(cert) {
            _self.isBusy = false;
            _self.certificate = cert;
        });


        socket.on('universal#ListCertificate#' + credentialString, function(jsonArray){
            _self.isBusy = false;
            _self.certificates = jsonArray;

            if (jsonArray.length == 0) {
                _self.isEmpty = true;
            }
        });


        socket.on('serversetting#PostfixSettingSave#' + credentialString, function(json) {

            _self.isBusy = false;

            if (json.success == true) {
                toastr.succes('Succesfully save your config!');
                _self.$broadcast('restart');
            } else {
                toastr.error(json.error);
            }
        });
    },
    methods: {

        chooseCertificate: function() {
            var _self = this;
            this.isBusy = true;
            this.isEmpty = false;

            $('#modalSelectCertificate').modal({
                backdrop: 'static',
            }).on('shown.bs.modal', function (e) {
                _self.$http({url: $('#postfixSetting').data('listcertificate-url'), method: 'GET'}).then(function() {

                }, function (error) {
                    this.isBusy = false;
                    toastr.error('Unknown error!');
                    $('#modalSelectCertificate').modal('hide');
                });

                $(this).off('shown.bs.modal');
            });
        },

        setCertificate: function(cert) {
            this.certificate = cert;
            $('#modalSelectCertificate').modal('hide');
        },

        cancel: function() {

        },

        submit: function(e) {
            this.isBusy = true;

            var data = {
                Cert: this.certificate,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                console.log(error);
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },
    },
});



/**
 * PDNS Setting
 */
new Vue({
    el: '#pdnsSetting',
});


/**
 * Pure-FTPD
 */
new Vue({
    el: '#pureFTPDSetting',
    data: {
        isBusy: true,
        MaxClientsNumber: null,
        MaxClientsPerIP: null,
        PassivePortRangeFrom: null,
        PassivePortRangeTo: null,
        NoAnonymous: null,
        DisplayDotFiles: null,
        MaxIdleTime: null,
        TLS: null,
        publicCertificate: null,
        privateKey: null,
    },
    ready: function() {
        var _self = this;


        socket.on('serversetting#PureFTPDSetting#' + credentialString, function(content) {
            _self.isBusy = false;

            for (var key in content) {
                _self[key] = content[key];
            }
        });


        socket.on('serversetting#PureFTPDSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
            } else {
                toastr.succes(message.message);
                _self.$broadcast('restart');
            }
        });
    },
    methods: {

        submit: function(e) {

            this.isBusy = true;

            var data = {
                MaxClientsNumber: this.MaxClientsNumber,
                MaxClientsPerIp: this.MaxClientsPerIP,
                PassivePortRangeFrom: this.PassivePortRangeFrom,
                PassivePortRangeTo: this.PassivePortRangeTo,
                NoAnonymous: this.NoAnonymous,
                DisplayDotFiles: this.DisplayDotFiles,
                MaxIdleTime: this.MaxIdleTime,
                tls: this.TLS,
                publicCertificate: this.publicCertificate,
                privateKey: this.privateKey,
            };


            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        }
    },
});


/**
 * Spamassassin
 */
new Vue({
    el: '#spamassassinSetting',
    data: {
        isBusy: true,
        report_safe: null,
        required_score: null,
    },
    ready: function() {
        var _self = this;


        socket.on('serversetting#SpamassassinSettings#' + credentialString, function(content) {

            _self.isBusy = false;

            for (var key in content) {
                _self[key] = content[key];
            }
        });


        socket.on('serversetting#SpamassassinSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
            } else {
                _self.$broadcast('restart');
                toastr.succes(message.message);
            }
        });
    },
    methods: {
        submit: function(e) {

            this.isBusy = true;

            var data = {
                report_safe: this.report_safe,
                required_score: this.required_score,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        }
    },
});


/**
 * SSH
 * @param  {[type]} )      {                                              return IO.ServerSetting();        } [description]
 * @param  {[type]} }     [description]
 * @param  {[type]} ready: function()    {        var _self [description]
 * @return {[type]}        [description]
 */
new Vue({
    el: '#sshSetting',
    data: {
        isBusy: true,
        UseDNS: null,
    },
    ready: function() {
        var _self = this;


        socket.on('serversetting#SSHSetting#' + credentialString, function(content) {

            _self.isBusy = false;

            for (var key in content) {
                _self[key] = content[key];
            }
        });


        socket.on('serversetting#SSHSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
            } else {
                _self.$broadcast('restart');
                toastr.succes(message.message);
            }
        });
    },
    methods: {

        submit: function(e) {
            this.isBusy = true;

            var data = {
                UseDns: this.UseDNS,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
});


/**
 * Nginx Setting
 */
new Vue({
    el: '#nginxSetting',
    data: {
        isBusy: true,
        worker_processes: null,
        worker_rlimit_nofile: null,
        worker_connections: null,
        multi_accept: null,
        sendfile: null,
        tcp_nopush: null,
        tcp_nodelay: null,
        client_body_timeout: null,
        client_header_timeout: null,
        keepalive_timeout: null,
        client_header_buffer_size: null,
        client_max_body_size: null,
        open_file_cache_valid: null,
        open_file_cache_min_uses: null,
        open_file_cache_errors: null,
        keepalive_requests: null,
        reset_timedout_connection: null,
    },
    ready: function() {
        var _self = this;


        socket.on('serversetting#NginxSetting#' + credentialString, function(content) {

            _self.isBusy = false;

            for (var key in content) {
                _self[key] = content[key];
            }
        });


        socket.on('serversetting#NginxSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
                _self.$broadcast('restart');
            } else {
                toastr.succes(message.message);
            }
        });
    },
    methods: {
        save: function(e) {
            this.isBusy = true;


            var data = {
                worker_processes: this.worker_processes,
                worker_rlimit_nofile: this.worker_rlimit_nofile,
                worker_connections: this.worker_connections,
                multi_accept: this.multi_accept,
                sendfile: this.sendfile,
                tcp_nopush: this.tcp_nopush,
                tcp_nodelay: this.tcp_nodelay,
                client_body_timeout: this.client_body_timeout,
                client_header_timeout: this.client_header_timeout,
                keepalive_timeout: this.keepalive_timeout,
                client_header_buffer_size: this.client_header_buffer_size,
                client_max_body_size: this.client_max_body_size,
                open_file_cache_valid: this.open_file_cache_valid,
                open_file_cache_min_uses: this.open_file_cache_min_uses,
                open_file_cache_errors: this.open_file_cache_errors,
                keepalive_requests: this.keepalive_requests,
                reset_timedout_connection: this.reset_timedout_connection,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
});

/**
 * Fail2Ban
 */
new Vue({
    el: '#fail2banSetting',
    data: {
        isBusy: true,
        ignoreip: null,
        bantime: null,
        findtime: null,
    },
    ready: function() {
        var _self = this;


        socket.on('serversetting#Fail2BanSetting#' + credentialString, function(content) {
            _self.isBusy = false;

            for (var key in content) {
                _self[key] = content[key];
            }
        });



        socket.on('serversetting#Fail2BanSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
            } else {
                toastr.succes(message.message);
            }
        });
    },
    methods: {

        save: function(e) {

            this.isBusy = true;

            var data = {
                ignoreip: this.ignoreip,
                bantime: this.bantime,
                findtime: this.findtime,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        }
    },
});


/**
 * CSF 
 */
new Vue({
    el: '#csfSetting',
    data: {
        cpu: 'Loading...',
        memory: 'Loading...',
        isBusy: true,
        switch: false,
        results: 'No results to show...',
        ip: null,
        csfAllow: '',
        csfDeny: '',
        csfIgnore: '',
        csfPignore: '',

        // Email
        LF_ALERT_TO: null,
        LF_ALERT_FROM: null,

        // Basic
        TCP_IN: null,
        TCP_OUT: null,
        UDP_IN: null,
        UDP_OUT: null,
        ICMP_IN: null,
        ICMP_OUT: null,
        SYNFLOOD: null,
        UDPFLOOD: null,

        // Temp / Perm Block
        LF_PERMBLOCK_COUNT: null,
        LF_PERMBLOCK_ALERT: null,

        // Login failure
        LF_EMAIL_ALERT: null,
        LF_SSHD: null,
        LF_SSHD_PERM: null,
        LF_FTPD: null,
        LF_FTPD_PERM: null,
        LF_SMTPAUTH: null,
        LF_SMTPAUTH_PERM: null,
        LF_POP3D: null,
        LF_POP3D_PERM: null,
        LF_IMAPD: null,
        LF_IMAPD_PERM: null,
        LF_SUHOSIN: null,
        LF_SUHOSIN_PERM: null,

        // ssh
        LF_SSH_EMAIL_ALERT: null,
        LF_CONSOLE_EMAIL_ALERT: null,

        // dirwatch
        LF_DIRWATCH: null,

        // process tracking
        PT_LIMIT: null,
        PT_USERPROC: null,
        PT_USERMEM: null,
        PT_USERTIME: null,
        PT_FORKBOMB: null,
    },
    computed: {
        csfAllowEditor: function() {
            return initEditor(this.csfAllow, 'csfAllowEditor', 'text', 'monokai', false);
        },

        csfDenyEditor: function() {
            return initEditor(this.csfDeny, 'csfDenyEditor', 'text', 'monokai', false);
        },

        csfIgnoreEditor: function() {
            return initEditor(this.csfIgnore, 'csfIgnoreEditor', 'text', 'monokai', false);
        },

        csfPignoreEditor: function() {
            return initEditor(this.csfPignore, 'csfPignoreEditor', 'text', 'monokai', false);
        },
    },
    ready: function() {
        var _self = this;

        this.fetchStatus();



        socket.on('serversetting#CSF#' + credentialString, function(content) {
            _self.isBusy = false;

            if (content.Status == "disabled") {
                _self.switch = false;
            } else {
                _self.switch = true;
            }

            for (var key in content) {
                _self[key] = content[key];
            }
        });

        socket.on('serversetting#CSFController#' + credentialString, function(content) {
            _self.isBusy = false;

            if (content.Command == 'start' && content.success == true) {
                toastr.succes('Succesfully start your service!');
                _self.switch = true;
            } else if (content.Command == 'stop' && content.success == true) {
                toastr.error('Succesfully stop your service!');
                _self.switch = false;
            } else if (content.Command == 'restart' && content.success == true) {
                toastr.info('Succesfully restart your service!');
                _self.switch = true;
            }

            _self.fetchStatus();
        });


        socket.on('serversetting#CSFSettingSave#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == false) {
                toastr.error(message.message);
            } else {
                toastr.succes(message.message);
            }
        });


        //tools
        socket.on('serversetting#CSFTools#' + credentialString, function(results, tool) {
            _self.isBusy = false;
            _self.results = results;

            if (tool == "ca" || tool == "cd" || tool == "df") {
                setTimeout(function() {
                    _self.fetchStatus();
                }, 1000);
            }
        });



        //Receive config
        socket.on('CSFAllow', function(data) {
            _self.csfAllow = data;

            _self.csfAllowEditor.setValue(_self.csfAllow);
            _self.csfAllowEditor.gotoPageDown();
        });

        socket.on('CSFDeny', function(data) {
            _self.csfDeny = data;

            _self.csfDenyEditor.setValue(_self.csfDeny);
            _self.csfDenyEditor.gotoPageDown();
        });

        socket.on('CSFIgnore', function(data) {
            _self.csfIgnore = data;

            _self.csfIgnoreEditor.setValue(_self.csfIgnore);
            _self.csfIgnoreEditor.gotoPageDown();
        });

        socket.on('CSFPignore', function(data) {
            _self.csfPignore = data;

            _self.csfPignoreEditor.setValue(_self.csfPignore);
            _self.csfPignoreEditor.gotoPageDown();
        });


        // Save config
        socket.on('serversetting#CSFFilesSave#' + credentialString, function(results) {

            _self.isBusy = false;

            if (results.success == true) {
                toastr.succes(results.message);
                _self.fetchStatus();
            } else {
                toastr.error(results.message);
            }
        });
    },
    methods: {
        toggle: function(state) {
            this.isBusy = true;
            
            var data = {
                state: state,
            };

            this.stateControl(data);
        },

        restart: function() {
            this.isBusy = true;

            var data = {
                state: 'restart',
            };

            this.stateControl(data);
        },

        stateControl: function(data) {
            this.$http({url: $(csfSetting).data('service-url'), method: 'GET', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },

        tools: function(tool) {
            this.isBusy = true;

            var data = {
                tool: tool,
                ip: this.ip,
            };

            this.ip = null;

            this.$http({url: $(csfSetting).data('tools-url'), method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },

        save: function(e) {

            this.isBusy = true;

            // credential.csf = {};
            
            var data = {
                LF_ALERT_TO: this.LF_ALERT_TO,
                LF_ALERT_FROM: this.LF_ALERT_FROM,

                TCP_IN: this.TCP_IN,
                TCP_OUT: this.TCP_OUT,
                UDP_IN: this.UDP_IN,
                UDP_OUT: this.UDP_OUT,
                ICMP_IN: this.ICMP_IN,
                ICMP_OUT: this.ICMP_OUT,
                SYNFLOOD: this.SYNFLOOD,
                UDPFLOOD: this.UDPFLOOD,

                // Temp / Perm Block
                LF_PERMBLOCK_COUNT: this.LF_PERMBLOCK_COUNT,
                LF_PERMBLOCK_ALERT: this.LF_PERMBLOCK_ALERT,

                // Login failure
                LF_EMAIL_ALERT: this.LF_EMAIL_ALERT,
                LF_SSHD: this.LF_SSHD,
                LF_SSHD_PERM: this.LF_SSHD_PERM,
                LF_FTPD: this.LF_FTPD,
                LF_FTPD_PERM: this.LF_FTPD_PERM,
                LF_SMTPAUTH: this.LF_SMTPAUTH,
                LF_SMTPAUTH_PERM: this.LF_SMTPAUTH_PERM,
                LF_POP3D: this.LF_POP3D,
                LF_POP3D_PERM: this.LF_POP3D_PERM,
                LF_IMAPD: this.LF_IMAPD,
                LF_IMAPD_PERM: this.LF_IMAPD_PERM,
                LF_SUHOSIN: this.LF_SUHOSIN,
                LF_SUHOSIN_PERM: this.LF_SUHOSIN_PERM,

                // ssh
                LF_SSH_EMAIL_ALERT: this.LF_SSH_EMAIL_ALERT,
                LF_CONSOLE_EMAIL_ALERT: this.LF_CONSOLE_EMAIL_ALERT,

                // dirwatch
                LF_DIRWATCH: this.LF_DIRWATCH,

                // process tracking
                PT_LIMIT: this.PT_LIMIT,
                PT_USERPROC: this.PT_USERPROC,
                PT_USERMEM: this.PT_USERMEM,
                PT_USERTIME: this.PT_USERTIME,
                PT_FORKBOMB: this.PT_FORKBOMB,
            };

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },

        saveConfig: function(config) {

            this.isBusy = true;

            if (config == "allow" ) {

                var data = {
                    config: 'allow',
                    data: this.csfAllowEditor.getValue(),
                };

                this.saveConfigControl(data);

            } else if (config == "deny") {

                var data = {
                    config: 'deny',
                    data: this.csfDenyEditor.getValue(),
                };

                this.saveConfigControl(data);

            } else if (config == "ignore") {

                var data = {
                    config: 'ignore',
                    data: this.csfIgnoreEditor.getValue(),
                };

                this.saveConfigControl(data);

            } else if (config == "pignore") {

                var data = {
                    config: 'pignore',
                    data: this.csfPignoreEditor.getValue(),
                };

                this.saveConfigControl(data);
                
            } else {
                toastr.error('Unknown config file');
            }
        },

        saveConfigControl: function(data) {
            this.$http({url: $('#csfSetting').data('saveeditor-url'), method: 'POST', data: data}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        },

        fetchStatus: function() {

            this.$http({url: $(csfSetting).data('status-url'), method: 'GET'}).then(function() {

            }, function (error) {
                this.isBusy = false;
                toastr.error('Unknown error!');
            });
        }
    },
});