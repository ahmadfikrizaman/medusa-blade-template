'use strict';

/**
 * Admin Dashboard
 * @param  {[type]} ) {                   const _self [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#admindashboard',
    data: {
        kernelVersion: 'Loading...',
        processorName: 'Loading...',
        freeMemory: 'Loading...',
        totalMemory: 'Loading...',
        freeDiskSpace: 'Loading...',
        totalDiskSpace: 'Loading...',
        nodeVersion: 'Loading...',
        uptime: 'Loading...',
        nginxCount: 'Loading...',
        licenseOwner: 'Loading...',
        licenseType: 'Loading...',
        licenseExpiry: 'Loading...',
        logs: [],
        licenseAlert: null,
        expired: false,
    },
    ready: function() {

        const _self = this;

        /**
         * Fetch Data
         */
        this.$http({url: $("#admindashboard").data('url'), method: 'GET'}).then(function success(success) {
            var packageCtx = $("#packageChart");
            var packageData = success.data.packageUsage;

            var packageChart = new Chart(packageCtx,{
                type: 'doughnut',
                data: packageData,
                options: {
                    title: {
                        display: true,
                        text: 'Package Distribution',
                        fontFamily: 'LatoWebThin',
                        fontSize: 20,
                    },
                    legend: {
                        display: true,
                        position: "right"
                    }
                }
            });

        }, function error(error) {

        });

        var ctx = document.getElementById("liveLoad").getContext("2d"); 

        var gradient = ctx.createLinearGradient(0, 0, 0, 400);
        gradient.addColorStop(0, 'rgba(75, 192, 192, 0.7)');
        gradient.addColorStop(0.7, 'rgba(34, 49, 63, 0.5)');
        gradient.addColorStop(1, 'rgba(34, 49, 63, 0.5)');

        var gradient2 = ctx.createLinearGradient(0, 0, 0, 400);
        gradient2.addColorStop(0, 'rgba(255, 255, 255, 0.3)');
        gradient2.addColorStop(0.7, 'rgba(255, 255, 255, 0.03)');
        gradient2.addColorStop(1, 'rgba(255, 255, 255, 0.01)');

        var data = {
            labels: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            datasets: [
                {
                    label: "Load",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: gradient,
                    borderColor: "rgba(75, 192, 192, 1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 3,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                }
            ]
        };

        var liveLoad = new Chart(ctx, {
            type: 'line',
            data: data,
            options: {
                title: {
                    display: false,
                },
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 0,
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: false,
                            // zeroLineColor: 'rgba(34, 49, 63, 1)',
                        }
                    }],
                    xAxes: [{
                        display: false,
                        gridLines: {
                            display: false,
                            // drawBorder: true,
                            // zeroLineColor: 'rgba(34, 49, 63, 1)',
                            // drawTicks: true,
                            // tickMarkLength: 10,
                            // color: gradient2,
                        },
                    }]
                }
            }
        });


        socket.on('dashboard#LiveLoad#' + credentialString, function(load) {
            ct = new Date();
            liveLoad.data.labels.shift();
            liveLoad.data.labels.push(('0' + ct.getHours()).slice(-2) + ':' + ('0' + ct.getMinutes()).slice(-2) + ':' + ('0' + ct.getSeconds()).slice(-2));
            liveLoad.data.datasets[0].data.shift();
            liveLoad.data.datasets[0].data.push(load);
            liveLoad.update();
            liveLoad.clear();
        });


        socket.on('ServerInfo', function(kernelVersion, processorName, freeMemory, totalMemory, diskFree, diskTotal, version, load, uptime) {

            ct = new Date();
            liveLoad.data.labels.shift();
            liveLoad.data.labels.push(('0' + ct.getHours()).slice(-2) + ':' + ('0' + ct.getMinutes()).slice(-2) + ':' + ('0' + ct.getSeconds()).slice(-2));

            _self.kernelVersion = kernelVersion;
            _self.processorName = processorName;
            _self.freeMemory = bytesToSize(freeMemory);
            _self.totalMemory = bytesToSize(totalMemory);
            _self.freeDiskSpace = bytesToSize(diskFree);
            _self.totalDiskSpace = bytesToSize(diskTotal);
            _self.nodeVersion = version;
            _self.uptime = uptime;
        });


        socket.on('dashboard#LiveInfo#' + credentialString, function(uptime, freeMemory) {
            _self.uptime = uptime;
            _self.freeMemory = bytesToSize(freeMemory);
        });


        socket.on('dashboard#PHPVersionDistribution#' + credentialString, function(data) {
            var phpCtx = $("#phpChart");

            var phpChart = new Chart(phpCtx, {
                type: 'pie',
                data: data,
                options: {
                     title: {
                        display: true,
                        text: 'PHP Distribution',
                        fontFamily: 'LatoWebThin',
                        fontSize: 20,
                    },
                    legend: {
                        display: true,
                        position: "right"
                    }
                }
            });
        });


        socket.on('ServerInfo-NginxCount-Client', function(total) {
            _self.nginxCount = (total - 1); // minus 1 coz of panel whoplet
        });


        socket.on('ServerInfo-License-Client', function(license) {
            _self.licenseOwner = license.owner;
            _self.licenseType = license.type;
            _self.licenseExpiry = license.expiry;

            if (license.alert !== null) {
                _self.licenseAlert = license.alert;
            }

            if (license.expired) {
                _self.expired = true;
            }
        });


        // fetch log
        this.$http({url: $("#admindashboard").data('log'), method: 'GET'}).then(function success(data) {
            this.logs = data.data;
        }, function error(e) {});

        socket.on('WHoPLog', function (log) {

            if (_self.logs.length >= 10) {
                _self.logs.pop();
            }
            
            _self.logs.unshift(log);
        });
    }
});



/**
 * Package
 * @param  {[type]} url        [description]
 * @param  {[type]} item)      {                         this.url [description]
 * @param  {[type]} clearInit: function()    {                    this.url      [description]
 * @return {[type]}            [description]
 */
new Vue({
    el: '#listPackages',
    data: {
        url: null,
        package: null,
        isBusy: false,
    },
    methods: {
        initDelete: function(url, item) {
            this.url = url;
            this.package = item;
        },

        clearInit: function() {
            this.url = null;
            this.package = null;
            this.isBusy = false;
        }
    }
});


/**
 * Add Client
 * @param  {[type]} )      {                                    return IO.AddClient();        }    } [description]
 * @param  {[type]} ready: function(     [description]
 * @return {[type]}        [description]
 */
new Vue({
    el: '#addClient',
    data: {
        isBusy: true,
        isAjaxError: false,
        name: '',
        username: '',
        password: '',
        verifyPassword: '',
        email: '',
        package: '',
        domainName: '',
        errorMessages: [],
    },
    ready: function() {
        var _self = this;


        socket.on('client#VerifyAddClient#' + credentialString, function(json) {

            if (json.success == true) {
                _self.submit();
            } else {
                _self.isBusy = false;
                _self.errorMessages.push(json.message)
            }
        });


        socket.on('client#CreateClientProgress#' + credentialString, function(json) {

            _self.percentage = json.percentage;
            _self.successMessages = json.messages;

            if (json.percentage == 100) {
                setTimeout(function() {
                    _self.successMessages = [];
                    _self.isBusy = false;
                }, 1500);
            }
        });
    },
    methods: {
        submit: function() {
            this.isBusy = true;

            var data = {
                name: this.name,
                username: this.username,
                password: this.password,
                verifyPassword: this.verifyPassword,
                email: this.email,
                package: this.package,
                domainName: this.domainName,
            };

            this.$http({url: $('#formCreateClient').data('store-action'), method: 'POST', data: data}).then(function() {
                this.isAjaxError = false;
                this.errorMessages = [];
                this.$broadcast('clear-error');
            }, function (error) {
                this.isBusy = false;
                this.isAjaxError = true;
                this.successMessages = [];
                this.errorMessages = [];
                this.$broadcast('clear-job');

                this.$broadcast('generate-error', error);
            });
        },

        checkLicense: function(e) {
            this.isBusy = true;
            this.errorMessages = [];

            this.$http({url: e.target.action, method: 'POST'}).then(function() {
                this.$broadcast('clear-error');
            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    }
});



/**
 * Client show page
 */
new Vue({
    el: '#clientShowPage',

    data: {
        isBusy: false,
        diskSpaceUsage: {
            color: null,
            value: null,
            max: null,
            percentage: null,
        },
        whopletUsage: {
            color: null,
            value: null,
            max: null,
            percentage: null,
        },
        websiteQuotaUsage: {
            color: null,
            value: null,
            max: null,
            percentage: null,
        },

        deleteClient: {
            url: null,
            redirectURL: null,
            username: null,
        },
        url: null,
    },

    computed: {
        diskSpaceUsageURL: function() {
            return $("meta[name='diskSpaceUsage']").attr('content');
        },

        whopletUsageURL: function() {
            return $("meta[name='whopletUsage']").attr('content');
        },
        websiteQuotaUsageURL: function() {
            return $("meta[name='websiteQuotaUsage']").attr('content');
        },
    },

    ready: function() {

        var _self = this;

        socket.on('applications#GetClientDiskSpaceUsage#' + credentialString, function(data) {
            _self.diskSpaceUsage = data;
        });

        socket.on('applications#GetClientWhopletUsage#' + credentialString, function(data) {
            _self.whopletUsage = data;
        });

        socket.on('applications#GetClientWebsiteQuotaUsage#' + credentialString, function(data) {
            _self.websiteQuotaUsage = data;
        });

        this.$http({url: this.diskSpaceUsageURL, method: 'GET'});
        this.$http({url: this.whopletUsageURL, method: 'GET'});
        this.$http({url: this.websiteQuotaUsageURL, method: 'GET'});
    },

    methods: {
        delete: function(url, redirectURL) {
            $('#deleteClientModal').modal({
                backdrop: 'static',
            });

            this.deleteClient.url = url;
            this.deleteClient.redirectURL = redirectURL;
        },

        deleteCommit: function() {

            var _self = this;

            this.isBusy = true;

            this.$http({url: this.deleteClient.url, method: 'POST', data: this.deleteClient}).then(function success(success) {
                toastr.success(success.data.message);
                this.$broadcast('clear-error');

                setTimeout(function() {
                    window.location.href = _self.deleteClient.redirectURL;
                }, 2000);
            }, function error(error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });

        },

        cancelDelete: function() {

            this.deleteClient.username = null;
        },


        resetQuota: function(url) {
            $('#resetQuotaModal').modal({
                backdrop: 'static',
            });

            this.url = url;
        },

        enableDisable: function(url) {
            $('#enableDisableAccountModal').modal({
                backdrop: 'static',
            });

            this.url = url;
        },

        resetAccount: function(url) {
            $('#resetExpiredAccountModal').modal({
                backdrop: 'static',
            });

            this.url = url;
        },
    },
});


/**
 * Edit Profile
 */
new Vue({
    el: '#editProfileVM',
});


/**
 * Whoplet Lst
 * @type {String}
 */
new Vue({
    el: '#whopletVM',

    data: {
        isBusy: true,
        whoplets: [],
        deleteMessage: null,
        removeDomain: {
            whoplet: null,
            whopletDomain: null,
        },
        trash: {
            whoplet: null,
        },
        whopletUsage: {
            color: null,
            value: null,
            max: null,
            percentage: null,
        },
        websiteQuotaUsage: {
            color: null,
            value: null,
            max: null,
            percentage: null,
        },
    },

    ready: function() {

        var _self = this;

        socket.on('applications#ListWhoplet#' + credentialString, function(whoplets) {
            _self.isBusy = false;

            _self.whoplets = whoplets;

            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#whopletList') ) {
                    initDataTable('#whopletList');
                }
                
                $('[data-toggle="tooltip"]').tooltip();
            });
        });

        socket.on('applications#RemoveDomainFromWhoplet#' + credentialString, function(message) {
            
            $('#removeWhopletDomainModal').modal('hide');

            if (message.success === true) {
                _self.fetchWhoplet();
                toastr.success(message.message);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });


        socket.on('applications#DeleteWhoplet#' + credentialString, function(message) {
            
            $('#deleteWhopletModal').modal('hide');

            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });


        socket.on('applications#GetClientWhopletUsage#' + credentialString, function(data) {
            _self.whopletUsage = data;
        });

        socket.on('applications#GetClientWebsiteQuotaUsage#' + credentialString, function(data) {
            _self.websiteQuotaUsage = data;
        });



        this.fetchWhoplet();

        this.$http({url: $('#whopletList').data('whopletusage'), method: 'GET'});
        this.$http({url: $('#whopletList').data('websitequotausage'), method: 'GET'});
    },

    methods: {
        fetchWhoplet: function() {
            this.$http({url: $('#whopletList').data('list'), method: 'GET'});
        },

        deleteDomain: function(deleteMessage, whoplet, domain) {
            this.deleteMessage = deleteMessage;
            this.removeDomain = {
                whoplet: whoplet,
                whopletDomain: domain,
            },

            $('#removeWhopletDomainModal').modal({
                backdrop: 'static',
            });
        },

        deleteDomainCommit: function() {

            this.isBusy = true;

            var data = this.removeDomain;

            this.$http({url: $('#whopletList').data('removedomain'), method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                $('#removeWhopletDomainModal').modal('hide');
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },

        deleteWhoplet: function(whoplet) {

            $('#deleteWhopletModal').modal({
                backdrop: 'static'
            });

            this.trash.whoplet = whoplet;
        },

        deleteWhopletCommit: function() {

            this.isBusy = true;

            var data = this.trash;

            this.$http({url: $('#whopletList').data('delete'), method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                $('#deleteWhopletModal').modal('hide');
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },

        manageWhoplet: function(whoplet, secure) {
            var data = '?whoplet=' + whoplet + '&secure=' + secure;

            return $('#whopletList').data('manage') + data;
        },
    },
});


/**
 * View Whoplet
 */
new Vue({
    el: '#showWhopletVM',

    data: {
        isBusy: true,
        whoplet: {},
        deleteMessage: null,
        trash: {
            whoplet: null,
        },
    },

    ready: function() {

        var _self = this;

        socket.on('applications#ShowWhoplet#' + credentialString, function(whoplet) {
            _self.whoplet = whoplet;

            _self.isBusy = false;

            Vue.nextTick(function() {
                $('[data-toggle="tooltip"]').tooltip();
            });
        });

        socket.on('applications#RemoveDomainFromWhoplet#' + credentialString, function(message) {
            
            $('#removeWhopletDomainModal').modal('hide');

            if (message.success === true) {
                _self.fetchWhoplet();
                toastr.success(message.message);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        socket.on('applications#DeleteWhoplet#' + credentialString, function(message) {
            
            $('#deleteWhopletModal').modal('hide');

            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    window.location.href = $("#showWhopletVM").data('redirect');
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        this.fetchWhoplet();
    },

    methods: {
        fetchWhoplet: function() {

            var data = {
                whoplet: gup('whoplet'),
            };

            this.$http({url: $('#showWhopletVM').data('fetch'), method: 'GET', data: data});
        }, 

        deleteDomain: function(deleteMessage, whoplet, domain) {
            this.deleteMessage = deleteMessage;
            this.removeDomain = {
                whoplet: whoplet,
                whopletDomain: domain,
            },

            $('#removeWhopletDomainModal').modal({
                backdrop: 'static',
            });
        },

        deleteDomainCommit: function() {

            this.isBusy = true;

            var data = this.removeDomain;

            this.$http({url: $('#showWhopletVM').data('removedomain'), method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                $('#removeWhopletDomainModal').modal('hide');
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        }, 


        deleteWhoplet: function(whoplet) {

            $('#deleteWhopletModal').modal({
                backdrop: 'static'
            });

            this.trash.whoplet = whoplet;
        },

        deleteWhopletCommit: function() {

            this.isBusy = true;

            var data = this.trash;

            this.$http({url: $('#showWhopletVM').data('delete'), method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                $('#deleteWhopletModal').modal('hide');
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
});


/**
 * Change WHoPlet PHP Version
 */
new Vue({
    el: '#changeWhopletPHPVM',

    data: {
        isBusy: true,
        phpVersions: [],
        changePHP: {
            whoplet: null,
            phpVersion: null,
        },
    },

    ready: function() {

        var _self = this;

        socket.on('applications#ShowWhoplet#' + credentialString, function(whoplet) {
            _self.changePHP = {
                whoplet: whoplet.whopletName,
                phpVersion: whoplet.phpVersion,
            };

            _self.isBusy = false;
        });

        socket.on('applications#GetPHPVersionList#' + credentialString, function(versions){
            _self.isBusy = false;

            _self.phpVersions = versions;
        });

        socket.on('applications#ChangeWhopletPHPVersion#' + credentialString, function(message) {
            
            $('#changeWhopletPHPVersionModal').modal('hide');

            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    window.location.href = $("#changeWhopletPHPVM").data('redirect');
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        this.fetchWhoplet();

        this.$http({url: $('#changeWhopletPHPVM').data('listphpversion'), method: 'GET'});
    },

    methods: {
        fetchWhoplet: function() {

            var data = {
                whoplet: gup('whoplet'),
            };

            this.$http({url: $('#changeWhopletPHPVM').data('fetch'), method: 'GET', data: data});
        }, 

        changePHPVersion: function(e) {

            this.isBusy = true;

            var data = this.changePHP;

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
});

/**
 * Add addtional whoplet domain
 */
new Vue({
    el: '#addWhopletDomainVM',

    data: {
        isBusy: false,
        addDomain: {
            domain: null,
            topLevelDomain: null,
            whoplet: null,
        },
    },

    ready: function() {

        var _self = this;


        socket.on('applications#ShowWhoplet#' + credentialString, function(whoplet) {
            _self.addDomain.whoplet = whoplet.whopletName;

            _self.isBusy = false;
        });

        socket.on('applications#AddDomainToWhoplet#' + credentialString, function(message) {
            
            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    window.location.href = $("#addWhopletDomainVM").data('redirect');
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        this.fetchWhoplet();
    },

    methods: {
        fetchWhoplet: function() {

            var data = {
                whoplet: gup('whoplet'),
            };

            this.$http({url: $('#addWhopletDomainVM').data('fetch'), method: 'GET', data: data});
        }, 


        addNewDomain: function(e) {

            this.isBusy = true;

            var data = this.addDomain;

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    }
});


/**
 * Change whoplet ceritificate
 */
new Vue({
    el: '#changeWhopletCertificateVM',

    data: {
        isBusy: false,
        changeCertificate: {
            whoplet: null,
            httpsCertificate: null,
        },
        certificates: [],
    },

    ready: function() {
        var _self = this;


        socket.on('applications#ShowWhoplet#' + credentialString, function(whoplet) {
            _self.changeCertificate = {
                whoplet: whoplet.whopletName,
                httpsCertificate: whoplet.secure.httpsCertificate,
            }

            _self.isBusy = false;
        });


        socket.on('applications#ChangeWhopletCertificate#' + credentialString, function(message) {

            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    window.location.href = $("#changeWhopletCertificateVM").data('redirect');
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        socket.on('universal#ListCertificate#' + credentialString, function(jsonArray){
            _self.isBusy = false;
            _self.certificates = jsonArray;

            if (jsonArray.length == 0) {
                _self.isEmpty = true;
            }
        });

        this.fetchWhoplet();
    },

    methods: {
        fetchWhoplet: function() {

            var data = {
                whoplet: gup('whoplet'),
            };

            this.$http({url: $('#changeWhopletCertificateVM').data('fetch'), method: 'GET', data: data});
        }, 


        changeWhopletCertificate: function(e) {

            this.isBusy = true;

            var data = this.changeCertificate;

            this.$http({url: e.target.action, method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },

        chooseCertificate: function() {
            var _self = this;
            this.isBusy = true;
            this.isEmpty = false;

            $('#modalSelectCertificate').modal({
                backdrop: 'static',
            }).on('shown.bs.modal', function (e) {
                _self.$http({url: $('#changeWhopletCertificateVM').data('listcertificate'), method: 'GET'}).then(function() {

                }, function (error) {
                    this.isBusy = false;
                    toastr.error('Unknown error!');
                    $('#modalSelectCertificate').modal('hide');
                });

                $(this).off('shown.bs.modal');
            });
        },

        setCertificate: function(cert) {
            this.changeCertificate.httpsCertificate = cert;
            $('#modalSelectCertificate').modal('hide');
        },
    },
}),


/**
 * Whoplet Log
 */
new Vue({
    el: '#whopletLogVM',

    data: {
        isBusy: true,
        viewing: {
            whoplet: null,
            logType: null,
        },

        log: [],
    },

    ready: function() {

        var _self = this;

        socket.on('applications#StreamWhopletLog#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.whopletName === _self.viewing.whopletName && message.logType === _self.viewing.logType) {
                _self.log.push(message.data);
            }
        });


        this.fetchLog();
    },

    methods: {
        fetchLog: function() {

            this.viewing = {
                whopletName: gup('whoplet'),
                logType: gup('logtype'),
            };

            this.$http({url: $('#whopletLogVM').data('stream'), method: 'GET'});
        },
    },
});


/**
 * Whoplet Statistic
 */
new Vue({
    el: '#whopletStatisticVM',

    data: {
        isBusy: false,
        chartConfig: {
            title: {
                display: false,
            },
            legend: {
                display: false,
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        beginAtZero: true,
                    }
                }]
            }

            //tooltipTemplate: "<%= datasetLabel %>: <%= value %>",
        },

        requestData: {},
        requestDataChart: {
            labels: [],
            datasets: [
                {
                    label: "Total Request",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [],
                },
            ]
        },

        errorData: {},
        errorDataChart: {
            labels: [],
            datasets: [
                {
                    label: "Total Error",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: "rgba(250,96,96,0.4)",
                    borderColor: "rgba(250,96,96,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(250,96,96,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(250,96,96,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [],
                },
            ]
        },
    },


    ready: function() {

        var _self = this;


        socket.on('applications#GetWhopletStatistic#' + credentialString, function(data) {

            _self.isBusy = false;

            if (data.success) {
                if (data.type == 'access') {
                    _self.requestData = data;

                    _self.requestDataChart.labels = data.days;
                    _self.requestDataChart.datasets[0].data = data.results;

                    var ctxRequest = $("#requestChart");
                    var requestChart = new Chart(ctxRequest, {
                        type: 'line',
                        data: _self.requestDataChart,
                        options: _self.chartConfig,
                    });
                } else if (data.type === 'error') {
                    _self.errorData = data;

                    _self.errorDataChart.labels = data.days;
                    _self.errorDataChart.datasets[0].data = data.results;

                    var ctxError = $("#errorChart");
                    var errorChart = new Chart(ctxError, {
                        type: 'line',
                        data: _self.errorDataChart,
                        options: _self.chartConfig,
                    });;
                }
            } else {
                toastr.error(data.message);
            }

        });


        this.fetchStatistic();
    },

    methods: {
        fetchStatistic: function() {
            this.isBusy = true;
            this.$http({url: $('#whopletStatisticVM').data('statistic'), method: 'GET'});
        },
    },
});



/**
 * Create Whoplet
 * @type {String}
 */
new Vue({
    el: '#addWhopletVM',

    data: {
        isBusy: true,
        isError: false,
        whoplet: {
            whopletName: null,
            domain: null,
            topLevelDomain: null,
            homeFolder: '/',
            openBasedir: '/',
            phpVersion: 'php70-whop',
            clickjackingProtection: true,
            xssProtection: true,
            preventMimeSniffing: true,
            httpsCertificate: null,
            allowHttp: false,
            hsts: false,
        },
        certificates: [],
        phpVersions: [],
    },

    ready: function() {
        var _self = this;

        socket.on('universal#ListCertificate#' + credentialString, function(jsonArray){
            _self.isBusy = false;
            _self.certificates = jsonArray;

            if (jsonArray.length == 0) {
                _self.isEmpty = true;
            }
        });

        socket.on('applications#GenerateWhoplet#' + credentialString, function(message){
            _self.$broadcast('clear-error');

            if (message.success == true) {
                toastr.success(message.message);
            } else {
                _self.isBusy = false;
                _self.$broadcast('clear-job');
                toastr.error(message.message);
            }
        });

        socket.on('applications#GetPHPVersionList#' + credentialString, function(versions){
            _self.phpVersions = versions;
        });


        this.$http({url: $('#formCreateWhoplet').data('listphpversion-url'), method: 'GET'});
    },

    events: {
        'choosed-homefolder': function(homefolder) {
            this.whoplet.openBasedir = homefolder;
        },
    },

    methods: {
        chooseCertificate: function() {
            var _self = this;
            this.isBusy = true;
            this.isEmpty = false;

            $('#modalSelectCertificate').modal({
                backdrop: 'static',
            }).on('shown.bs.modal', function (e) {
                _self.$http({url: $('#formCreateWhoplet').data('listcertificate-url'), method: 'GET'}).then(function() {

                }, function (error) {
                    this.isBusy = false;
                    toastr.error('Unknown error!');
                    $('#modalSelectCertificate').modal('hide');
                });

                $(this).off('shown.bs.modal');
            });
        },

        setCertificate: function(cert) {
            this.whoplet.httpsCertificate = cert;
            $('#modalSelectCertificate').modal('hide');
        },

        submit: function(e) {
            var url = e.target.action;

            this.isBusy = true;

            this.$http({url: url, method: 'POST', data: this.whoplet}).then(function() {
                this.$broadcast('clear-error');
            }, function (error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },

        cancel: function() {
            this.whoplet.httpsCertificate = null;
        }
    },
});



/**
 * List Certificate
 * @param  {[type]} ) {                   var _self [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#certificateVM',

    data: {
        certificates: [],
        certificate: null,
        certificateData: null,
        isBusy: true,
    },

    ready: function() {

        var _self = this;

        socket.on('universal#ListCertificate#' + credentialString, function(json) {
            _self.isBusy = false;

            _self.certificates = json;

            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#listCertificate') ) {
                    initDataTable('#listCertificate');
                }
            });
        });

        socket.on('universal#ViewCertificate#' + credentialString, function(certContent){
            _self.isBusy = false;
            _self.certificateData = certContent;
        });


        socket.on('universal#DeleteClientCertificate#' + credentialString, function(message){

            $('#deleteCertificateModal').modal('hide');

            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        //fetch
        this.fetchCertificate();
    },

    methods: {
        fetchCertificate: function() {
            var url =  $('#listCertificate').data('list');

            this.$http({url: url, method: 'GET'});
        },

        view: function(cert) {
            this.isBusy = true;
            this.certificate = cert;

            var data = {
                cert: cert.slice(0, -4),
            }

            this.$http({url: $("#listCertificate").data('view'), method: 'GET', data: data}).then(function(s) {
                $('#viewCertificateModal').modal({
                    backdrop: 'static',
                });
            }, function() {
                toastr.error('Unable to view your certificate!');
                this.isBusy = false;
            });
        },

        deleteConfirmation: function(certName) {
            
            $('#deleteCertificateModal').modal({
                backdrop: 'static',
            })

            this.certificate = certName;
        },

        deleteCertificate: function() {
            this.isBusy = true;

            var data = {
                CertName: this.certificate.split('.crt')[0],
            };

            this.$http({url: $('#listCertificate').data('delete'), method: 'POST', data: data}).then(function (success) {
                this.$broadcast('clear-error');
            }, function (error) {
                $('#deleteCertificateModal').modal('hide');
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
}),


/**
 * Add Certificate
 * @param  {[type]} ) {                   var _self [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#addCertificateVM',

    data: {
        certificates: null,
        certificate: null,
        certificateData: null,
        isBusy: false,
        certificateName: null,
        publicCertificate: null,
        privateKey: null,
    },

    ready: function() {

        var _self = this;

        socket.on('applications#AddClientCertificate#' + credentialString, function(json) {
            _self.isBusy = false;

            if (json.flag == false) {
                toastr.error(json.message);
            } else {
                toastr.success(json.message, 1.5);

                setTimeout(function() {
                    window.location.href = $("#formAddCertificate").data('redirect');
                }, 1500);
            }
        });
    },

    methods: {
        addCertificate: function(e) {

            var _self = this;

            this.isBusy = true;

            var data = {
                certificateName: this.certificateName,
                publicCertificate: this.publicCertificate,
                privateKey: this.privateKey,
            };


            this.$http({url: e.target.action, method: 'POST', data: data}).then(function(s) {
                this.$broadcast('clear-error');
            }, function(error) {
                this.isBusy = false;
                
                this.$broadcast('generate-error', error);
            });
        },
    },
});




/**
 * MariaDB
 * @param  {[type]} )        {               }   [description]
 * @param  {[type]} methods: {               }} [description]
 * @return {[type]}          [description]
 */
new Vue({
    el: "#mariaDB",

    data: {
        password: null,
        verifyPassword: null,
        url: null,
        deleteMessage: null,
        postMessage: null,
        databases: [],
        databaseUsers: [],
        isBusy: true,
    },

    ready: function() {

        /**
         * Fetch Database
         */
        this.$http({url: $("#databaseList").data('url'), method: 'GET'}).then(function(success) {

            this.isBusy = false;
            this.databases = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#databaseList') ) {
                    initDataTable('#databaseList');
                }
                
                $('[data-toggle="tooltip"]').tooltip();
            });
        });


        /**
         * Fetch Database User
         */
        this.$http({url: $("#databaseUserList").data('url'), method: 'GET'}).then(function(success) {

            this.isBusy = false;
            this.databaseUsers = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#databaseUserList') ) {
                    initDataTable('#databaseUserList');
                }
            });
        });
    },

    methods: {
        changePassword: function(url) {
            this.url = url;
            this.password = null;
            this.verifyPassword = null;
        },

        delete: function(url, deleteMessage) {
            this.url = url;
            this.postMessage = null;
            this.deleteMessage = deleteMessage;
        },

        revoke: function(url, message) {
            this.url = url;
            this.deleteMessage = null;
            this.postMessage = message;
        }
    }
});

/**
 * Domain Name
 */
new Vue({
    el: '#domainVM',

    data: {
        url: null,
        deleteMessage: null,
        domains: [],
        isBusy: true,
    },

    ready: function() {

        /**
         * Fetch Domains
         */
        this.$http({url: $("#domainList").data('url'), method: 'GET'}).then(function(success) {

            this.isBusy = false;
            this.domains = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#domainList') ) {
                    initDataTable('#domainList');
                }
            });
        });
    },

    methods: {
        delete: function(url, message) {
            this.url = url;
            this.deleteMessage = message;
        }
    },
});

/**
 * Record
 * @param  {[type]} ) {                   var _self [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#recordVM',

    data: {
        type:null,
        contentPlaceholder: null,
        url: null,
        deleteMessage: null,
        IPv4: [],
        IPv6: [],
        isBusy: true,
        dnssecData: null,
    },

    ready: function() {

        var _self = this;

        /**
         * Fetch IP
         */
        this.$http({url: $("#recordVM").data('getip'), method: 'GET'}).then(function(success) {
            this.isBusy = false;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#recordList') ) {
                    initDataTable('#recordList');
                }
            });
        }, function(error) {
            this.isBusy = false;
        });


        /**
         * Fetch DNSSEC
         */
        this.$http({url: $('#recordVM').data('dnssec'), method: 'GET'}).then(function(success) {});


        socket.on('applications#GetServerIP#' + credentialString, function(net) {
            $.each(net, function(index, netint) {
                $.each(netint, function(index, obj) {
                    if (obj.internal === false && obj.family == "IPv4") {
                        _self.IPv4.push(obj.address);
                    } if (obj.internal === false && obj.family == "IPv6") {
                        _self.IPv6.push(obj.address);
                    }
                });
            });
        });


        socket.on('applications#GetDNSSEC#' + credentialString, function(data) {
            _self.dnssecData = data;
        });
    },

    methods: {
        updatePlaceholder: function() {
            if (this.type === 'A') {
                this.contentPlaceholder = 'IPv4 Address';
            } else if (this.type === 'AAA') {
                this.contentPlaceholder = 'IPv6 Address';
            } else if (this.type === 'CNAME') {
                this.contentPlaceholder = 'Domain Name';
            } else if (this.type === 'MX') {
                this.contentPlaceholder = 'Mail Exchange Server';
            } else if (this.type === 'SPF') {
                this.contentPlaceholder = 'SPF Policy';
            } else if (this.type === 'TXT') {
                this.contentPlaceholder = 'TXT Content';
            }
        },

        delete: function(url, message) {
            this.url = url;
            this.deleteMessage = message;
        },
    }
});

/**
 * Email
 * @param  {[type]} ) {                           this.$http({url: $("#emailList").data('url'), method: 'GET'}).then(function(success) {            this.isBusy [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#emailVM',

    data: {
        url: null,
        deleteMessage: null,
        emails: [],
        settings: {},
        isBusy: true,
    },

    ready: function() {
        /**
         * Fetch Emails
         */
        this.$http({url: $("#emailList").data('url'), method: 'GET'}).then(function(success) {
            this.isBusy = false;
            this.emails = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#emailList') ) {
                    initDataTable('#emailList');
                }
                
                $('[data-toggle="tooltip"]').tooltip();
            });
        });
    },

    methods: {
        delete: function(url, message) {
            this.url = url;
            this.deleteMessage = message;
        },

        showSetting: function(settings) {
            this.settings = settings;

            $('#viewEmailSettingModal').modal({
                backdrop: 'static',
            });
        }
    },
});

/**
 * Email Forwarding
 * @param  {[type]} ) {                           this.$http({url: $("#forwardingList").data('url'), method: 'GET'}).then(function(success) {            this.isBusy [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#emailForwardingVM',

    data: {
        forwardings: [],
        deleteMessage: null,
        url: null,
        isBusy: true,
    },

    ready: function() {
        /**
         * Fetch Email forwarding
         */
        this.$http({url: $("#forwardingList").data('url'), method: 'GET'}).then(function(success) {
            this.isBusy = false;
            this.forwardings = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#forwardingList') ) {
                    initDataTable('#forwardingList');
                }

                $('[data-toggle="tooltip"]').tooltip();
            });
        });
    },

    methods: {
        delete: function(url, message) {
            this.url = url;
            this.deleteMessage = message;
        },
    },
});

/**
 * Webmail Password
 * @param  {[type]} )  {                       this.$broadcast('generate-password', 32);        } [description]
 * @param  {[type]} } [description]
 * @param  {[type]} } [description]
 * @return {[type]}    [description]
 */
new Vue({
    el: '#changeWebmailPasswordVM',
});

/**
 * Webmail Forwarding
 * @param  {[type]} ) {                           this.$http({url: $("#forwardingList").data('url'), method: 'GET'}).then(function(success) {            this.isBusy [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#webmailForwardingVM',

    data: {
        forwardings: [],
        deleteMessage: null,
        url: null,
        isBusy: true,
    },

    ready: function() {
        /**
         * Fetch Email forwarding
         */
        this.$http({url: $("#forwardingList").data('url'), method: 'GET'}).then(function(success) {
            this.isBusy = false;
            this.forwardings = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#forwardingList') ) {
                    initDataTable('#forwardingList');
                }
                
                $('[data-toggle="tooltip"]').tooltip();
            });
        });
    },

    methods: {
        delete: function(url, message) {
            this.url = url;
            this.deleteMessage = message;
        },
    },
});

/**
 * FTP
 * @param  {[type]} ) {                           this.$http({url: $("#ftpAccountList").data('url'), method: 'GET'}).then(function(success) {            this.isBusy [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#ftpVM',

    data: {
        homeFolder: '/',
        isBusy: true,
        isError: false,
        accounts: [],
        url: null,
        deleteMessage: null,
    },

    ready: function() {
        /**
         * Fetch Account
         */
        this.$http({url: $("#ftpAccountList").data('url'), method: 'GET'}).then(function(success) {

            this.isBusy = false;
            this.accounts = success.data;
            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#ftpAccountList') ) {
                    initDataTable('#ftpAccountList');
                }
                
                $('[data-toggle="tooltip"]').tooltip();
            });
        });
    },

    methods: {
        delete: function(url, message) {
            this.url = url;
            this.deleteMessage = message;
        },
    },
});

/**
 * File Manager
 * @type {String}
 */
new Vue({
    el: '#fileManagerVM',

    data: {
        isBusy: true,
        isError: false,
        structure: {
            parent: '/',
            currentLocation: '/',
            message: null,
            content: null,  
        },
        selectedItems: [],
        disableChangePermission: true,
        disableRename: true,
        disableEdit: true,
        disableDelete: true,
        errorMessages: [],
        create: {
            type: null,
            name: null,
        },
        rename: {
            oldName: null,
            newName: null,
        },
        deletion: {
            names: [],
        },
        permission: {
            names: [],
            user: {
                read: false,
                write: false,
                execute: false,
            },
            group: {
                read: false,
                write: false,
                execute: false,
            },
            world: {
                read: false,
                write: false,
                execute: false,
            },
        },
    },

    computed: {
        listURL: function() {
            return $('#fmTable').data('list');
        },

        createURL: function() {
            return $('#fmTable').data('newfiledirectory');
        },

        renameURL: function() {
            return $('#fmTable').data('rename');
        },

        deleteURL: function() {
            return $('#fmTable').data('delete');
        },

        editURL: function() {
            return $('#fmTable').data('edit');
        },

        permissionURL: function() {
            return $('#fmTable').data('permission');
        },

        permissionSum: function() {
            var userPerm = 0;
            var groupPerm = 0;
            var worldPerm = 0;
            var permArray = [];

            userPerm +=  this.permission.user.read ? 4 : 0;
            userPerm +=  this.permission.user.write ? 2 : 0;
            userPerm +=  this.permission.user.execute ? 1 : 0;

            groupPerm +=  this.permission.group.read ? 4 : 0;
            groupPerm +=  this.permission.group.write ? 2 : 0;
            groupPerm +=  this.permission.group.execute ? 1 : 0;

            worldPerm +=  this.permission.world.read ? 4 : 0;
            worldPerm +=  this.permission.world.write ? 2 : 0;
            worldPerm +=  this.permission.world.execute ? 1 : 0;

            permArray.push(userPerm);
            permArray.push(groupPerm);
            permArray.push(worldPerm);

            return permArray.join('');
        },
    },


    ready: function() {

        var _self = this;
        /**
         * List
         */
        this.fetchList(this.currentLocation);


        socket.on('applications#ListFileDirectory#' + credentialString, function(message) {

            _self.isBusy = false;

            if (message.success == false) {
                _self.isError = true;
            } else {
                _self.isError = false;
            }

            _self.structure = {
                parent: message.parent,
                currentLocation: message.currentLocation,
                message: message.message,
                content: message.content,
            };
        });

        socket.on('applications#NewFileDirectory#' + credentialString, function(message) {

            _self.isBusy = false;

            if (message.success == true) {
                toastr.success(message.message);
                _self.closeModal();
                _self.create.name = null;
                _self.fetchList(_self.structure.currentLocation);
            } else {
                toastr.error(message.message);
            }
        });


        socket.on('applications#UploadFile#' + credentialString, function(message) {
            toastr.error(message.message);
        });


        socket.on('applications#RenameFileDirectory#' + credentialString, function(message) {

            _self.isBusy = false;

            if (message.success == true) {
                toastr.success(message.message);
                _self.closeModal();
                _self.fetchList(_self.structure.currentLocation);
            } else {
                toastr.error(message.message);
            }
        });

        socket.on('applications#DeleteFileDirectory#' + credentialString, function(message) {

            _self.isBusy = false;

            if (message.success == true) {
                toastr.success(message.message);
                _self.closeModal();
                _self.fetchList(_self.structure.currentLocation);
            } else {
                toastr.error(message.message);
            }
        });


        socket.on('applications#ChangeFileDirectoryPermission#' + credentialString, function(message) {

            _self.isBusy = false;

            if (message.success == true) {
                toastr.success(message.message);
                _self.closeModal();
                _self.resetPermission();
                _self.fetchList(_self.structure.currentLocation);
            } else {
                toastr.error(message.message);
            }
        });


        // Multiselect
        $('table#fmTable').multiSelect({
            actcls: 'info',
            selector: 'tbody tr.directory',
            except: ['tbody'],
            callback: function (items) {
                if (items.length == 0) {
                    _self.disableChangePermission = true;
                    _self.disableRename = true;
                    _self.disableEdit = true;
                    _self.disableDelete = true;
                    _self.selectedItems = [];
                } else {
                    _self.selectedItems = [];
                    _self.disableChangePermission = false;
                    _self.disableEdit = true;
                    _self.disableDelete = false;
                    _self.disableRename = true;
                }

                if (items.length == 1 && items[0].children[3].innerHTML !== "inode/directory" && (items[0].children[3].innerHTML.indexOf('text/') !== -1 || items[0].children[3].innerHTML.indexOf('inode/x-empty') !== -1) ) {
                    _self.disableEdit = false;
                }

                if (items.length == 1) {
                    _self.disableRename = false;
                    _self.rename = {
                        oldName: items[0].children[1].innerHTML,
                        newName: items[0].children[1].innerHTML,
                    };
                    // $scope.newName = items[0].children[1].innerHTML;
                } else {
                    _self.disableRename = true;
                }

                $.each(items, function(key, value) {
                    _self.selectedItems.push(value.children[1].innerHTML);
                });
            },
        });


        // file upload
        var dzUpload = new Dropzone("#dzUpload", { 
            url: $('#fmTable').data('upload'),
            sending: function(file, xhr, formData) {
                formData.append("_token", $("meta[name='X-CSRF-TOKEN']").attr('content'));
                formData.append("folder", _self.structure.currentLocation);
            }
        });


        dzUpload.on("complete", function(file) {
            setTimeout(function() {
                dzUpload.removeFile(file);
            }, 2000);
        });
    },

    methods: {

        fetchList: function(folder) {

            var _self = this;

            this.isBusy = true;

            var data = {
                folder: folder,
            };

            this.$http({url: this.listURL, method: 'GET', data: data}).then(function(success) {
            }, function(error) {
                this.isBusy = false;
            });
        },


        walk: function(item) {
            if (item.type == 'inode/directory') {
                this.fetchList(item.location);
            }
        },

        goToParent: function() {
            this.fetchList(this.structure.parent);
        },

        createFileDirectory: function(type) {

            this.create.type = type;

            if (type == 'file') {
                $('#createFileModal').modal({
                    backdrop: 'static',
                });
            } else if (type == 'folder') {
                $('#createFolderModal').modal({
                    backdrop: 'static',
                });
            }
        },


        createCommit: function() {

            this.isBusy = true;
            this.errorMessages = [];

            var _self = this;

            var data = this.create;
            data.folder = this.structure.currentLocation;

            this.$http({url: this.createURL, method: 'POST', data: data}).then(function(success) {
                
            }, function(error) {
                this.isBusy = false;

                if (error.status == 422) {

                    _self.closeModal();

                    $.each(error.data, function(key, value) {
                        $.each(value, function(key, error) {
                            _self.errorMessages.push(error);
                        });
                    });
                }
            });

        },


        renameFile: function() {
            $('#renameFileFolderModal').modal({
                backdrop: 'static',
            });
        },


        renameCommit: function() {
            this.isBusy = true;
            this.errorMessages = [];

            var _self = this;

            var data = this.rename;
            data.folder = this.structure.currentLocation;

            this.$http({url: this.renameURL, method: 'POST', data: data}).then(function(success) {
                
            }, function(error) {
                this.isBusy = false;

                if (error.status == 422) {

                    _self.closeModal();

                    $.each(error.data, function(key, value) {
                        $.each(value, function(key, error) {
                            _self.errorMessages.push(error);
                        });
                    });
                }
            });
        },


        deleteFile: function() {
            $('#deleteFileFolderModal').modal({
                backdrop: 'static',
            });

            this.deletion.names = this.selectedItems;
        },


        deleteCommit: function() {
            this.isBusy = true;
            this.errorMessages = [];

            var _self = this;

            var data = this.deletion;
            data.folder = this.structure.currentLocation;

            this.$http({url: this.deleteURL, method: 'POST', data: data}).then(function(success) {
            }, function(error) {
                this.isBusy = false;

                if (error.status == 422) {

                    _self.closeModal();

                    $.each(error.data, function(key, value) {
                        $.each(value, function(key, error) {
                            _self.errorMessages.push(error);
                        });
                    });
                }
            });
        },


        changePermission: function() {
            $('#changePermissionModal').modal({
                backdrop: 'static',
            });

            this.permission.names = this.selectedItems;
        },


        permissionCommit: function() {
            this.isBusy = true;
            this.errorMessages = [];

            var _self = this;

            var data = {
                permission: this.permission,
                folder: this.structure.currentLocation,
            };

            this.$http({url: this.permissionURL, method: 'POST', data: data}).then(function(success) {
            }, function(error) {
                this.isBusy = false;

                if (error.status == 422) {

                    _self.closeModal();

                    $.each(error.data, function(key, value) {
                        $.each(value, function(key, error) {
                            _self.errorMessages.push(error);
                        });
                    });
                }
            });
        },


        resetPermission: function() {
            this.permission =  {
                names: [],
                user: {
                    read: false,
                    write: false,
                    execute: false,
                },
                group: {
                    read: false,
                    write: false,
                    execute: false,
                },
                world: {
                    read: false,
                    write: false,
                    execute: false,
                },
            };
        },


        closeModal: function() {
            $('#createFileModal').modal('hide');
            $('#createFolderModal').modal('hide');
            $('#renameFileFolderModal').modal('hide');
            $('#deleteFileFolderModal').modal('hide');
            $('#changePermissionModal').modal('hide');
        },


        edit: function() {
            var folder= this.structure.currentLocation;
            var filename = this.selectedItems[0];

            var data = '?folder=' + folder+ '&filename=' + filename;

            window.open(this.editURL + data,'_blank');
        },
    },
});


/**
 * File Manager Edit
 * @param  {[type]} )            {                         return $('#fmEditVM').data('editor');                                                   } [description]
 * @param  {[type]} saveURL:     function()    {                  return                         $('#fmEditVM').data('save');                         }            [description]
 * @param  {[type]} getThemeURL: function()    {                  return                         $('#fmEditVM').data('gettheme' [description]
 * @return {[type]}              [description]
 */
new Vue({
    el: '#fmEditVM',

    data: {
        isBusy: true,
        mode: null,
        oldMode: null,
        theme: null,
        oldTheme: null,
        editor: null,
    },

    computed: {
        editorURL: function() {
            return $('#fmEditVM').data('editor');
        },

        saveURL: function() {
            return $('#fmEditVM').data('save');
        },

        getThemeURL: function() {
            return $('#fmEditVM').data('gettheme');
        },

        saveThemeURL: function() {
            return $('#fmEditVM').data('savetheme');
        },

        editingFile: function() {
            return gup('folder') + gup('filename');
        },
    },


    ready: function() {
        var _self = this;


        socket.on('applications#EditFile#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == true) {

                _self.editor = initEditor(message.data.join('\n'), 'editor', _self.mode, _self.theme, 0);
                toastr.success(message.message);

                _self.editor.commands.addCommand({
                    name: "Save",
                    exec: function() {
                        _self.save();
                    },
                    bindKey: {mac: "cmd-s", win: "ctrl-s"}
                });
            } else {
                toastr.error(message.message);
            }
        });


        socket.on('applications#SaveEditFile#' + credentialString, function(message) {

            _self.isBusy = false;

            if (message.success == true) {
                toastr.success(message.message);
            } else {
                toastr.error(message.message);
            }
        });




        this.mode = modelist.getModeForPath(gup('filename')).mode;
        this.oldMode = this.mode;

        // get theme
        this.$http({url: this.getThemeURL, method: 'GET'}).then(function(success) {
            this.theme = success.data.theme;
            this.oldTheme = success.data.theme;

            // fetch data
            this.fetchData();
        }, function(error) {
        });


        $(document).bind('keydown', 'ctrl+s', function(e) {
            e.preventDefault();
            _self.save();
        });
    },

    methods: {

        fetchData: function() {

            this.isBusy = true;

            var data = {
                folder: gup('folder'),
                filename: gup('filename'),
            };

            this.$http({url: this.editorURL, method: 'GET', data: data}).then(function(success) {
            }, function(error) {
                this.isBusy = false;
            });
        },


        changeMode: function() {
            $('#editorModeModal').modal({
                backdrop: 'static',
            });
        },

        applyMode: function() {
            this.editor.getSession().setMode(this.mode);
        },

        cancelChangeMode: function() {
            this.mode = this.oldMode;
            this.editor.getSession().setMode(this.mode);
        },

        changeModeCommit: function() {
            this.applyMode();
            this.oldMode = this.mode;
            $('#editorModeModal').modal('hide');
        },


        changeTheme: function() {
            $('#editorThemeModal').modal({
                backdrop: 'static',
            });
        },

        applyTheme: function() {
            this.editor.setTheme(this.theme);
        },


        cancelChangeTheme: function() {
            this.theme = this.oldTheme;
            this.editor.getSession().setMode(this.theme);
        },

        changeThemeCommit: function() {
            this.applyTheme();
            this.oldTheme = this.theme;
            $('#editorThemeModal').modal('hide');

            var data = {
                editorTheme: this.theme,
            };

            this.$http({url: this.saveThemeURL, method: 'POST', data: data}).then(function(success) {
            }, function(error) {
            });
        },


        save: function() {
            this.isBusy = true;

            var data = {
                folder: gup('folder'),
                filename: gup('filename'),
                content: this.editor.getSession().getValue(),
            };

            this.$http({url: this.saveURL, method: 'POST', data: data}).then(function(success) {
            }, function(error) {
                this.isBusy = false;
            });
        },
    },
});

/**
 * Crontab
 * @param  {[type]} ) {                   var _self [description]
 * @return {[type]}   [description]
 */
new Vue({
    el: '#cronVM',

    data: {
        isBusy: true,
        jobs: [],
        deleteCron: {
            cronId: null,
        },
    },

    ready: function() {

        var _self = this;

        socket.on('applications#ListClientCronJobs#' + credentialString, function(message) {
            _self.isBusy = false;

            if (message.success == true) {
                _self.jobs = message.jobs;
            } else {
                toastr.error(message.message);
            }

            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#jobTableList') ) {
                    initDataTable('#jobTableList');
                }
            });
        });


        socket.on('applications#DeleteCronJob#' + credentialString, function(message) {
            
            $('#deleteCronModal').modal('hide');

            if (message.success === true) {
                toastr.success(message.message);

                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });


        this.fetchList();
    },

    methods: {
        fetchList: function() {
            this.isBusy = true;

            this.$http({url: $('#cronVM').data('list'), method: 'GET'});
        },

        deleteJob: function(id) {

            this.deleteCron.cronId = id;

            $('#deleteCronModal').modal({
                backdrop: 'static',
            })
        },

        deleteJobCommit: function() {

            this.isBusy = true;

            this.$http({url: $('#cronVM').data('delete'), method: 'POST', data: this.deleteCron}).then(function() {
                this.$broadcast('clear-error');
            }, function error(error) {
                this.isBusy = false;

                $('#deleteCronModal').modal('hide');
                this.$broadcast('generate-error', error);
            });
        },
    },
});


/**
 * Crontab Create
 */
new Vue({
    el: '#addCronVM',

    data: {
        cron: {
            minute: null,
            hour: null,
            day: null,
            month: null,
            weekDay: null,
            command: null,
        },
        isBusy: false,
        availablePath: [],
    },

    ready: function() {
        var _self = this;

        socket.on('applications#RegisterCron#' + credentialString, function(message) {
            _self.isBusy = false;
            if (message.success == true) {
                toastr.success(message.message);

                setTimeout(function() {
                    window.location.href = $("#addCronVM").data('redirect');
                }, 2000);
            } else {
                toastr.error(message.message);
            }
        });

        socket.on('applications#CronCommandHelper#' + credentialString, function(availablePath) {
            _self.availablePath = availablePath;
        });

        this.fetchCommandHelper();
    },

    methods: {
        submit: function(e) {

            this.isBusy = true;


            this.$http({url: e.target.action, method: 'POST', data: this.cron}).then(function success() {
                this.$broadcast('clear-error');
            }, function error(error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },

        fetchCommandHelper: function() {
            this.$http({url: $('#addCronVM').data('commandhelper'), method: 'GET'});
        },
    },
});


/**
 * Supervisor
 */
new Vue({
    el: '#supervisorVM',

    data: {
        isBusy: true,
        jobs: [],
        obj: {
            jobName: null,
        }
    },

    ready: function() {

        var _self = this;

        socket.on('applications#ListClientSupervisor#' + credentialString, function(jobs) {
            _self.isBusy = false;

            _self.jobs = jobs;

            Vue.nextTick(function() {
                if ( ! $.fn.dataTable.isDataTable('#listJob') ) {
                    initDataTable('#listJob');
                }
            });
        });

        socket.on('applications#ReloadClientSupervisor#' + credentialString, function(message) {

            if (message.success == true) {
                _self.fetchJobs();
                toastr.success(message.message);

                $('#reloadSupervisorModal').modal('hide');
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        socket.on('applications#DeleteClientSupervisor#' + credentialString, function(message) {

            if (message.success == true) {
                toastr.success(message.message);

                $('#deleteSupervisorModal').modal('hide');

                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });

        this.fetchJobs();
    },

    methods: {
        fetchJobs: function() {
            this.isBusy = true;

            this.$http({url: $('#listJob').data('list')});
        },

        reloadSupervisor: function(baseName) {
            this.obj.jobName = baseName;

            $('#reloadSupervisorModal').modal({
                backdrop: 'static',
            });
        },

        reloadSupervisorCommit: function() {
            this.isBusy = true;

            this.$http({url: $('#listJob').data('reload'), method: 'POST', data: this.obj});
        },

        deleteSupervisor: function(baseName) {
            this.obj.jobName = baseName;

            $('#deleteSupervisorModal').modal({
                backdrop: 'static',
            });
        },


        deleteSupervisorCommit: function() {
            this.isBusy = true;

            this.$http({url: $('#listJob').data('delete'), method: 'POST', data: this.obj});
        },
    },
});


/**
 * Create Supervisor Job
 */
new Vue({
    el: '#supervisorCreateVM',

    data: {
        isBusy: true,
        job: {
            name: null,
            vendor: null,
            command: null,
        },
        vendor: [],
    },

    ready: function() {

        var _self = this;

        socket.on('applications#SupervisorVendorList#' + credentialString, function(vendor) {
            _self.isBusy = false;

            _self.vendor = vendor;
        });


        socket.on('applications#RegisterSupervisor#' + credentialString, function(message) {
            if (message.success == true) {
                toastr.success(message.message);

                setTimeout(function() {
                    window.location.href = $("#supervisorCreateVM").data('redirect');
                }, 2000);
            } else {
                _self.isBusy = false;
                toastr.error(message.message);
            }
        });


        this.$http({url: $('#supervisorCreateVM').data('vendor-list'), method: 'GET'});
    },

    methods: {
        submit: function(e) {

            this.isBusy = true;


            var url = e.target.action;

            this.$http({url: url, method: 'POST', data: this.job}).then(function success() {
                this.$broadcast('clear-error');
            }, function error(error) {
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },
    },
});


/**
 * License Uploader
 */
new Vue({
    el: '#licenseUploaderVM',

    data: {
        isBusy: false,
        validating: false,
        licenseAlert: null,
        licenseSuccess: null,
    },

    ready: function() {

        var _self = this;

        $("#licenseInput").fileinput();


        socket.on('universal#CheckLicense#' + credentialString, function(license) {
            _self.validating = false;
            
            if (license.alert !== null) {
                _self.licenseAlert = license.licenseAlert;
            } else {
                _self.licenseSuccess = 'Succesfully updated your license';
            }
        });
    },

    methods: {

        submit: function(e) {

            var url = e.target.action;

            this.isBusy = true;
            this.validating = false;
            this.licenseAlert = null;
            this.licenseSuccess = null;

            var form = new FormData();

            form.append('license', $('input[type=file]')[0].files[0]);


            this.$http({url: url, method: 'POST', data: form}).then(function success(success) {
                this.isBusy = false;
                this.validating = true;
            }, function error(error) {
                this.isBusy = false;
                this.validating = false;
            });            
        },
    },
});