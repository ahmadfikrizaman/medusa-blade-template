'use strict';

new Vue({
    el: '#panelCertificate',

    data: {
        certificates: null,
        certificate: null,
        certificateData: null,
        isBusy: true,
        usingCert: null,
        certificateName: null,
        publicCertificate: null,
        privateKey: null,
    },

    ready: function() {

        var _self = this;


        /**
         * Fetch Data
         */
        this.fetchCertificate();



        socket.on('ListUniversalCertificate', function(jsonArray){
            _self.isBusy = false;
            _self.certificates = jsonArray;

            Vue.nextTick(function() {
                initDataTable('');
            });
        });


        socket.on('GetPanelCertificate', function(using) {
            _self.usingCert = using;
        });


        socket.on('universal#ViewCertificate#' + credentialString, function(certContent){
            _self.isBusy = false;
            _self.certificateData = certContent;
        });

        socket.on('ChangePanelCertificate', function(bool) {
            _self.isBusy = false;

            if (bool) {
                toastr.success('Succesfully save your config!');
            } else {
                toastr.error('Unknown error!');
            }
        });

        socket.on('universal#DeleteUniversalCertificate#' + credentialString, function(json) {

            if (json.flag == true) {
                toastr.success(json.message);

                setTimeout(function() {
                    location.reload();
                }, 1500);
            } else {
                toastr.error(json.message);
            }

            $("#deleteCertificateModal").modal('hide');
        });

        socket.on('universal#AddCertificate#' + credentialString, function(json) {

            if (json.flag == false) {
                toastr.error(json.message);
            } else {
                toastr.success(json.message);

                setTimeout(function() {
                    location.reload();
                }, 1500);
            }
        });
    },

    methods: {
        view: function(cert) {
            this.isBusy = true;
            this.certificate = cert;

            var data = {
                cert: cert.slice(0, -4),
            }

            this.$http({url: $("#panelCertificate").data('view-url'), method: 'GET', data: data}).then(function(s) {
                $('#viewCertificateModal').modal({
                    backdrop: 'static',
                });
            }, function() {
                toastr.error('Unable to view your certificate!');
                this.isBusy = false;
            });
        },

        close: function() {
            this.certificate = null;
            this.certificateData = null;
        },

        changePanelCertificate: function(cert){

            var data = {
                cert: cert.slice(0, -4),
                oldCert: this.usingCert.slice(0, -4),
            };

            this.usingCert = cert;

            this.isBusy = true;


            this.$http({url: $("#panelCertificate").data('update-url'), method: 'POST', data: data}).then(function(s) {

            }, function() {
                toastr.error('Please provide all required parameter/s!');
                this.isBusy = false;
            });
        },

        deleteConfirmation: function(cert) {
            this.certificate = cert;

            $("#deleteCertificateModal").modal({
                backdrop: 'static',
            });
        },

        deleteCertificate: function() {
            this.isBusy = true;
            credential.CertName = this.certificate .slice(0, -4);

            var data = {
                CertName: this.certificate .slice(0, -4),
            };
            

            this.$http({url: $("#panelCertificate").data('destroy-url'), method: 'POST', data: data}).then(function(s) {

            }, function() {
                toastr.error('Please provide certificate to delete!');
                this.isBusy = false;
            });
        },

        addCertificate: function(e) {

            var _self = this;

            this.isBusy = true;

            var data = {
                certificateName: this.certificateName,
                publicCertificate: this.publicCertificate,
                privateKey: this.privateKey,
            };


            this.$http({url: e.target.action, method: 'POST', data: data}).then(function(s) {

            }, function(error) {
                this.isBusy = false;
                
                this.$broadcast('generate-error', error);
            });
        },

        fetchCertificate: function() {
            this.$http({url: $("#panelCertificate").data('list-url'), method: 'GET'});
        }
    }
});



new Vue({
    el: '#panelSetting',

    data: {
        phpVersion: null,
        phpVersions: [],
        isBusy: true,
    },

    ready: function() {

        var _self = this;

        /**
         * Fetch Data
         */
        this.fetchData();



        socket.on('GetPanelPHPVersion', function(config) {
            _self.isBusy = false;
            _self.phpVersion = config.Using;
            _self.phpVersions = config.AllVersion;
        });


        socket.on('universal#ChangePanelPHPVersion#' + credentialString, function(message) {
            _self.isBusy = false;
            
            if (message.flag == true) {
                toastr.success(message.message);
            } else {
                toastr.error(message.message);
            }
        });
    },

    methods: {
        changePHPVersion: function(e) {

            this.isBusy = true;
            credential.PHPVersion = this.phpVersion;

            var data = {
                phpVersion: this.phpVersion,
            };
            
            this.$http({url: e.target.action, method: 'POST', data: data}).then(function(success) {

            }, function(error){
                this.isBusy = false;
                this.$broadcast('generate-error', error);
            });
        },

        fetchData: function() {
            this.$http({url: $("#panelSetting").data('list-url'), method: 'GET'});
        }
    }
});