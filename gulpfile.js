require('es6-promise').polyfill();
var elixir = require('laravel-elixir');


require('./elixir-extensions');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    
    mix.less([
        'medusa/app.less',
    ], 'public/themes/medusa/css/app2.css')

    .styles([
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/loaders.css/loaders.min.css',
        'bower_components/toastr/toastr.css',
        'bower_components/datatables/media/css/dataTables.bootstrap.min.css',
        'bower_components/dropzone/dist/dropzone.css',
        'bower_components/bootstrap-fileinput/css/fileinput.min.css',
        './public/themes/medusa/css/app2.css'
    ], 'public/themes/medusa/css/app.css', 'resources/assets')

    .scripts([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/Chart.js/dist/Chart.min.js',
        'bower_components/vue/dist/vue.min.js',
        'bower_components/vue-resource/dist/vue-resource.min.js',
        'bower_components/ace-builds/src-min/ace.js',
        'bower_components/ace-builds/src-min/ext-modelist.js',
        'bower_components/toastr/toastr.min.js',
        'bower_components/clipboard/dist/clipboard.min.js',
        'bower_components/datatables/media/js/jquery.dataTables.min.js',
        'bower_components/datatables/media/js/dataTables.bootstrap.min.js',
        'bower_components/jquery-multiselect/jquery-multiselect.js',
        'bower_components/jquery.hotkeys/jquery.hotkeys.js',
        'bower_components/dropzone/dist/dropzone.js',
        'bower_components/bootstrap-fileinput/js/fileinput.min.js',
        'js/medusa/app.js',
        'js/medusa/vm.js',
        'js/medusa/serversetting.js',
        'js/medusa/whopsetting.js'
    ], 'public/themes/medusa/js/app.js', 'resources/assets')

    .copy('resources/assets/bower_components/bootstrap/fonts', 'public/build/themes/medusa/fonts')
    .copy('resources/assets/bower_components/font-awesome/fonts', 'public/build/themes/medusa/fonts')
    .copy('resources/assets/font/medusa/lato', 'public/build/themes/medusa/fonts')
    .copy('resources/assets/images/medusa', 'public/build/themes/medusa/images')
    .copy('resources/assets/bower_components/ace-builds/src-min', 'public/build/themes/medusa/js/ace')

    .version2([
        'public/themes/medusa/css/app.css',
        'public/themes/medusa/css/app2.css',
        'public/themes/medusa/js/app.js'
    ], 'medusa');
});

